//
//  CreatePhotoCircleCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol CreatePhotoCircleCellDelegate {
  func didSelectCell(cell: CreatePhotoCircleCell)
}

class CreatePhotoCircleCell: UITableViewCell {
  @IBOutlet weak var selectorImageView: UIImageView!
  @IBOutlet weak var circlePictureImageView: UIImageView!
  @IBOutlet weak var circleLabel: UILabel!
  
  var delegate: CreatePhotoCircleCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    circlePictureImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
  }
  
  func configureWithCircle(circle: Circle, selected: Bool) {
    circlePictureImageView.setImageWithURL(NSURL(string: circle.pictureURL ?? "")!, placeholderImage: R.image.img_placeholder_circle)
    circleLabel.text = circle.name
    selectorImageView.image = selected ? R.image.btn_radio_on : R.image.btn_radio_off
  }
  
  func updateWithSelection(selection: Bool) {
    selectorImageView.image = selection ? R.image.btn_radio_on : R.image.btn_radio_off
  }
  
  @IBAction func toggleSelection(sender: UIButton) {
    delegate?.didSelectCell(self)
  }
}
