//
//  CircleCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CircleCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var circlePictureImageView: UIImageView!
  
  func configureWithTitle(title: String, circlePicture: String?) {
    titleLabel.text = title
    circlePictureImageView.image = R.image.img_placeholder_circle
    if let circlePicture = circlePicture, let url = NSURL(string: circlePicture) {
      circlePictureImageView.setImageWithURL(url)
    }
  }
  
  func configureWithAddTitle(title: String) {
    titleLabel.text = title
    circlePictureImageView.image = R.image.icn_menu_novo_circulo
  }
}
