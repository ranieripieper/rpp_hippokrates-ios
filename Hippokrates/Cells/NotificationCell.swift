//
//  NotificationCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
  @IBOutlet weak var label: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    label.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 20 * 2
  }
  
  func configureWithNotification(userNotification: UserNotification) {
    label.text = userNotification.text
    label.font = UIFont(name: (userNotification.unread ? "HelveticaNeue-Bold" : "HelveticaNeue-Light"), size: 17)
  }
}
