//
//  UITableViewCell+DynamicHeight.swift
//  Hippokrates
//
//  Created by Gilson Gil on 7/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

extension UITableViewCell {
  func calculateHeight() -> CGFloat {
    setNeedsLayout()
    layoutIfNeeded()
    return contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height + 1
  }
}
