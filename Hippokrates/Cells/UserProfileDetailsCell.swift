//
//  UserProfileDetailsCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/30/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum CellType {
  case State, School, Class, Specialty, Occupation, Residency
  
  private func icon() -> UIImage {
    switch self {
    case .State:
      return R.image.icn_sign_up_cadastro_crm!
    case .School:
      return R.image.icn_sign_up_instituicao!
    case .Class:
      return R.image.icn_sign_up_numero_da_turma!
    case .Specialty:
      return R.image.icn_sign_up_especialidade!
    case .Occupation:
      return R.image.icn_sign_up_ocupacao!
    case .Residency:
      return R.image.icn_sign_up_inst_residencia!
    }
  }
  
  private func title() -> String {
    switch self {
    case .State:
      return "estado"
    case .School:
      return "instituição de ensino"
    case .Class:
      return "número da turma"
    case .Specialty:
      return "especialidade"
    case .Occupation:
      return "ocupação"
    case .Residency:
      return "instituição de residência"
    }
  }
}

final class UserProfileDetailsCell: UITableViewCell {
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subtitleLabel: UILabel!
  
  func configureWithType(type: CellType, text: String) {
    iconImageView.image = type.icon()
    titleLabel.text = type.title()
    subtitleLabel.text = text
  }
}
