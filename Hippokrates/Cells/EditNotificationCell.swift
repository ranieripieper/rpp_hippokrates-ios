//
//  EditNotificationCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol EditNotificationCellDelegate {
  func activated(activated: Bool, atCell cell: EditNotificationCell)
}

class EditNotificationCell: UITableViewCell {
  @IBOutlet weak var label: UILabel!
  @IBOutlet weak var activatedSwitch: UISwitch!
  
  var delegate: EditNotificationCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    label.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 2 * 20 - 51
  }
  
  func configureWithPushNotification(pushNotification: PushNotification) {
    label.text = pushNotification.toString()
    activatedSwitch.on = pushNotification.activated
  }
  
  @IBAction func toggleActivation(sender: UISwitch) {
    delegate?.activated(sender.on, atCell: self)
  }
}
