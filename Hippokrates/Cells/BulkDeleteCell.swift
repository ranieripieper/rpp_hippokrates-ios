//
//  BulkDeleteCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 6/3/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BulkDeleteCell: UICollectionViewCell {
  @IBOutlet weak var photoThumbImageView: UIImageView!
  @IBOutlet weak var circleNameLabel: UILabel!
  
  @IBOutlet weak var selectedView: UIView?
  override var selected : Bool {
    didSet {
      selectedView?.hidden = !selected
    }
  }
  
  func configureWithPhoto(photo: Photo) {
    selectedView?.frame = bounds
    circleNameLabel.text = photo.circle?.name
    if let circle = photo.circle where circle.type != "public" {
      circleNameLabel.text = circle.name
    } else {
      circleNameLabel.text = photo.specialty?.name
    }
    photoThumbImageView.image = nil
    photoThumbImageView.setImageWithURL(NSURL(string: photo.imageURL ?? "")!, placeholderImage: R.image.img_placeholder_photo)
  }
}
