//
//  CommentCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol CommentCellDelegate {
  func goToUserAtCommentCell(cell: CommentCell)
  func presentOptionsAtCommentCell(cell: CommentCell)
}

class CommentCell: UITableViewCell {
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var userButton: UIButton!
  @IBOutlet weak var commentLabel: UILabel!
  
  var delegate: CommentCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    avatarImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
    let longPress = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
    addGestureRecognizer(longPress)
  }
  
  func configureWithComment(comment: Comment) {
    avatarImageView.setImageWithURL(NSURL(string: comment.user?.avatarURL ?? "")!, placeholderImage: R.image.img_placeholder)
    userButton.setTitle(comment.user?.name, forState: .Normal)
    commentLabel.text = comment.text
  }
  
  func handleLongPress(sender: UILongPressGestureRecognizer) {
    delegate?.presentOptionsAtCommentCell(self)
  }
  
  func updateComment(text: String) {
    commentLabel.text = text
  }
  
  // MARK: User
  @IBAction func userTapped(sender: UIButton) {
    delegate?.goToUserAtCommentCell(self)
  }
}
