//
//  AutocompleteCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AutocompleteCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
  
  func configureWithText(text: String) {
    titleLabel.text = text
  }
}
