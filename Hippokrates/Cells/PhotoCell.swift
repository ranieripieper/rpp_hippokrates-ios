//
//  PhotoCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol PhotoCellDelegate {
  func likeAtCell(cell: PhotoCell)
  func commentCell(cell: PhotoCell)
  func goToCircleAtCell(cell: PhotoCell)
  func goToUserAtPhotoCell(cell: PhotoCell)
  func optionsForCell(cell: PhotoCell)
}

class PhotoCell: UITableViewCell {
  @IBOutlet weak var likesButton: UIButton!
  @IBOutlet weak var commentsButton: UIButton!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var photoImageView: UIImageView!
  @IBOutlet weak var circleImageView: UIImageView!
  @IBOutlet weak var circleButton: UIButton!
  @IBOutlet weak var userButton: UIButton!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var userButtonHeightConstraint: NSLayoutConstraint!
  
  let imageCache = NSCache()
  var delegate: PhotoCellDelegate?
  weak var photo: Photo?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    descriptionLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 12 * 2
    circleImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
    let doubleTap = UITapGestureRecognizer(target: self, action: "likeTapped:")
    doubleTap.numberOfTapsRequired = 2
    photoImageView.addGestureRecognizer(doubleTap)
  }
  
  func configureWithPhoto(photo: Photo, showUser: Bool, showHeader: Bool, dateFormatter: NSDateFormatter) {
    likesButton.hidden = !showHeader
    commentsButton.hidden = !showHeader
    dateLabel.hidden = !showHeader
    self.photo = photo
    likesButton.selected = photo.liked
    likesButton.setTitle(String(photo.likesCount), forState: .Normal)
    commentsButton.setTitle(String(photo.commentsCount), forState: .Normal)
    dateLabel.text = dateFormatter.stringFromDate(photo.createDate)
    let photoPlaceholderImage = R.image.img_placeholder_photo
    photoImageView.image = photoPlaceholderImage
    if photo.image != nil {
      photoImageView.image = photo.image!
    } else {
      photoImageView.setImageWithURL(NSURL(string: photo.imageURL ?? "")!, placeholderImage: photoPlaceholderImage)
    }
    let circlePlaceholderImage = R.image.img_placeholder_circle
    circleImageView.image = circlePlaceholderImage
    circleImageView.setImageWithURL(NSURL(string: photo.circle?.pictureURL ?? "")!, placeholderImage: circlePlaceholderImage)
    if let circle = photo.circle where circle.type != "public" {
      circleButton.setTitle(circle.name, forState: .Normal)
    } else {
      circleButton.setTitle(photo.specialty?.name, forState: .Normal)
    }
    if showUser {
      userButton.setTitle(photo.user?.name, forState: .Normal)
      userButtonHeightConstraint.constant = 18
    } else {
      userButton.setTitle(nil, forState: .Normal)
      userButtonHeightConstraint.constant = 0
    }
    descriptionLabel.text = photo.description
  }
  
  func updateLikes(photo: Photo) {
    if photo.id == self.photo?.id {
      likesButton.selected = photo.liked
      likesButton.setTitle(String(photo.likesCount), forState: .Normal)
    }
  }
  
  // MARK: Actions
  @IBAction func likeTapped(sender: UIButton) {
    delegate?.likeAtCell(self)
  }
  
  @IBAction func commentTapped(sender: UIButton) {
    delegate?.commentCell(self)
  }
  
  @IBAction func circleTapped(sender: UIButton) {
    delegate?.goToCircleAtCell(self)
  }
  
  @IBAction func userTapped(sender: UIButton) {
    delegate?.goToUserAtPhotoCell(self)
  }
  
  @IBAction func optionsTapped(sender: UIButton) {
    delegate?.optionsForCell(self)
  }
}
