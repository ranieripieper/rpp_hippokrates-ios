//
//  UserCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol UserCellDelegate {
  func declineUserAtCell(cell: UserCell)
  func acceptUserAtCell(cell: UserCell)
  func optionsForUserAtCell(cell: UserCell)
}

class UserCell: UITableViewCell {
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var userLabel: UILabel!
  @IBOutlet weak var deleteButton: UIButton?
  @IBOutlet weak var acceptButton: UIButton?
  
  var delegate: UserCellDelegate?
  var longPress: UILongPressGestureRecognizer?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    layoutIfNeeded()
    avatarImageView.layer.cornerRadius = avatarImageView.bounds.height / 2
  }
  
  func configureWithUser(user: User, showEditButtons: Bool, longPress: Bool) {
    layoutIfNeeded()
    avatarImageView.layer.cornerRadius = avatarImageView.bounds.height / 2
    let placeholder = R.image.img_placeholder
    avatarImageView.image = placeholder
    avatarImageView.setImageWithURL(NSURL(string: user.avatarURL ?? "")!, placeholderImage: placeholder)
    userLabel.text = user.name
    if !showEditButtons {
      deleteButton?.removeFromSuperview()
      acceptButton?.removeFromSuperview()
    }
    if longPress && self.longPress == nil {
      self.longPress = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
      addGestureRecognizer(self.longPress!)
    }
  }
  
  // MARK: Actions
  @IBAction func deleteTapped(sender: UIButton) {
    delegate?.declineUserAtCell(self)
  }
  
  @IBAction func acceptTapped(sender: UIButton) {
    delegate?.acceptUserAtCell(self)
  }
  
  func handleLongPress(longPress: UILongPressGestureRecognizer) {
    delegate?.optionsForUserAtCell(self)
  }
}
