//
//  InviteCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class InviteCell: UITableViewCell {
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var selectorImageView: UIImageView!
  
  func configureWithAvatar(avatar: UIImage?, name: String, email: String, selected: Bool) {
    avatarImageView.image = avatar ?? R.image.img_placeholder
    nameLabel.text = name
    emailLabel.text = email
    selectorImageView.image = selected ? R.image.btn_radio_on : R.image.btn_radio_off
  }
  
  func select() {
    selectorImageView.image = R.image.btn_radio_on
  }
  
  func deselect() {
    selectorImageView.image = R.image.btn_radio_off
  }
}
