//
//  PhotoThumbCell.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class PhotoThumbCell: UICollectionViewCell {
  @IBOutlet weak var photoThumbImageView: UIImageView!
  
  func configureWithPhoto(photo: Photo) {
    photoThumbImageView.setImageWithURL(NSURL(string: photo.imageURL ?? "")!, placeholderImage: R.image.img_placeholder_photo)
  }
}
