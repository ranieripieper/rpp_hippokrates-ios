//
//  PushNotification.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class PushNotification {
  let name: String
  var activated: Bool
  
  init(name: String, activated: Bool) {
    self.name = name
    self.activated = activated
  }
  
  func toString() -> String {
    return PushNotification.fromString(name)
  }
  
  class func fromString(string: String) -> String {
    switch string {
    case "notify_comment_reported":
      return "Me avisar quando um comentário meu for reportado"//
    case "notify_friend_invite_signup":
      return "Me avisar quando um amigo meu se cadastrar com meu convite"//
    case "notify_friendship_circle_admin_received":
      return "Me avisar quando me tornar admin de um círculo"//
    case "notify_new_comment":
      return "Me avisar quando uma foto minha tiver novo comentário"//
    case "notify_password_updated":
      return "Me avisar quando minha senha for alterada"//
    case "notify_circle_new_member":
      return "Me avisar quando um círculo que pertenço tiver um novo membro"//
    case "notify_new_pending_circle_member":
      return "Me avisar quando um círculo que administro tiver uma nova sugestão de membro"//
    case "notify_pending_request_approved":
      return "Me avisar quando for autorizado a participar de um círculo"
    case "notify_pending_request_declined":
      return "Me avisar quando for recusado a participar de um círculo"//
    case "notify_post_deleted":
      return "Me avisar quando uma foto minha for excluída"//
    case "notify_new_like":
      return "Me avisar quando um usuário curtiu uma foto minha"
    case "notify_post_reported":
      return "Me avisar quando uma foto minha for denunciada"
    case "notify_removed_from_circle":
      return "Me avisar quando for excluído do círculo"
//    case "notify_signup":
//      return "Me avisar quando "//
//    case "notify_subscription_successfully_charged":
//      return "Me avisar quando minha assinatura for cobrada com sucesso"//
//    case "notify_subscription_will_expire":
//      return "Me avisar quando minha assinatura for expirar"
    default:
      return ""
    }
  }
}

// MARK: API
extension PushNotification {
  class func registerToken(token: String) {
    let newToken = token.stringByReplacingOccurrencesOfString("<", withString: "").stringByReplacingOccurrencesOfString(">", withString: "").stringByReplacingOccurrencesOfString(" ", withString: "")
    if let persistedToken = NSUserDefaults.standardUserDefaults().stringForKey("PushToken") where persistedToken == newToken {
      if !NSUserDefaults.standardUserDefaults().boolForKey("PushTokenRegistered") {
        sendToServer(newToken)
      }
    } else {
      NSUserDefaults.standardUserDefaults().setObject(newToken, forKey: "PushToken")
      NSUserDefaults.standardUserDefaults().removeObjectForKey("PushTokenRegistered")
      NSUserDefaults.standardUserDefaults().synchronize()
      sendToServer(newToken)
    }
  }
  
  class func registerToken() {
    if let persistedToken = NSUserDefaults.standardUserDefaults().stringForKey("PushToken") {
      registerToken(persistedToken)
    }
  }
  
  class func sendToServer(token: String) {
    if let userId = NSUserDefaults.standardUserDefaults().objectForKey("LoggedUserId") as? Int {
      NetworkManager.sharedInstance.registerDevice(token) {
        switch $0 {
        case .Success(let boxed):
          NSUserDefaults.standardUserDefaults().setBool(true, forKey: "PushTokenRegistered")
          NSUserDefaults.standardUserDefaults().synchronize()
        case .Failure(let error):
          break
        }
      }
    }
  }
}

// MARK: Handler
extension PushNotification {
  class func handleWithOptions(options: [String: AnyObject]) {
    if let data = options["data"] as? [String: AnyObject], let type = data["notification_type"] as? String, let pushType = pushType(type), let id = data["notificable_id"] as? Int {
      switch pushType {
      case .User:
        break
//        (UIApplication.sharedApplication().delegate as! AppDelegate).goToUser(id)
      case .Circle:
        (UIApplication.sharedApplication().delegate as! AppDelegate).goToCircle(id)
      case .Photo:
        (UIApplication.sharedApplication().delegate as! AppDelegate).goToPhoto(id)
      case .Subscription:
        (UIApplication.sharedApplication().delegate as! AppDelegate).goToSubscription()
      }
    }
  }
  
  enum PushType {
    case User, Circle, Photo, Subscription
  }
  
  class func pushType(type: String) -> PushType? {
    switch type {
//    case "":
//      return .User
    case "notify_friendship_circle_admin_transfered", "notify_friendship_circle_admin_received", "notify_new_pending_circle_member", "notify_pending_request_approved", "notify_pending_request_declined", "notify_circle_new_member", "circle_join_request_approved":
      return .Circle
    case "notify_comment_reported", "notify_post_reported", "notify_new_comment", "notify_post_liked", "notify_new_like", "new_like_on_post":
      return .Photo
    case "notify_subscription_will_expire":
      return .Subscription
    default:
      return nil
    }
  }
}
