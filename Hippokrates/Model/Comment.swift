//
//  Comment.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Comment {
  let id: Int
  var text: String
  let date: NSDate
  var user: User?
  let userId: Int?
  
  init(_ API: APIResponse) {
    id = API["id"] as? Int ?? 0
    text = API["content"] as? String ?? ""
    if let created_at = API["created_at"] as? String, let createdDate = DateFormatter.APIDateFormatter.dateFromString(created_at) {
      date = createdDate
    } else {
      date = NSDate()
    }
    if let userAPI = API["user"] as? APIResponse {
      user = User(userAPI)
    } else {
      user = nil
    }
    userId = API["user_id"] as? Int
  }
}

// MARK: API
extension Comment {
  func edit(postId: Int, text: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.editComment(id, postId: postId, text: text) {
      switch $0 {
      case .Success(let boxed):
        if let comment = boxed.unbox["comment"] as? APIResponse, let text = comment["content"] as? String {
          self.text = text
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func report(postId: Int, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.reportComment(id, postId: postId) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func delete(postId: Int, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.deleteComment(id, postId: postId) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
}
