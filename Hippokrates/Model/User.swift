//
//  User.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class User {
  static var currentUser = User(id: NSUserDefaults.standardUserDefaults().integerForKey("LoggedUserId"))
  
  var id: Int
  var name: String
  var firstName: String
  var lastName: String
  var gender: Gender
  var type: UserType = .Undefinied
  
//  var email: String?
//  var crm: Int?
//  var crmState: State?
  var avatarURL: String?
  var formation: SchoolAttended?
  var formationYear: Int?
  var specialty: Specialty?
  var residency: SchoolAttended?
  var photos = [Photo]()
  var circles = [Circle]()
  var notifications = [UserNotification]()
  var occupation: String?
  var remainingInvites = 0
  var remainingSubscriptionDays = 0
  var privacy = [String]()
  
//  init(id: Int, gender: Gender, firstName: String, lastName: String) {
//    self.id = id
//    self.name = firstName + " " + lastName
//    self.firstName = firstName
//    self.lastName = lastName
//    self.gender = gender
//  }
  
  init(id: Int) {
    self.id = id
    name = ""
    firstName = ""
    lastName = ""
    gender = .Undefinied
    me(nil)
    myCircles(1)
    currentSubscription() {
      switch $0 {
      case .Success(let boxed):
        break
      case .Failure(let error):
        break
      }
    }
  }
  
  init(_ API: APIResponse) {
    id = API["id"] as? Int ?? 0
    if let type = API["profile_type"] as? String {
      if type == "doctor" {
        self.type = .Physician
      } else if type == "teacher" {
        self.type = .Professor
      } else {
        self.type = .Student
      }
    } else {
      self.type = .Undefinied
    }
    firstName = API["name"] as? String ?? ""
    lastName = API["last_name"] as? String ?? ""
    name = API["fullname"] as? String ?? ""
    occupation = API["occupation"] as? String
    avatarURL = API["profile_image_url"] as? String
    if let genderString = API["gender"] as? String {
      gender = Gender(string: genderString)
    } else {
      gender = .Undefinied
    }
    if let educationData = API["education_data"] as? APIResponse {
      if let schoolName = educationData["university_institution_name"] as? String {
        let school = School(name: schoolName)
        formation = SchoolAttended(school: school, year: 0, type: .Formation)
      }
      if let schoolYear = educationData["classroom_number"] as? Int {
        formationYear = schoolYear
      }
      if let residencyName = educationData["residency_program_institution_name"] as? String {
        let school = School(name: residencyName)
        residency = SchoolAttended(school: school, year: 0, type: .Residency)
      }
    }
    if let medicalSpecialty = API["medical_specialty"] as? APIResponse {
      let specialty = Specialty.all() { _ in }
      let possibleSpecialty = specialty.filter() {
        $0.id == medicalSpecialty["id"] as! Int
        }.first
      if let possibleSpecialty = possibleSpecialty {
        self.specialty = possibleSpecialty
      }
    }
    photos = []
  }
//  {
  //  "id": 5,
  //  "profile_type": "doctor",
  //  "name": "Darwin",
  //  "last_name": "Hahn",
  //  "username": "darwinhahn",
  //  "fullname": "Darwin Hahn",
  //  "profile_images": {
  //    "mini_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/5/mini_thumb_idautemadipisci.png",
  //    "small_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/5/small_thumb_idautemadipisci.png",
  //    "thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/5/thumb_idautemadipisci.png",
  //    "medium": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/5/medium_idautemadipisci.png"
  //  }
  //},
  
  func appendUser(user: User) {
    name = user.name
    firstName = user.firstName
    lastName = user.lastName
    gender = user.gender
    type = user.type
    
    avatarURL = user.avatarURL ?? avatarURL
    formation = user.formation ?? formation
    formationYear = user.formationYear ?? formationYear
    specialty = user.specialty ?? specialty
    residency = user.residency ?? residency
    photos += user.photos.filter() {
      !Set(self.photos.map() { $0.id }).contains($0.id)
    }
    circles += user.circles.filter() {
      !Set(self.circles.map() { $0.id }).contains($0.id)
    }
    notifications += user.notifications.filter() {
      !Set(self.notifications.map() { $0.id }).contains($0.id)
    }
    occupation = user.occupation ?? occupation
  }
}

enum Gender {
  case Undefinied, Female, Male
  
  init(string: String) {
    switch string.lowercaseString {
    case "female", "feminino", "f":
      self = Female
    case "male", "masculino", "m":
      self = Male
    default:
      self = Undefinied
    }
  }
  
  func toAPIString() -> String {
    switch self {
    case .Female:
      return "female"
    case .Male:
      return "male"
    default:
      return ""
    }
  }
}

enum UserType {
  case Undefinied, Student, Physician, Professor
  
  init(string: String) {
    switch string.lowercaseString {
    case "student":
      self = .Student
    case "doctor":
      self = .Physician
    case "teacher":
      self = .Professor
    default:
      self = .Undefinied
    }
  }
}

// MARK: API
extension User {
//  https://hipapp-staging.herokuapp.com/api/v1/users?user[name]=Hip&user[surname]=App&user[email]=hipapp@doisdoissete.com.br&user[password]=qwerty123&user[password_confirmation]=qwerty123&user[crm][number]=1&user[crm][state]=MG&user[gender]=male&user[medical_specialty_id]=1
  class func register(email: String, password: String, firstName: String, lastName: String, crm: String, crmState: String, gender: Gender, specialty: Specialty?, occupation: String?, formationSchool: School?, formationClassNumber: String?, residencySchool: School?, code: String, avatar: UIImage?, professor: Bool, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.register(email, password: password, firstName: firstName, lastName: lastName, crm: crm, crmState: State(string: crmState), gender: gender, specialty: specialty, occupation: occupation, formationSchool: formationSchool, classNumber: formationClassNumber, residencySchool: residencySchool, code: code, avatar: avatar, professor: professor) {
      switch $0 {
      case .Success(let boxed):
        if let id = boxed.unbox["id"] as? Int {
          #if RELEASE
            Mixpanel.sharedInstance().createAlias(String(id), forDistinctID: Mixpanel.sharedInstance().distinctId)
            Mixpanel.sharedInstance().identify(Mixpanel.sharedInstance().distinctId)
            Mixpanel.sharedInstance().nameTag = firstName + " " + lastName
            Mixpanel.sharedInstance().people.set("profile_type", to: "doctor")
            Mixpanel.sharedInstance().people.set("gender", to: gender.toAPIString())
          #endif
          NSUserDefaults.standardUserDefaults().setInteger(id, forKey: "LoggedUserId")
          NSUserDefaults.standardUserDefaults().synchronize()
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func registerStudent(email: String, password: String, firstName: String, lastName: String, gender: Gender, formationSchool: School, highschoolState: State, highschoolYear: Int, code: String, avatar: UIImage?, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.registerStudent(email, password: password, firstName: firstName, lastName: lastName, gender: gender, formationSchool: formationSchool, highschoolState: highschoolState, highschoolYear: highschoolYear, code: code, avatar: avatar) {
      switch $0 {
      case .Success(let boxed):
        if let id = boxed.unbox["id"] as? Int {
          #if RELEASE
            Mixpanel.sharedInstance().identify(String(id))
            Mixpanel.sharedInstance().people.set("profile_type", to: "student")
            Mixpanel.sharedInstance().people.set("gender", to: gender.toAPIString())
          #endif
          NSUserDefaults.standardUserDefaults().setInteger(id, forKey: "LoggedUserId")
          NSUserDefaults.standardUserDefaults().synchronize()
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func login(email: String, password: String, completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.login(email, password: password) { result in
      switch result {
      case .Success(let boxed):
        if let userId = boxed.unbox["id"] as? Int {
          #if RELEASE
            Mixpanel.sharedInstance().identify(String(userId))
          #endif
          NSUserDefaults.standardUserDefaults().setInteger(userId, forKey: "LoggedUserId")
          NSUserDefaults.standardUserDefaults().synchronize()
        }
      case .Failure(let error):
        break
      }
      completion(result)
    }
  }
  
  class func validateCRM(crm: String, crmState: State, firstName: String, lastName: String, completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.validateCRM(crm, crmState: crmState, firstName: firstName, lastName: lastName, completion: completion)
  }
  
  func graduate(crm: String, crmState: State, firstName: String, lastName: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.graduateStudent(crm, crmState: crmState.UF(), name: firstName, lastName: lastName) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func me(completion: (Result<APIResponse> -> ())?) {
    NetworkManager.sharedInstance.me() {
      switch $0 {
      case .Success(let boxed):
        if let user = boxed.unbox["user"] as? APIResponse {
          self.id = user["id"] as? Int ?? 0
          self.name = user["fullname"] as? String ?? self.name
          self.firstName = user["name"] as? String ?? self.firstName
          self.lastName = user["last_name"] as? String ?? self.lastName
          if let gender = user["gender"] as? String {
            self.gender = Gender(string: gender)
          }
          if let specialtyId = user["medical_specialty_id"] as? Int {
            self.specialty = Specialty.id(specialtyId)
          }
          if let notificationsAPI = user["notifications"] as? [APIResponse] {
            var notifications = [UserNotification]()
            for notificationAPI in notificationsAPI {
              notifications.append(UserNotification(notificationAPI))
            }
            self.notifications = notifications
          }
          self.occupation = user["occupation"] as? String ?? ""
          self.avatarURL = user["profile_image_url"] as? String ?? self.avatarURL
          if let userType = user["profile_type"] as? String {
            self.type = UserType(string: userType)
          }
          self.remainingInvites = user["remaining_invites"] as? Int ?? self.remainingInvites
          self.remainingSubscriptionDays = user["remaining_subscription_days"] as? Int ?? self.remainingSubscriptionDays
          if let circlesAPI = user["friendship_circles"] as? [APIResponse] {
            var circles = [Circle]()
            for circleAPI in circlesAPI {
              let circle = Circle(circleAPI)
              if circle.id != 1 {
                circles.append(Circle(circleAPI))
              }
            }
            self.circles = circles
          }
          if let privacyData = user["privacy_role_data"] as? APIResponse {
            self.privacy.removeAll(keepCapacity: false)
            if let privateData = privacyData["private"] as? APIResponse {
              for key in privateData.keys {
                if let array = privateData[key] as? [String] {
                  for value in array {
                    self.privacy.append(key + "." + value)
                  }
                } else {
                  self.privacy.append(key)
                }
              }
            }
          }
        }
      case .Failure(let error):
        break
      }
      completion?($0)
    }
  }
  
  func myCircles(page: Int) {
    NetworkManager.sharedInstance.myCircles(page) {
      switch $0 {
      case .Success(let boxed):
        if let circlesAPI = boxed.unbox["friendship_circles"] as? [APIResponse] {
          var circles = [Circle]()
          for circleAPI in circlesAPI {
            let circle = Circle(circleAPI)
            if circle.type != "public" {
              circles.append(Circle(circleAPI))
            }
          }
          self.circles = circles
        }
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          self.myCircles(page + 1)
        }
      case .Failure(let error):
        break
      }
    }
  }
  
  class func userWithId(id: Int, completion: Result<User> -> ()) {
      NetworkManager.sharedInstance.userWithId(id) {
        switch $0 {
        case .Success(let boxed):
          if let userAPI = boxed.unbox["user"] as? APIResponse {
            completion(Result(User(userAPI)))
          } else {
            completion(Result(Error(code: -1, domain: "us.hippokrates", description: nil)))
          }
        case .Failure(let error):
          completion(Result(error))
        }
      }
  }
  
  func feed(page: Int, completion: Result<([Photo], Bool)> -> ())  {
    NetworkManager.sharedInstance.feed(page) {
      switch $0 {
      case .Success(let boxed):
        var users = [User]()
        var circles = [Circle]()
        if let linkedData = boxed.unbox["linked_data"] as? APIResponse {
          if let circlesAPI = linkedData["friendship_circles"] as? [APIResponse] {
            for circleAPI in circlesAPI {
              circles.append(Circle(circleAPI))
            }
          }
          if let usersAPI = linkedData["users"] as? [APIResponse] {
            for userAPI in usersAPI {
              users.append(User(userAPI))
            }
          }
        }
        var photos = [Photo]()
        if let photosAPI = boxed.unbox["posts"] as? [APIResponse] {
          for photoAPI in photosAPI {
            if let blocked = photoAPI["blocked"] as? Bool where blocked {
              continue
            }
            let photo = Photo(photoAPI)
            let user = users.filter() {
              $0.id == photo.userId ?? 0
            }.first
            if let user = user {
              photo.user = user
            }
            let circle = circles.filter() {
              $0.id == photo.circleId ?? 0
            }.first
            if let circle = circle {
              photo.circle = circle
            }
            photos.append(photo)
          }
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (photos, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func feedWithId(page: Int, completion: Result<([Photo], Bool)> -> ())  {
    NetworkManager.sharedInstance.feedWithId(id, page: page) {
      switch $0 {
      case .Success(let boxed):
        var users = [User]()
        var circles = [Circle]()
        if let linkedData = boxed.unbox["linked_data"] as? APIResponse {
          if let circlesAPI = linkedData["friendship_circles"] as? [APIResponse] {
            for circleAPI in circlesAPI {
              circles.append(Circle(circleAPI))
            }
          }
          if let usersAPI = linkedData["users"] as? [APIResponse] {
            for userAPI in usersAPI {
              users.append(User(userAPI))
            }
          }
        }
        var photos = [Photo]()
        if let basePostsAPI = boxed.unbox["base_posts"] as? [APIResponse] {
          for basePostAPI in basePostsAPI {
            if let postsAPI = basePostAPI["posts"] as? [APIResponse] {
              for postAPI in postsAPI {
                if let blocked = postAPI["blocked"] as? Bool where blocked {
                  continue
                }
                var photoAPI = postAPI
                photoAPI["base_post"] = basePostAPI
                let photo = Photo(photoAPI)
                photo.imageURL = photo.imageURL ?? basePostAPI["image_url"] as? String
                let user = users.filter() {
                  $0.id == photo.userId ?? 0
                  }.first
                if let user = user {
                  photo.user = user
                }
                let circle = circles.filter() {
                  $0.id == photo.circleId ?? 0
                  }.first
                if let circle = circle {
                  photo.circle = circle
                }
                photos.append(photo)
              }
            }
          }
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPath = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (photos, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func notifications(page: Int, completion: Result<([UserNotification], Bool)> -> ()) {
    NetworkManager.sharedInstance.notifications(page) {
      switch $0 {
      case .Success(let boxed):
        var notifications = [UserNotification]()
        if let notificationsAPI = boxed.unbox["notifications"] as? [APIResponse] {
          for notificationAPI in notificationsAPI {
            notifications.append(UserNotification(notificationAPI))
          }
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (notifications, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func updateProfile(occupation: String?, specialtyId: Int?, formationSchoolName: String?, formationSchoolYear: Int?, professor: Bool?, avatar: UIImage?, password: String?, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.updateProfile(occupation, specialtyId: specialtyId, formationSchoolName: formationSchoolName, formationSchoolYear: formationSchoolYear, professor: professor, avatar: avatar, password: password) {
      switch $0 {
      case .Success(let boxed):
        if let userAPI = boxed.unbox["user"] as? APIResponse {
          self.appendUser(User(userAPI))
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func updateStudentProfile(formationSchoolName: String?, avatar: UIImage?, password: String?, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.updateStudentProfile(formationSchoolName, avatar: avatar, password: password) {
      switch $0 {
      case .Success(let boxed):
        if let userAPI = boxed.unbox["user"] as? APIResponse {
          self.appendUser(User(userAPI))
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func changePrivacy(key: String, value: Bool, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.changePrivacy(key, value: value) {
      switch $0 {
      case .Success(let boxed):
        if value {
          self.privacy.append(key)
        } else {
          self.privacy = self.privacy.filter() {
            $0 != key
          }
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func currentSubscription(completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.currentSubscription() {
      switch $0 {
      case .Success(let boxed):
        break
      case .Failure(let error):
        break
      }
    }
  }
  
  func deleteAllPhotos(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.deleteAllPhotos() {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func deletePhotosWithIds(ids: [Int], completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.deletePhotosWithIds(ids) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func searchUsers(string: String, page: Int, onlyPhysicians: Bool, completion: Result<([User], Bool)> -> ()) -> NSURLSessionDataTask {
    return NetworkManager.sharedInstance.searchUser(string, page: page, onlyPhysicians: onlyPhysicians) {
      switch $0 {
      case .Success(let boxed):
        var users = [User]()
        if let usersAPI = boxed.unbox["users"] as? [APIResponse] {
          for userAPI in usersAPI {
            users.append(User(userAPI))
          }
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (users, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func invite(users: [(String, String)], completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.invite(users) {
      switch $0 {
      case .Success(let boxed):
        if let remainingInvites = boxed.unbox["remaining_invites"] as? Int {
          self.remainingInvites = remainingInvites
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func newPassword(password: String, token: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.newPassword(password, token: token) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func deleteAccount(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.deleteAccount() {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func preferences(completion: Result<[PushNotification]> -> ()) {
    NetworkManager.sharedInstance.preferences() {
      switch $0 {
      case .Success(let boxed):
        var preferences = [PushNotification]()
        if let userAPI = boxed.unbox["user"] as? APIResponse, let preferencesAPI = userAPI["preferences"] as? APIResponse {
          for preferenceAPI in preferencesAPI.keys {
            let preferenceString = PushNotification.fromString(preferenceAPI)
            if count(preferenceString) > 0 {
              if let activated = preferencesAPI[preferenceAPI] as? String {
                let preference = PushNotification(name: preferenceAPI, activated: activated == "true")
                preferences.append(preference)
              }
            }
          }
        }
        completion(Result(preferences))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func updatePreferences(preferences: [PushNotification], completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.updatePreferences(preferences) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
}
