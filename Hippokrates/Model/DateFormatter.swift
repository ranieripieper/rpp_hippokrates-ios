//
//  DateFormatter.swift
//  Hippokrates
//
//  Created by Gilson Gil on 5/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class DateFormatter {
  class var APIDateFormatter: NSDateFormatter {
    struct Static {
      static let instance: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ'"
        return dateFormatter
        }()
    }
    return Static.instance
  }
}
