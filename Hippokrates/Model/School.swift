//
//  School.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

@objc(School)
class School: NSObject, NSCoding, NamedObject {
  let id: Int
  let name: String
  let branchId: Int
  let branchName: String
  
  init(id: Int, name: String, branchId: Int, branchName: String) {
    self.id = id
    self.name = name
    self.branchId = branchId
    self.branchName = branchName
  }
  
  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeIntegerForKey("id")
    name = aDecoder.decodeObjectForKey("name") as! String
    branchId = aDecoder.decodeIntegerForKey("branchId")
    branchName = aDecoder.decodeObjectForKey("branchName") as! String
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeInteger(id, forKey: "id")
    aCoder.encodeObject(name, forKey: "name")
    aCoder.encodeInteger(branchId, forKey: "branchId")
    aCoder.encodeObject(branchName, forKey: "branchName")
  }
  
  init(_ API: APIResponse) {
    id = API["id"] as? Int ?? 0
    if let branch = API["city"] as? APIResponse {
      branchId = branch["id"] as? Int ?? 0
      branchName = branch["name"] as? String ?? ""
      if count(branchName) > 0 {
        name = (API["name"] as? String ?? "") + " - " + branchName
      } else {
        name = API["name"] as? String ?? ""
      }
    } else {
      name = API["name"] as? String ?? ""
      branchId = 0
      branchName = ""
    }
  }
  
  init(name: String) {
    id = 0
    self.name = name
    branchId = 0
    branchName = ""
  }
}

// MARK: API
extension School {
  class func initialLoad() {
    all() { result in }
  }
  
  class func all(completion: Result<[School]> -> ()) -> [School] {
    if let date = Defaults["SchoolsRefreshDate"].date {
      let timeInterval = abs(date.timeIntervalSinceDate(NSDate()))
      if timeInterval < 60 * 60 * 24 {
        if let schoolsData = Defaults["Schools"].object as? [NSData] {
          return schoolsData.map() { NSKeyedUnarchiver.unarchiveObjectWithData($0) as! School }.sorted() { $0.name < $1.name }
        }
      }
    }
    getAll(completion)
    return []
  }
  
  class func getAll(completion: Result<[School]> -> ()) {
    NetworkManager.sharedInstance.getSchools() { result in
      switch result {
      case .Success(let boxed):
        if let schoolsAPI = boxed.unbox["colleges"] as? [APIResponse] {
          var schools = [School]()
          for schoolAPI in schoolsAPI {
            let school = School(schoolAPI)
            if school.id != 0 && school.name != ""{
              schools.append(school)
            }
          }
          schools.sort() { $0.name < $1.name }
          Defaults["Schools"] = schools.map() { NSKeyedArchiver.archivedDataWithRootObject($0) }
          Defaults["SchoolsRefreshDate"] = NSDate()
          Defaults.synchronize()
          completion(Result(schools))
        } else {
          completion(Result(Error(code: -1, domain: "us.hippokrates", description: nil)))
        }
      case .Failure(let error):
        completion(Result(Error(code: -1, domain: "us.hippokrates", description: nil)))
      }
    }
  }
}
