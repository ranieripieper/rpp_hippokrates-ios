//
//  NamedObject.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

protocol NamedObject {
  var name: String { get }
}
