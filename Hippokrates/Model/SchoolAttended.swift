//
//  SchoolAttended.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class SchoolAttended {
  let school: School
  let year: Int
  let type: Type
  
  init(school: School, year: Int, type: Type) {
    self.school = school
    self.year = year
    self.type = type
  }
}

extension SchoolAttended {
  enum Type {
    case Formation, Residency, Teaching
  }
}
