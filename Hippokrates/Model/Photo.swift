//
//  Photo.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class Photo {
  var id: Int
  var basePostId: Int
  var image: UIImage?
  var imageURL: String?
  var user: User?
  let userId: Int?
  var circle: Circle?
  let circleId: Int?
  let description: String
  var liked: Bool
  var likesCount: Int
  var commentsCount: Int
  var comments = [Comment]()
  var createDate: NSDate
  let specialty: Specialty?
  
  init(_ API: APIResponse) {
    id = API["id"] as? Int ?? 0
    basePostId = API["base_post_id"] as? Int ?? 0
    let userId: Int?
    if let basePost = API["base_post"] as? APIResponse {
      userId = basePost["user_id"] as? Int
      description = basePost["content"] as? String ?? ""
      imageURL = API["image_url"] as? String
    } else {
      userId = API["user_id"] as? Int
      description = API["content"] as? String ?? ""
      imageURL = API["image_url"] as? String
    }
    if let userAPI = API["user"] as? APIResponse {
      user = User(userAPI)
    } else {
      user = nil
    }
    self.userId = userId
    if let circleAPI = API["friendship_circle"] as? APIResponse {
      circle = Circle(circleAPI)
    } else {
      circle = nil
    }
    circleId = API["friendship_circle_id"] as? Int
    liked = API["user_liked"] as? Bool ?? false
    likesCount = API["likes_count"] as? Int ?? 0
    commentsCount = API["comments_count"] as? Int ?? 0
    if let created_at = API["created_at"] as? String, let date = DateFormatter.APIDateFormatter.dateFromString(created_at) {
      createDate = date
    } else {
      createDate = NSDate()
    }
    if let commentsAPI = API["comments"] as? [APIResponse] {
      var comments = [Comment]()
      for commentAPI in commentsAPI {
        comments.append(Comment(commentAPI))
      }
      self.comments = comments
    } else {
      self.comments = []
    }
    if let specialtyId = API["medical_specialty_id"] as? Int, let specialty = Specialty.id(specialtyId) {
      self.specialty = specialty
    } else if let specialtyAPI = API["medical_specialty"] as? APIResponse, let specialtyId = specialtyAPI["id"] as? Int, let specialty = Specialty.id(specialtyId) {
      self.specialty = specialty
    } else {
      self.specialty = nil
    }
  }
}

// MARK: API
extension Photo {
  class func getPhotoWithId(id: Int, completion: Result<Photo> -> ()) {
    NetworkManager.sharedInstance.getPhotoWithId(id) {
      switch $0 {
      case .Success(let boxed):
        let photo = Photo(boxed.unbox)
        completion(Result(photo))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func upload(photo: UIImage, text: String, specialtyId: Int, isPrivate: Bool, circles: [Circle], completion: Result<Photo> -> ()) {
    NetworkManager.sharedInstance.uploadPhoto(photo, text: text, specialtyId: specialtyId, isPrivate: isPrivate, circles: circles.map() { $0.id }) {
      switch $0 {
      case .Success(let boxed):
        if let basePostAPI = boxed.unbox["base_post"] as? APIResponse {
          completion(Result(Photo(basePostAPI)))
        }
        completion(Result(Error(code: -1, domain: "", description: "")))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func update(image: UIImage, text: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.updatePhoto(basePostId, image: image, text: text) {
      switch $0 {
      case .Success(let boxed):
        (UIImageView.sharedImageCache() as! NSCache).removeObjectForKey(self.imageURL!)
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func delete(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.deletePhoto(id) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func adminDeleteFromCircle(circleId: Int, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.removePhoto(id, fromCircle: circleId) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func toggleLike(completion: Result<Int> -> ()) {
    if liked {
      dislike(completion)
    } else {
      like(completion)
    }
  }
  
  func like(completion: Result<Int> -> ()) {
    NetworkManager.sharedInstance.likePhoto(id) {
      switch $0 {
      case .Success(let boxed):
        if let count = boxed.unbox["post_likes_count"] as? Int {
          self.liked = true
          self.likesCount = count
          completion(Result(count))
        } else {
          completion(Result(-1))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func dislike(completion: Result<Int> -> ()) {
    NetworkManager.sharedInstance.dislikePhoto(id) {
      switch $0 {
      case .Success(let boxed):
        if let count = boxed.unbox["post_likes_count"] as? Int {
          self.liked = false
          self.likesCount = count
          completion(Result(count))
        } else {
          completion(Result(-1))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func comment(comment: String, completion: Result<Comment> -> ()) {
    NetworkManager.sharedInstance.commentPhoto(id, comment: comment) {
      switch $0 {
      case .Success(let boxed):
        if let commentAPI = boxed.unbox["comment"] as? APIResponse {
          let comment = Comment(commentAPI)
          comment.user = User.currentUser
          self.comments.insert(comment, atIndex: 0)
          completion(Result(comment))
        } else {
          completion(Result(Error(code: -1, domain: "us.hippokrates", description: nil)))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func photoComments(page: Int, completion: Result<([Comment], Bool)> -> ()) {
    NetworkManager.sharedInstance.photoComments(id, page: page) {
      switch $0 {
      case .Success(let boxed):
        var users = [User]()
        if let linkedData = boxed.unbox["linked_data"] as? APIResponse, let usersAPI = linkedData["users"] as? [APIResponse] {
          for userAPI in usersAPI {
            users.append(User(userAPI))
          }
        }
        var comments = [Comment]()
        if let commentsAPI = boxed.unbox["comments"] as? [APIResponse] {
          for commentAPI in commentsAPI {
            let comment = Comment(commentAPI)
            let user = users.filter() {
              $0.id == comment.userId ?? 0
            }.first
            if let user = user {
              comment.user = user
            }
            comments.append(comment)
          }
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (comments, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  enum ReportType {
    case Improper, Patient
    
    func rawValue() -> Int {
      switch self {
      case .Improper:
        return 1
      case .Patient:
        return 2
      }
    }
  }
  
  func report(type: ReportType, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.report(id, type: type.rawValue()) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func search(hashtag: String, completion: Result<([Photo], Bool)> -> ()) {
    NetworkManager.sharedInstance.searchPosts(hashtag) {
      switch $0 {
      case .Success(let boxed):
        var users = [User]()
        if let linkedData = boxed.unbox["linked_data"] as? APIResponse, let usersAPI = linkedData["users"] as? [APIResponse] {
          for userAPI in usersAPI {
            users.append(User(userAPI))
          }
        }
        var photos = [Photo]()
        if let photosAPI = boxed.unbox["posts"] as? [APIResponse] {
          for photoAPI in photosAPI {
            if let blocked = photoAPI["blocked"] as? Bool where blocked {
              continue
            }
            let photo = Photo(photoAPI)
            let user = users.filter() {
              $0.id == photo.userId ?? 0
              }.first
            if let user = user {
              photo.user = user
            }
            photos.append(photo)
          }
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (photos, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func search(specialtyId: Int, page: Int, completion: Result<([Photo], [User], Bool, Int)> -> ()) {
    NetworkManager.sharedInstance.searchPosts(specialtyId, page: page) {
      switch $0 {
      case .Success(let boxed):
        var users = [User]()
        if let linkedData = boxed.unbox["linked_data"] as? APIResponse, let usersAPI = linkedData["users"] as? [APIResponse] {
          for userAPI in usersAPI {
            users.append(User(userAPI))
          }
        }
        var photos = [Photo]()
        if let photosAPI = boxed.unbox["posts"] as? [APIResponse] {
          for photoAPI in photosAPI {
            if let blocked = photoAPI["blocked"] as? Bool where blocked {
              continue
            }
            let photo = Photo(photoAPI)
            let user = users.filter() {
              $0.id == photo.userId ?? 0
              }.first
            if let user = user {
              photo.user = user
            }
            photos.append(photo)
          }
        }
        let hasNextPage: Bool
        let totalPhotos: Int
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse {
          if let nextPage = pagination["next_page"] as? Int {
            hasNextPage = true
          } else {
            hasNextPage = false
          }
          if let total = pagination["total_count"] as? Int {
            totalPhotos = total
          } else {
            totalPhotos = 0
          }
        } else {
          hasNextPage = false
          totalPhotos = 0
        }
        let tuple = (photos, users, hasNextPage, totalPhotos)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func getHashtags(completion: Result<([String], Bool)> -> ()) {
    NetworkManager.sharedInstance.getHashtags() {
      switch $0 {
      case .Success(let boxed):
        var hashtags = [String]()
        if let hashtagsAPI = boxed.unbox["hashtags"] as? [String] {
          hashtags = hashtagsAPI
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (hashtags, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func getHashtags(string: String, page: Int, completion: Result<([String], Bool)> -> ()) -> NSURLSessionDataTask {
    return NetworkManager.sharedInstance.getHashtags(string, page: page) {
      switch $0 {
      case .Success(let boxed):
        var hashtags = [String]()
        if let hashtagsAPI = boxed.unbox["hashtags"] as? [String] {
          hashtags = hashtagsAPI
        }
        let hasNextPage: Bool
        if let pagination = boxed.unbox["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (hashtags, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
}
