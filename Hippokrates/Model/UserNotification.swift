//
//  UserNotification.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class UserNotification {
  let id: Int
  let text: String
  var unread: Bool
  
  init(_ API: APIResponse) {
    id = API["id"] as? Int ?? 0
    text = (API["formatted_body"] as? String ?? "").stringByReplacingOccurrencesOfString("<strong>", withString: "").stringByReplacingOccurrencesOfString("</strong>", withString: "")
    let read = API["read"] as? Bool ?? false
    unread = !read
  }
}
