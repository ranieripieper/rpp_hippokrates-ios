//
//  Specialty.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

@objc(Specialty)
class Specialty: NSObject, NSCoding, NamedObject {
  dynamic let id: Int
  dynamic let name: String
  
  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeIntegerForKey("id")
    name = aDecoder.decodeObjectForKey("name") as! String
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeInteger(id, forKey: "id")
    aCoder.encodeObject(name, forKey: "name")
  }
  
  init(data: APIResponse) {
    self.id = data["id"] as? Int ?? 0
    self.name = data["name"] as? String ?? ""
  }
  
  class func id(id: Int) -> Specialty? {
    return all(nil).filter() {
      $0.id == id
    }.first
  }
}

// MARK: API
extension Specialty {
  class func initialLoad() {
    all() { result in }
  }
  
  class func all(completion: (Result<[Specialty]> -> ())?) -> [Specialty] {
    if let date = Defaults["SpecialtiesRefreshDate"].date {
      if abs(date.timeIntervalSinceDate(NSDate())) < 60 * 60 * 24 {
        if let specialtiesData = Defaults["Specialties"].object as? [NSData] {
          return specialtiesData.map() {
            NSKeyedUnarchiver.unarchiveObjectWithData($0) as! Specialty
            }.sorted() {
              $0.name < $1.name
          }
        }
      }
    }
    if let completion = completion {
      getAll(completion)
    }
    return []
  }
  
  class func getAll(completion: Result<[Specialty]> -> ()) {
    NetworkManager.sharedInstance.getSpecialties() { result in
      switch result {
      case .Success(let boxed):
        if let specialtiesAPI = boxed.unbox["medical_specialities"] as? [APIResponse] {
          var specialties = [Specialty]()
          for specialtyAPI in specialtiesAPI {
            let specialty = Specialty(data: specialtyAPI)
            if specialty.id != 0 && specialty.name != ""{
              specialties.append(specialty)
            }
          }
          specialties.sort() { $0.name < $1.name }
          Defaults["Specialties"] = specialties.map() { NSKeyedArchiver.archivedDataWithRootObject($0) }
          Defaults["SpecialtiesRefreshDate"] = NSDate()
          Defaults.synchronize()
          completion(Result(specialties))
        } else {
          completion(Result(Error(code: -1, domain: "us.hippokrates", description: nil)))
        }
      case .Failure(let error):
        completion(Result(Error(code: -1, domain: "us.hippokrates", description: nil)))
      }
    }
  }
}
