//
//  State.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

enum State {
  case None, AC, AL, AP, AM, BA, CE, DF, ES, GO, MA, MS, MT, MG, PA, PB, PR, PE, PI, RN, RS, RJ, RO, RR, SC, SP, SE, TO
  
  init(string: String) {
    switch string.lowercaseString {
    case "acre", "ac":
      self = .AC
    case "alagoas", "al":
      self = .AL
    case "amapa", "amapá", "ap":
      self = .AP
    case "amazonas", "am":
      self = .AM
    case "bahia", "ba":
      self = .BA
    case "ceara", "ceará", "ce":
      self = .CE
    case "distrito federal", "df":
      self = .DF
    case "espirito santo", "espírito santo", "es":
      self = .ES
    case "goias", "goiás", "go":
      self = .GO
    case "maranhao", "maranhão", "ma":
      self = .MA
    case "mato grosso do sul", "ms":
      self = .MS
    case "mato grosso", "mt":
      self = .MT
    case "minas gerais", "mg":
      self = .MG
    case "para", "pará", "pa":
      self = .PA
    case "paraíba", "paraiba", "pb":
      self = .PB
    case "parana", "paraná", "pr":
      self = .PR
    case "pernambuco", "pe":
      self = .PE
    case "piaui", "piauí", "pi":
      self = .PI
    case "rio grande do norte", "rn":
      self = .RN
    case "rio grande do sul", "rs":
      self = .RS
    case "rio de janeiro", "rj":
      self = .RJ
    case "rondonia", "rondônia", "ro":
      self = .RO
    case "roraima", "rr":
      self = .RR
    case "santa catarina", "sc":
      self = .SC
    case "sao paulo", "são paulo", "sp":
      self = .SP
    case "sergipe", "se":
      self = .SE
    case "tocantins", "to":
      self = .TO
    default:
      self = .None
    }
  }
  
  func name() -> String {
    switch self {
    case .AC:
      return "Acre"
    case .AL:
      return "Alagoas"
    case .AP:
      return "Amapá"
    case .AM:
      return "Amazonas"
    case .BA:
      return "Bahia"
    case .CE:
      return "Ceará"
    case .DF:
      return "Distrito Federal"
    case .ES:
      return "Espírito Santo"
    case .GO:
      return "Goiás"
    case .MA:
      return "Maranhão"
    case .MS:
      return "Mato Grosso do Sul"
    case .MT:
      return "Mato Grosso"
    case .MG:
      return "Minas Gerais"
    case .PA:
      return "Pará"
    case .PB:
      return "Paraíba"
    case .PR:
      return "Paraná"
    case .PE:
      return "Pernambuco"
    case .PI:
      return "Piauí"
    case .RN:
      return "Rio Grande do Norte"
    case .RS:
      return "Rio Grande do Sul"
    case .RJ:
      return "Rio de Janeiro"
    case .RO:
      return "Rondônia"
    case .RR:
      return "Roraima"
    case .SC:
      return "Santa Catarina"
    case .SP:
      return "São Paulo"
    case .SE:
      return "Sergipe"
    case .TO:
      return "Tocantins"
    case None:
      return ""
    }
  }
  
  func UF() -> String {
    switch self {
    case .AC:
      return "AC"
    case .AL:
      return "AL"
    case .AP:
      return "AP"
    case .AM:
      return "AM"
    case .BA:
      return "BA"
    case .CE:
      return "CE"
    case .DF:
      return "DF"
    case .ES:
      return "ES"
    case .GO:
      return "GO"
    case .MA:
      return "MA"
    case .MS:
      return "MS"
    case .MT:
      return "MT"
    case .MG:
      return "MG"
    case .PA:
      return "PA"
    case .PB:
      return "PB"
    case .PR:
      return "PR"
    case .PE:
      return "PE"
    case .PI:
      return "PI"
    case .RN:
      return "RN"
    case .RS:
      return "RS"
    case .RJ:
      return "RJ"
    case .RO:
      return "RO"
    case .RR:
      return "RR"
    case .SC:
      return "SC"
    case .SP:
      return "SP"
    case .SE:
      return "SE"
    case .TO:
      return "TO"
    case None:
      return ""
    }
  }
}
