//
//  Circle.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Circle {
  let id: Int
  var name: String
  var inviteCode: String?
  var pictureURL: String?
//  let members = [User]()
//  let suggestedMembers = [User]()
  var photos = [Photo]()
  var numberOfPhotos = 0
  var numberOfMembers = 0
  var numberOfSuggestedMembers = 0
  var remainingMemberCount: Int
  var adminId: Int
  var type: String?

  init(_ API: APIResponse) {
    id = API["id"] as? Int ?? 0
    name = API["name"] as? String ?? ""
    inviteCode = API["code"] as? String
    remainingMemberCount = API["remaining_members_count"] as? Int ?? 0
    adminId = API["admin_user_id"] as? Int ?? 0
    pictureURL = API["profile_image_url"] as? String
    numberOfMembers = API["users_count"] as? Int ?? 0
    numberOfPhotos = API["posts_count"] as? Int ?? 0
    type = API["circle_type"] as? String
  }
}

extension Circle {
  class func getCircleWithId(id: Int, completion: Result<Circle> -> ()) {
    NetworkManager.sharedInstance.circleInfo(id) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(Circle(boxed.unbox)))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func info(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.circleInfo(id) {
      switch $0 {
      case .Success(let boxed):
        self.name = boxed.unbox["name"] as? String ?? self.name
        self.remainingMemberCount = boxed.unbox["remaining_members_count"] as? Int ?? self.remainingMemberCount
        self.adminId = boxed.unbox["admin_user_id"] as? Int ?? self.adminId
        self.pictureURL = boxed.unbox["profile_image_url"] as? String
        self.numberOfPhotos = boxed.unbox["posts_count"] as? Int ?? self.numberOfPhotos
        self.numberOfMembers = boxed.unbox["users_count"] as? Int ?? self.numberOfMembers
        self.numberOfSuggestedMembers = boxed.unbox["pending_users_count"] as? Int ?? self.numberOfSuggestedMembers
        self.inviteCode = boxed.unbox["code"] as? String ?? self.inviteCode
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func posts(page: Int, completion: Result<([Photo], [User], Bool)> -> ()) {
    NetworkManager.sharedInstance.postsForCircleId(id, page: page) {
      switch $0 {
      case .Success(let boxed):
        var posts = [Photo]()
        if let postsAPI = boxed.unbox["posts"] as? [APIResponse] {
          for postAPI in postsAPI {
            if let blocked = postAPI["blocked"] as? Bool where blocked {
              continue
            }
            let photo = Photo(postAPI)
            photo.circle = self
            posts.append(photo)
              
          }
        }
        var users = [User]()
        if let linkedData = boxed.unbox["linked_data"] as? APIResponse {
          if let usersAPI = linkedData["users"] as? [APIResponse] {
            for userAPI in usersAPI {
              users.append(User(userAPI))
            }
          }
        }
        var hasNextPage = false
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        }
        let tuple = (posts, users, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func postsForId(id: Int, page: Int, completion: Result<([Photo], [User], Bool)> -> ()) {
    NetworkManager.sharedInstance.postsForCircleId(id, page: page) {
      switch $0 {
      case .Success(let boxed):
        var posts = [Photo]()
        if let postsAPI = boxed.unbox["posts"] as? [APIResponse] {
          for postAPI in postsAPI {
            if let blocked = postAPI["blocked"] as? Bool where blocked {
              continue
            }
            posts.append(Photo(postAPI))
          }
        }
        var users = [User]()
        if let linkedData = boxed.unbox["linked_data"] as? APIResponse {
          if let usersAPI = linkedData["users"] as? [APIResponse] {
            for userAPI in usersAPI {
              users.append(User(userAPI))
            }
          }
        }
        var hasNextPage = false
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        }
        let tuple = (posts, users, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func members(page: Int, completion: Result<([User], Bool)> -> ()) {
    NetworkManager.sharedInstance.membersForCircleId(id, page: page) {
      switch $0 {
      case .Success(let boxed):
        var members = [User]()
        if let membersAPI = boxed.unbox["members"] as? [APIResponse] {
          for memberAPI in membersAPI {
            members.append(User(memberAPI))
          }
        }
        let hasNextPage: Bool
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let nextPage = pagination["next_page"] as? Int {
          hasNextPage = true
        } else {
          hasNextPage = false
        }
        let tuple = (members, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func createCircle(name: String, image: UIImage?, completion: Result<Circle> -> ()) {
    NetworkManager.sharedInstance.createCircle(name, image: image) {
      switch $0 {
      case .Success(let boxed):
        if let circleAPI = boxed.unbox["friendship_circle"] as? APIResponse {
          let circle = Circle(circleAPI)
          User.currentUser.circles.append(circle)
          completion(Result(circle))
        } else {
          completion(Result(Error(code: -1, domain: "us.hippokrates", description: "unknown error")))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func createStudentCircle(name: String, image: UIImage?, schoolName: String, subject: String?, fromDate: NSDate?, toDate: NSDate?, limit: Int, completion: Result<Circle> -> ()) {
    NetworkManager.sharedInstance.createStudentCircle(name, image: image, schoolName: schoolName, subject: subject, fromDate: fromDate, toDate: toDate, limit: limit) {
      switch $0 {
      case .Success(let boxed):
        if let circleAPI = boxed.unbox["friendship_circle"] as? APIResponse {
          let circle = Circle(circleAPI)
          User.currentUser.circles.append(circle)
          completion(Result(circle))
        } else {
          completion(Result(Error(code: -1, domain: "us.hippokrates", description: "unknown error")))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func inviteUser(userId: Int, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.inviteUser(userId, toCircle: id) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func joinCircle(code: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.joinCircle(code) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func leave(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.leaveCircle(id) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func delete(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.deleteCircle(id) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func removeUser(userId: Int, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.removeUser(userId, fromCircle: id) {
      switch $0 {
      case .Success:
        self.numberOfMembers--
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func getPendingRequestsCount(completion: Result<Int> -> ()) {
    NetworkManager.sharedInstance.getCirclePendingRequests(id, page: 1) {
      switch $0 {
      case .Success(let boxed):
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse, let total = pagination["total_count"] as? Int {
          completion(Result(total))
        } else {
          completion(Result(0))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func getPendingRequests(page: Int, completion: Result<([User], Int, Bool)> -> ()) {
    NetworkManager.sharedInstance.getCirclePendingRequests(id, page: page) {
      switch $0 {
      case .Success(let boxed):
        var pendingMembers = [User]()
        if let pendingMembersAPI = boxed.unbox["pending_moderations"] as? [APIResponse] {
          for pendingMemberAPI in pendingMembersAPI {
            if let userAPI = pendingMemberAPI["user"] as? APIResponse {
              let user = User(userAPI)
              pendingMembers.append(user)
            }
          }
        }
        let hasNextPage: Bool
        let totalCount: Int
        if let meta = boxed.unbox["meta"] as? APIResponse, let pagination = meta["pagination"] as? APIResponse {
          if let nextPage = pagination["next_page"] as? Int {
            hasNextPage = true
          } else {
            hasNextPage = false
          }
          if let count = pagination["total_count"] as? Int {
            totalCount = count
          } else {
            totalCount = 0
          }
        } else {
          hasNextPage = false
          totalCount = 0
        }
        let tuple = (pendingMembers, totalCount, hasNextPage)
        completion(Result(tuple))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func turnCircleAdmin(userId: Int, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.turnCircleAdmin(id, userId: userId) {
      switch $0 {
      case .Success(let boxed):
        self.adminId = userId
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func updateImage(image: UIImage, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.updateCircleImage(id, circleName: name, image: image) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func acceptMembers(ids: [Int], completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.acceptMembers(ids, circleId: id) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func declineMembers(ids: [Int], completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.declineMembers(ids, circleId: id) {
      switch $0 {
      case .Success:
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
}