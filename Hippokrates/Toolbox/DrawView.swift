//
//  DrawView.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum DrawingMode {
  case None, Blur, Paint
}

class DrawView: UIView {
  var previousPoint = CGPointZero
  var currentPoint = CGPointZero
  var image = UIImage()
  var selectedColor = UIColor.whiteColor()
  var selectedSize: CGFloat = 40
  var drawingMode: DrawingMode = .None
  
  // MARK: Drawing
  override func drawRect(rect: CGRect) {
    image.drawInRect(bounds)
  }
  
  func drawLineNew() {
    UIGraphicsBeginImageContext(bounds.size)
    image.drawInRect(bounds)
    
    switch drawingMode {
    case .Paint:
      let context = UIGraphicsGetCurrentContext()
      CGContextSetLineCap(context, kCGLineCapRound)
      CGContextSetStrokeColorWithColor(context, selectedColor.CGColor)
      CGContextSetLineWidth(context, selectedSize)
      CGContextBeginPath(context)
      CGContextMoveToPoint(context, previousPoint.x, previousPoint.y)
      CGContextAddLineToPoint(context, currentPoint.x, currentPoint.y)
      CGContextStrokePath(context)
    case .Blur:
      let tuple = blur()
      tuple.1?.drawInRect(tuple.0)
    case .None:
      break
    }
    
    image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    previousPoint = currentPoint
    
    setNeedsDisplay()
  }
  
  func blur() -> (CGRect, UIImage?) {
    let size: CGFloat = 50
    let rect = CGRect(x: currentPoint.x - size / 2, y: currentPoint.y - size / 2, width: size, height: size)
    
    if let imageView = superview as? UIImageView, let image = imageView.image {
      let scaleX = image.size.width / bounds.width
      let scaleY = image.size.height / bounds.height
      let imageRect = CGRect(x: rect.origin.x * scaleX * UIScreen.mainScreen().scale, y: rect.origin.y * scaleY * UIScreen.mainScreen().scale, width: rect.width * scaleX * UIScreen.mainScreen().scale, height: rect.height * scaleY * UIScreen.mainScreen().scale)
      if let croppedImage = CGImageCreateWithImageInRect(image.CGImage, imageRect) {
        let ciContext = CIContext(options: nil)
        let blurCIImage = CIImage(CGImage: croppedImage)
        let filter = CIFilter(name: "CIGaussianBlur")
        filter.setValue(blurCIImage, forKey: kCIInputImageKey)
        filter.setValue(30, forKey: "inputRadius")
        if let result = filter.valueForKey(kCIOutputImageKey) as? CIImage {
          let cgImage = ciContext.createCGImage(result, fromRect: result.extent())
          return (rect, UIImage(CGImage: cgImage))
        }
      }
    }
    return (rect, nil)
  }
  
  // MARK: Touches
  func handleTouches() {
    switch drawingMode {
    case .Blur:
      drawLineNew()
    case .Paint:
      drawLineNew()
    case .None:
      break
    }
  }
  
  override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
    if let touch = touches.first as? UITouch {
      previousPoint = touch.locationInView(self)
    }
  }
  
  override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
    if let touch = touches.first as? UITouch {
      currentPoint = touch.locationInView(self)
      handleTouches()
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    if let touch = touches.first as? UITouch {
      currentPoint = touch.locationInView(self)
      handleTouches()
    }
  }
  
  // MARK: Snapshot
  func getEditedImage() -> UIImage {
    UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
    layer.superlayer.renderInContext(UIGraphicsGetCurrentContext())
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
}
