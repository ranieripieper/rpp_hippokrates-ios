//
//  Error.swift
//  Networking
//
//  Created by Gilson Gil on 2/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct Error {
  let code: Int
  let domain: String
  let description: String
  
  init(code: Int, domain: String, description: String?) {
    self.code = code
    self.domain = domain
    if let description = description {
      self.description = description
    } else {
      self.description = "unknown error"
    }
  }
}