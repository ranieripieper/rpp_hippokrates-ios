//
//  ios8.swift
//
//  Created by Gilson Gil on 4/1/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

func IOS_8() -> Bool {
  switch UIDevice.currentDevice().systemVersion.compare("8.0.0", options: NSStringCompareOptions.NumericSearch) {
  case .OrderedSame, .OrderedDescending:
    return true
  case .OrderedAscending:
    return false
  }
}
