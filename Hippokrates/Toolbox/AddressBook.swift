//
//  AddressBook.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AddressBook {
  class func getEmails(callback: Result<[(String, String, UIImage?)]> -> ()) {
    swiftAddressBook?.requestAccessWithCompletion() { (success: Bool, error: CFError?) -> () in
      if success {
        var records = [(String, String, UIImage?)]()
        if let people = swiftAddressBook?.allPeople {
          for person in people {
            let emails = person.emails?.map() {
              $0.value
            }
            if emails != nil {
              for email in emails! {
                if let name = person.compositeName {
                  let tuple = (name, email, person.image)
                  records.append(tuple)
                }
              }
            }
          }
        }
        callback(Result(records))
      } else {
        callback(Result(Error(code: 1, domain: "us.hippokrates", description: CFErrorCopyDescription(error!) as String)))
      }
    }
  }
}
