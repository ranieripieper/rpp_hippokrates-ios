//
//  CropView.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CropView: UIView {
  let imageChopperOutsideStillTouchable: CGFloat = 0
  let imageChopperInsideStillEdge: CGFloat = 20
  
  var imageView: UIImageView
  var cropView: UIView?
  
  var topView: UIView?
  var bottomView: UIView?
  var leftView: UIView?
  var rightView: UIView?
  
  var topLeftView: UIView?
  var topRightView: UIView?
  var bottomLeftView: UIView?
  var bottomRightView: UIView?
  
  var imageScale: CGFloat = 1
  
  var isPanning = false
  var currentTouchesCount = 0
  var panTouchPoint: CGPoint?
  var scaleDistance = 0
  weak var currentDragView: UIView?
  
  var crop: CGRect {
    if cropView == nil {
      return CGRectZero
    }
    var frame = cropView!.frame
    frame.origin.x = max(frame.origin.x, 0)
    frame.origin.y = max(frame.origin.y, 0)
    
    return CGRect(x: frame.origin.x / imageScale, y: frame.origin.y / imageScale, width: frame.width / imageScale, height: frame.height / imageScale)
  }
  var unscaledCrop: CGRect {
    return CGRect(x: crop.origin.x * imageScale, y: crop.origin.y * imageScale, width: crop.width * imageScale, height: crop.height * imageScale)
  }
  
  func constraintCropToImage() {
    if cropView == nil {
      return
    }
    var frame = cropView!.frame
    if CGRectEqualToRect(frame, CGRectZero) {
      return
    }
    var change = false
    do {
      change = false
    
      if frame.origin.x < 0 {
        frame.origin.x = 0
        change = true
      }
      
      if frame.width > cropView!.superview!.frame.width {
        frame.size.width = cropView!.superview!.frame.width
        change = true
      }
      
      if frame.width < 20 {
        frame.size.width = 20
        change = true
      }
      
      if frame.origin.x + frame.width > cropView!.superview!.frame.width {
        frame.origin.x = cropView!.superview!.frame.width - frame.width
        change = true
      }
      
      if frame.origin.y < 0 {
        frame.origin.y = 0
        change = true
      }
      
      if frame.height > cropView!.superview!.frame.height {
        frame.size.height = cropView!.superview!.frame.height
        change = true
      }
      
      if frame.height < 20 {
        frame.size.height = 20
        change = true
      }
    
      if frame.origin.y + frame.height > cropView!.superview!.frame.height {
        frame.origin.y = cropView!.superview!.frame.height - frame.height
        change = true
      }
    } while change
    
    cropView!.frame = frame
  }
  
  func updateBounds() {
    constraintCropToImage()
    
    if cropView == nil {
      return
    }
    
    let frame = cropView!.frame
    let x = frame.origin.x
    let y = frame.origin.y
    let width = frame.width
    let height = frame.height
    
    let selfWidth = imageView.frame.width
    let selfHeight = imageView.frame.height
    
    topView!.frame = CGRect(x: x, y: -1, width: width, height: y)
    bottomView!.frame = CGRect(x: x, y: y + height, width: width, height: selfHeight - y - height)
    leftView!.frame = CGRect(x: -1, y: y, width: x + 1, height: height)
    rightView!.frame = CGRect(x: x + width, y: y, width: selfWidth - x - width, height: height)
    
    topLeftView!.frame = CGRect(x: -1, y: -1, width: x + 1, height: y + 1)
    topRightView!.frame = CGRect(x: x + width, y: -1, width: selfWidth - x - width, height: y + 1)
    bottomLeftView!.frame = CGRect(x: -1, y: y + height, width: x + 1, height: selfHeight - y - height)
    bottomRightView!.frame = CGRect(x: x + width, y: y + height, width: selfWidth - x - width, height: selfHeight - y - height)
    
    didChangeValueForKey("crop")
  }
  
  func setCrop(crop: CGRect) {
    cropView!.frame = CGRect(x: crop.origin.x * imageScale, y: crop.origin.y * imageScale, width: crop.width * imageScale, height: crop.height * imageScale)
    updateBounds()
  }
  
  func newEdgeView() -> UIView {
    var view = UIView()
    view.backgroundColor = UIColor.blackColor()
    view.alpha = 0.5
    imageView.addSubview(view)
    return view
  }
  
  class func initialCropViewForImageView(imageView: UIImageView) -> UIView {
    // 3/4 the size, centered
    let max = imageView.bounds
    let size = max.width / 4 * 3
    let x = (max.width - size) / 2
    let y = (max.height - size) / 2
    
    var cropView = UIView(frame: CGRect(x: x, y: y, width: size, height: size))
    cropView.layer.borderColor = UIColor.whiteColor().CGColor
    cropView.layer.borderWidth = 2
    cropView.backgroundColor = UIColor.clearColor()
    cropView.alpha = 0.4
    return cropView
  }
  
  func setup() {
    userInteractionEnabled = true
    multipleTouchEnabled = true
    backgroundColor = UIColor.clearColor()
    
    cropView = CropView.initialCropViewForImageView(imageView)
    imageView.addSubview(cropView!)
    
    topView = newEdgeView()
    bottomView = newEdgeView()
    leftView = newEdgeView()
    rightView = newEdgeView()
    topLeftView = newEdgeView()
    topRightView = newEdgeView()
    bottomLeftView = newEdgeView()
    bottomRightView = newEdgeView()
    
    updateBounds()
  }
  
  func calcFrameWithImage(image: UIImage, andMaxSize maxSize: CGSize) -> CGRect {
    let increase = imageChopperOutsideStillTouchable * 2
    let noScale = CGRect(x: 0, y: 0, width: image.size.width + increase, height: image.size.height + increase)
    if noScale.width <= maxSize.width && noScale.height <= maxSize.height {
      imageScale = 1
      return noScale
    }
    var scaled: CGRect
    
    imageScale = (maxSize.height - increase) / image.size.height
    scaled = CGRect(x: 0, y: 0, width: image.size.width * imageScale + increase, height: image.size.height * imageScale + increase)
    if scaled.width <= maxSize.width && scaled.height <= maxSize.height {
      return scaled
    }
    
    imageScale = (maxSize.width - increase) / image.size.width
    scaled = CGRect(x: 0, y: 0, width: image.size.width * imageScale + increase, height: image.size.height * imageScale + increase)
    return scaled
  }
  
  required init(coder aDecoder: NSCoder) {
    imageView = UIImageView(frame: CGRectZero)
    super.init(coder: aDecoder)
    imageView.frame = CGRectInset(bounds, imageChopperOutsideStillTouchable, imageChopperOutsideStillTouchable)
    addSubview(imageView)
    setup()
  }
  
  override init(frame: CGRect) {
    imageView = UIImageView(frame: frame)
    super.init(frame: frame)
    imageView.frame = CGRectInset(bounds, imageChopperOutsideStillTouchable, imageChopperOutsideStillTouchable)
    addSubview(imageView)
    setup()
  }
  
  init(image: UIImage) {
    imageView = UIImageView(image: image)
    super.init(frame: CGRectZero)
    frame = CGRectInset(imageView.frame, -imageChopperOutsideStillTouchable, -imageChopperOutsideStillTouchable)
    addSubview(imageView)
    setup()
  }
  
  init(image: UIImage, andMaxSize maxSize: CGSize) {
    imageView = UIImageView(frame: CGRectZero)
    super.init(frame: CGRectZero)
    frame = calcFrameWithImage(image, andMaxSize: maxSize)
    imageView.frame = CGRectInset(bounds, imageChopperOutsideStillTouchable, imageChopperOutsideStillTouchable)
    imageView.image = image
    addSubview(imageView)
    setup()
  }
  
//  override init() {
//    imageView = UIImageView(frame: CGRectZero)
//    super.init()
//    setup()
//  }
  
  func distanceBetweenTwoPoints(fromPoint: CGPoint, toPoint: CGPoint) -> CGFloat {
    let x = toPoint.x - fromPoint.x
    let y = toPoint.y - fromPoint.y
    return sqrt(x * x + y * y)
  }
  
  override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
    willChangeValueForKey("crop")
    if let allTouches = event.allTouches() {
      switch allTouches.count {
      case 1:
        currentTouchesCount = 1
        isPanning = false
        let insetAmount = imageChopperInsideStillEdge
        if let touch = allTouches.first as? UITouch {
          let touchPoint = touch.locationInView(imageView)
          if CGRectContainsPoint(CGRectInset(cropView!.frame, insetAmount, insetAmount), touchPoint) {
            isPanning = true
            panTouchPoint = touchPoint
            return
          }
          
          var frame = cropView!.frame
          let x = touchPoint.x
          let y = touchPoint.y
          
          currentDragView = nil
          
          if CGRectContainsPoint(CGRectInset(topLeftView!.frame, -insetAmount, -insetAmount), touchPoint) {
            currentDragView = topLeftView
            
            if CGRectContainsPoint(topLeftView!.frame, touchPoint) {
              frame.size.width += frame.origin.x - x
              frame.size.height += frame.origin.y - y
              frame.origin = touchPoint
            }
          } else if CGRectContainsPoint(CGRectInset(topRightView!.frame, -insetAmount, -insetAmount), touchPoint) {
            currentDragView = topRightView
            
            if CGRectContainsPoint(topRightView!.frame, touchPoint) {
              frame.size.height += frame.origin.y - y
              frame.origin.y = y
              frame.size.width = x - frame.origin.x
            }
          } else if CGRectContainsPoint(CGRectInset(bottomLeftView!.frame, -insetAmount, -insetAmount), touchPoint) {
            currentDragView = bottomLeftView
            
            if CGRectContainsPoint(bottomLeftView!.frame, touchPoint) {
              frame.size.width += frame.origin.x - x
              frame.size.height = y - frame.origin.y
              frame.origin.x = x
            }
          } else if CGRectContainsPoint(CGRectInset(bottomRightView!.frame, -insetAmount, -insetAmount), touchPoint) {
            currentDragView = bottomRightView
            
            if CGRectContainsPoint(bottomRightView!.frame, touchPoint) {
              frame.size.width = x - frame.origin.x
              frame.size.height = y - frame.origin.y
            }
          } else if CGRectContainsPoint(CGRectInset(topView!.frame, 0, -insetAmount), touchPoint) {
            currentDragView = topView
            
            if CGRectContainsPoint(topView!.frame, touchPoint) {
              frame.size.height += frame.origin.y - y
              frame.origin.y = y
            }
          } else if CGRectContainsPoint(CGRectInset(bottomView!.frame, 0, -insetAmount), touchPoint) {
            currentDragView = bottomView
            
            if CGRectContainsPoint(bottomView!.frame, touchPoint) {
              frame.size.height = y - frame.origin.y
            }
          } else if CGRectContainsPoint(CGRectInset(leftView!.frame, -insetAmount, 0), touchPoint) {
            currentDragView = leftView
            
            if CGRectContainsPoint(leftView!.frame, touchPoint) {
              frame.size.width += frame.origin.x - x
              frame.origin.x = x
            }
          } else if CGRectContainsPoint(CGRectInset(rightView!.frame, -insetAmount, 0), touchPoint) {
            currentDragView = rightView
            
            if CGRectContainsPoint(rightView!.frame, touchPoint) {
              frame.size.width = x - frame.origin.x
            }
          }
          
          cropView!.frame = frame
          updateBounds()
        }
      case 2:
        if allTouches.count > 1 {
          if let touch1 = allTouches.first as? UITouch, let touch2 = Array(allTouches)[1] as? UITouch {
            let point1 = touch1.locationInView(imageView)
            let point2 = touch2.locationInView(imageView)
            if currentTouchesCount == 0 && CGRectContainsPoint(cropView!.frame, point1) && CGRectContainsPoint(cropView!.frame, point2) {
              isPanning = true
            }
          }
        }
        currentTouchesCount = allTouches.count
      default:
        break
      }
    }
  }
  
  override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
    willChangeValueForKey("crop")
    if let allTouches = event.allTouches() {
      switch allTouches.count {
      case 1:
        if let touch = allTouches.first as? UITouch {
          let touchPoint = touch.locationInView(imageView)
          if isPanning {
            let touchCurrent = touch.locationInView(imageView)
            let x = touchCurrent.x - panTouchPoint!.x
            let y = touchCurrent.y - panTouchPoint!.y
            
            cropView!.center = CGPoint(x: cropView!.center.x + x, y: cropView!.center.y + y)
            panTouchPoint = touchCurrent
          } else if CGRectContainsPoint(bounds, touchPoint) {
            var frame = cropView!.frame
            let x = min(touchPoint.x, imageView.frame.width)
            let y = min(touchPoint.y, imageView.frame.height)
            
            if currentDragView == topView {
              let diff = frame.origin.y - y
              frame.size.height += diff
              frame.origin.y = y
              let widthDiff = diff
              frame.size.width += widthDiff
              frame.origin.x -= widthDiff / 2
            } else if currentDragView == bottomView {
              let diff = y - frame.origin.y
              frame.size.height = diff
              let width = diff
              let widthDiff = width - frame.size.width
              frame.size.width = width
              frame.origin.x -= widthDiff / 2
            } else if currentDragView == leftView {
              let diff = frame.origin.x - x
              frame.size.width += diff
              frame.origin.x = x
              let heightDiff = diff
              frame.size.height += heightDiff
              frame.origin.y -= heightDiff / 2
            } else if currentDragView == rightView {
              let diff = x - frame.origin.x
              frame.size.width = diff
              let height = diff
              let heightDiff = height - frame.size.height
              frame.size.height = height
              frame.origin.y -= heightDiff / 2
            } else if currentDragView == topLeftView {
//              frame.size.width += frame.origin.x - x
//              frame.size.height += frame.origin.y - y
//              frame.origin = touchPoint
            } else if currentDragView == topRightView {
//              frame.size.height += frame.origin.y - y
//              frame.origin.y = y
//              frame.size.width = x - frame.origin.x
            } else if currentDragView == bottomLeftView {
//              frame.size.width += frame.origin.x - x
//              frame.size.height = y - frame.origin.y
//              frame.origin.x = x
            } else if currentDragView == bottomRightView {
//              frame.size.width = x - frame.origin.x
//              frame.size.height = y - frame.origin.y
            }
            cropView!.frame = frame
          }
        }
      case 2:
        if let touch1 = Array(allTouches)[0] as? UITouch, let touch2 = Array(allTouches)[1] as? UITouch {
          let point1 = touch1.locationInView(imageView)
          let point2 = touch2.locationInView(imageView)
          
          if isPanning {
            let distance = distanceBetweenTwoPoints(point1, toPoint: point2)
            
            if scaleDistance != 0 {
              let scale = 1 + (distance - CGFloat(scaleDistance)) / CGFloat(scaleDistance)
              let originalCenter = cropView!.center
              let originalSize = cropView!.frame.size
              let newSize = CGSize(width: originalSize.width * scale, height: originalSize.height * scale)
              if newSize.width >= 50 && newSize.height >= 50 && newSize.width <= cropView!.superview!.frame.width && newSize.height <= cropView!.superview!.frame.height {
                cropView!.frame = CGRect(origin: CGPointZero, size: newSize)
                cropView!.center = originalCenter
              }
            }
            scaleDistance = Int(distance)
          } else if currentDragView == topLeftView || currentDragView == topRightView || currentDragView == bottomLeftView || currentDragView == bottomRightView {
            let x = min(point1.x, point2.x)
            let y = min(point1.y, point2.y)
            let width = max(point1.x, point2.x) - x
            let height = max(point1.y, point2.y) - y
            cropView!.frame = CGRect(x: x, y: y, width: width, height: height)
          } else if currentDragView == topView || currentDragView == bottomView {
            let y = min(point1.y, point2.y)
            let height = max(point1.y, point2.y) - y
            if height > 30 || cropView!.frame.height < 45 {
              cropView!.frame = CGRect(x: cropView!.frame.origin.x, y: y, width: cropView!.frame.width, height: height)
            }
          } else if currentDragView == leftView || currentDragView == rightView {
            let x = min(point1.x, point2.x)
            let width = max(point1.x, point2.x) - x
            if width > 30 || cropView!.frame.width < 45 {
              cropView!.frame = CGRect(x: x, y: cropView!.frame.origin.y, width: width, height: cropView!.frame.height)
            }
          }
        }
      default:
        break
      }
      updateBounds()
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    scaleDistance = 0
    currentTouchesCount = event.allTouches()?.count ?? 0
  }
  
  func croppedImage() -> UIImage {
    let rect = crop
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    let drawRect = CGRect(x: -rect.origin.x, y: -rect.origin.y, width: imageView.image!.size.width, height: imageView.image!.size.height)
    CGContextClipToRect(context, CGRect(origin: CGPointZero, size: rect.size))
    imageView.image!.drawInRect(drawRect)
    let croppedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    let newSize = CGSize(width: 700, height: 700 * croppedImage.size.height / croppedImage.size.width)
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
    croppedImage.drawInRect(CGRect(origin: CGPointZero, size: newSize))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage
  }
}
