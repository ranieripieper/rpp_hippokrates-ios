//
//  ImagePicker.swift
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ImagePicker: NSObject {
  var viewController: UIViewController
  var callback: UIImage? -> ()
  
  init(viewController: UIViewController, callback: UIImage? -> ()) {
    self.viewController = viewController
    self.callback = callback
  }
  
  func presentImagePicker() {
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    dispatch_async(dispatch_get_main_queue()) {
      if UIImagePickerController.isCameraDeviceAvailable(.Rear) || UIImagePickerController.isCameraDeviceAvailable(.Front) {
        let alertView = AlertView(buttons: [("Camera", {
          dispatch_async(dispatch_get_main_queue()) {
            imagePicker.sourceType = .Camera
            imagePicker.allowsEditing = true
            self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
          }
        }), ("Biblioteca", {
          dispatch_async(dispatch_get_main_queue()) {
            imagePicker.sourceType = .PhotoLibrary
            imagePicker.allowsEditing = true
            self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
          }
        })], cancelButtonTitle: "Cancelar")
        alertView.callback = {
          self.callback(nil)
        }
        alertView.alertInViewController(self.viewController)
      } else {
        dispatch_async(dispatch_get_main_queue()) {
          imagePicker.sourceType = .PhotoLibrary
          imagePicker.allowsEditing = true
          self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
        }
      }
    }
  }
}

extension ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
      let newSize = CGSize(width: 300, height: 300)
      UIGraphicsBeginImageContextWithOptions(newSize, false, 1)
      image.drawInRect(CGRect(origin: CGPointZero, size: newSize))
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      callback(newImage)
      viewController.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    callback(nil)
    viewController.dismissViewControllerAnimated(true, completion: nil)
  }
}