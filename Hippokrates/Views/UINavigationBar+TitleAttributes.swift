//
//  UINavigationBar+TitleAttributes.swift
//  Hippokrates
//
//  Created by Gilson Gil on 7/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

extension UINavigationBar {
  public override func awakeFromNib() {
    super.awakeFromNib()
    titleTextAttributes = [NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 20)!]
  }
}
