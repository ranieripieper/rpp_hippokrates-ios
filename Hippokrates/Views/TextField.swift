//
//  TextField.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

@IBDesignable
class TextField: UITextField {
  @IBInspectable var image: UIImage?
  
  let imagePadding: CGFloat = 16
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setPaddings()
    setBorder()
    setPlaceholderStyle()
  }
  
  override func drawRect(rect: CGRect) {
    image?.drawInRect(CGRect(x: imagePadding, y: (bounds.height - (image?.size.height ?? 0)) / 2, width: image?.size.width ?? 0, height: image?.size.height ?? 0))
  }
  
  func setPlaceholderStyle() {
    attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue-Light", size: font.pointSize)!])
  }
  
  func setBorder() {
    layer.borderWidth = 1
    layer.borderColor = UIColor(red: 215 / 255, green: 215 / 255, blue: 215 / 255, alpha: 1).CGColor
    layer.cornerRadius = 4
  }
  
  func setPaddings() {
    setPaddings(imagePadding * 2 + (image?.size.width ?? 20))
  }
  
  func setPaddings(width: CGFloat) {
    leftView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: width, height: bounds.height)))
    leftViewMode = .Always
    rightView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 20, height: bounds.height)))
    rightViewMode = .Always
  }
}
