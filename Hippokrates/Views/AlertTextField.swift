//
//  AlertTextField.swift
//  Hippokrates
//
//  Created by Gilson Gil on 6/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AlertTextField: UIView {
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var nameTextField: UITextField?
  @IBOutlet weak var emailTextField: UITextField?
  @IBOutlet weak var commentTextView: UITextView?
  @IBOutlet weak var commentTextViewHeightConstraint: NSLayoutConstraint?
  @IBOutlet weak var inviteTopConstraint: NSLayoutConstraint?
  
  var callback: ((String?, String?) -> ())?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setTranslatesAutoresizingMaskIntoConstraints(false)
    let tap = UITapGestureRecognizer(target: self, action: "tapped:")
    addGestureRecognizer(tap)
  }
  
  class func inviteTextField(callback: ((String?, String?) -> ())?) -> AlertTextField {
    let alertTextField = R.nib.inviteAlertTextField.firstView(self, options: nil)!
    alertTextField.callback = callback
    NSNotificationCenter.defaultCenter().addObserver(alertTextField, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
    alertTextField.present()
    return alertTextField
  }
  
  class func editCommentTextField(initialComment: String, callback: ((String?, String?) -> ())?) -> AlertTextField {
    let alertTextField = R.nib.editCommentAlertTextField.firstView(self, options: nil)!
    alertTextField.commentTextView?.text = initialComment
    alertTextField.callback = callback
    NSNotificationCenter.defaultCenter().addObserver(alertTextField, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
    alertTextField.present()
    return alertTextField
  }
  
  func present() {
    alpha = 0
    let window = UIApplication.sharedApplication().delegate?.window
    window??.addSubview(self)
    frame = window??.bounds ?? CGRectZero
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[view]-(0)-|", options: .allZeros, metrics: nil, views: ["view": self])
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(0)-[view]-(0)-|", options: .allZeros, metrics: nil, views: ["view": self])
    window??.addConstraints(horizontalConstraints)
    window??.addConstraints(verticalConstraints)
    transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
    if let nameTextField = self.nameTextField {
      nameTextField.becomeFirstResponder()
    } else if let commentTextView = self.commentTextView {
      commentTextView.becomeFirstResponder()
    }
    UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10, options: .CurveEaseIn, animations: {
      self.transform = CGAffineTransformIdentity
      self.alpha = 1
      }, completion: nil)
  }
  
  func dismiss() {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
      self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      self.alpha = 0
    }) { finished in
      self.removeFromSuperview()
    }
  }
  
  func keyboardWillShow(notification: NSNotification) {
    if let userInfo = notification.userInfo, let frameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let height = frameValue.CGRectValue().height
      commentTextViewHeightConstraint?.constant = bounds.height - 64 - 30 - 30 - height
      if UIScreen.mainScreen().bounds.height == 480 {
        inviteTopConstraint?.constant = 20
      }
      layoutIfNeeded()
    }
  }
  
  // MARK: Tapped
  func tapped(sender: UITapGestureRecognizer) {
    dismiss()
  }
  
  // MARK: Done
  @IBAction func okTapped(sender: UIButton) {
    if validateFields() {
      if let name = nameTextField?.text, let email = emailTextField?.text {
        callback?(name, email)
      } else if let comment = commentTextView?.text {
        callback?(comment, nil)
      }
      dismiss()
    }
  }
  
  func validateFields() -> Bool {
    if let name = nameTextField?.text, let email = emailTextField?.text {
      return count(name) > 0 && count(email) > 0
    } else if let comment = commentTextView?.text {
      return count(comment) > 0
    } else {
      return false
    }
  }
}

extension AlertTextField: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if let nextTextField = viewWithTag(textField.tag + 1) {
      nextTextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
    }
    return true
  }
}
