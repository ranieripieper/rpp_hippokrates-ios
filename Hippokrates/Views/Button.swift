//
//  Button.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

@IBDesignable
class Button: UIButton {
  @IBInspectable var title: String!
  @IBInspectable var subtitle: String!
  @IBInspectable var image: UIImage!
  @IBInspectable var titleColor: UIColor! = UIColor.whiteColor()
  @IBInspectable var subtitleColor: UIColor! = UIColor.whiteColor()
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func drawRect(rect: CGRect) {
    let padding: CGFloat = 30
    let titleHeight: CGFloat = 38
    let subtitleHeight: CGFloat = (bounds.height - titleHeight) / 2 + 8
    (title as NSString).drawInRect(CGRect(x: padding, y: (bounds.height - titleHeight) / 2 - 8, width: bounds.width - 3 * padding, height: titleHeight), withAttributes: [NSForegroundColorAttributeName: titleColor, NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 30)!])
    var paragraph = NSMutableParagraphStyle()
    paragraph.lineBreakMode = .ByWordWrapping
    (subtitle as NSString).drawInRect(CGRect(x: padding, y: (bounds.height + titleHeight) / 2 - 8, width: bounds.width - 2.5 * padding, height: subtitleHeight), withAttributes: [NSForegroundColorAttributeName: subtitleColor, NSFontAttributeName: UIFont(name: "HelveticaNeue-Light", size: 14)!, NSParagraphStyleAttributeName: paragraph])
    image.drawInRect(CGRect(x: bounds.width - 16 - 31, y: 16, width: 31, height: 31))
  }
}
