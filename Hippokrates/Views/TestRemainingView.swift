//
//  TestRemainingView.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TestRemainingView: UIView {
  var days: Int?
  
  class func installInNavigationItem(navigationItem: UINavigationItem, days: Int?) {
    if days != nil {
      navigationItem.rightBarButtonItems = [UIBarButtonItem.negativeSpace(), UIBarButtonItem(customView: TestRemainingView(days: days!))]
    }
  }
  
  init(days: Int) {
    self.days = days
    super.init(frame: CGRect(x: 16, y: 0, width: 130, height: 44))
    backgroundColor = UIColor.clearColor()
  }

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    frame = CGRect(x: 16, y: 0, width: 130, height: 44)
    backgroundColor = UIColor.clearColor()
  }
  
  override func drawRect(rect: CGRect) {
    var paragraph = NSMutableParagraphStyle()
    paragraph.alignment = .Right
    let lineHeight: CGFloat = 12
    let bottomPadding: CGFloat = 8
    let attributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSBackgroundColorAttributeName: UIColor.clearColor(), NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 12)!, NSParagraphStyleAttributeName: paragraph]
    ("Você tem" as NSString).drawInRect(CGRect(x: 0, y: bounds.height - 2 * lineHeight - bottomPadding, width: bounds.width - 38, height: lineHeight), withAttributes: attributes)
    ("\(days!) dias de teste" as NSString).drawInRect(CGRect(x: 0, y: bounds.height - lineHeight - bottomPadding, width: bounds.width - 38, height: lineHeight), withAttributes: attributes)
    let image = R.image.icn_home_countdown!
    image.drawInRect(CGRect(x: bounds.width - image.size.width - 8, y: (bounds.height - image.size.height) / 2 + 4, width: image.size.width, height: image.size.height))
  }
}

extension UIBarButtonItem {
  class func negativeSpace() -> UIBarButtonItem {
    let negativeSpace = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
    negativeSpace.width = -16
    return negativeSpace
  }
}
