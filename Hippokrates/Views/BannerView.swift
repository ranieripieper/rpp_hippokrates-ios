//
//  BannerView.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BannerView: UIView {
  let minHeight: CGFloat = 70
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  class func present(text: String, inViewController viewController: UIViewController) {
    let bannertView = BannerView(text: text)
    bannertView.presentInViewController(viewController)
  }
  
  init(text: String) {
    super.init(frame: CGRect(origin: CGPointZero, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: 100)))
    backgroundColor = UIColor(red: 51/255, green: 112/255, blue: 151/255, alpha: 1)
    
    var labelFrame = CGRect(x: 12, y: 12, width: UIScreen.mainScreen().bounds.width - 2 * 12, height: 0)
    
    let height = (text as NSString).boundingRectWithSize(labelFrame.size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 16)!], context: nil).height
    labelFrame.size.height = height
    
    frame = CGRect(origin: CGPointZero, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: max(height + 2 * 12, minHeight)))
    labelFrame.origin.y = (frame.height - height) / 2
    
    let label = UILabel(frame: labelFrame)
    label.backgroundColor = UIColor.clearColor()
    label.textColor = UIColor.whiteColor()
    label.numberOfLines = 0
    label.font = UIFont(name: "HelveticaNeue", size: 16)
    label.text = text
    addSubview(label)
  }
  
  func presentInViewController(viewController: UIViewController) {
    frame = CGRect(x: 0, y: -bounds.height, width: bounds.width, height: bounds.height)
    viewController.view.addSubview(self)
    UIView.animateKeyframesWithDuration(0.3, delay: 0.0, options: .AllowUserInteraction, animations: {
      self.frame = CGRect(x: 0, y: 64, width: self.bounds.width, height: self.bounds.height)
    }) { finished in
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC * 3)), dispatch_get_main_queue()) {
        self.dismiss()
      }
    }
  }
  
  func dismiss() {
    UIView.animateKeyframesWithDuration(0.3, delay: 0.0, options: .AllowUserInteraction, animations: {
      self.frame = CGRect(x: 0, y: -self.bounds.height, width: self.bounds.width, height: self.bounds.height)
    }) { finished in
      self.removeFromSuperview()
    }
  }
}
