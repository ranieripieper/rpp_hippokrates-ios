//
//  LogoView.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LogoView: UIView {
  override func drawRect(rect: CGRect) {
    ("Hippokrates" as NSString).drawInRect(CGRect(x: 0, y: 14, width: bounds.width, height: bounds.height), withAttributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSBackgroundColorAttributeName: UIColor.clearColor(), NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 20)!])
  }
}
