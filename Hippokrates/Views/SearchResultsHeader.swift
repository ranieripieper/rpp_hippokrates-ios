//
//  SearchResultsHeader.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultsHeader: UITableViewHeaderFooterView {
  @IBOutlet weak var label: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    contentView.backgroundColor = UIColor(red: 246 / 255, green: 246 / 255, blue: 246 / 255, alpha: 1)
  }
  
  func configureWithTerm(term: String) {
    label.text = "Resultados para \"\(term)\""
  }
}
