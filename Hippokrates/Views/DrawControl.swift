//
//  DrawControl.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol DrawControlDelegate {
  func blurTapped()
  func pencilTapped()
  func resetTapped()
}

class DrawControl: UIView {
  var delegate: DrawControlDelegate?
  
  @IBAction func blurTapped(sender: UIButton) {
    delegate?.blurTapped()
  }
  
  @IBAction func pencilTapped(sender: UIButton) {
    delegate?.pencilTapped()
  }
  
  @IBAction func resetTapped(sender: UIButton) {
    delegate?.resetTapped()
  }
}
