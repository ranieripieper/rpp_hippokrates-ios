//
//  PencilControl.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol PencilControlDelegate {
  func pencilControlCanceled()
  func pencilControlSelectedColor(color: UIColor)
  func pencilControlSelectedSize(size: CGFloat)
}

class BlurControl: UIView {
  var delegate: PencilControlDelegate?
  
  // MARK: Cancel
  @IBAction func cancelTapped(sender: UIButton) {
    delegate?.pencilControlCanceled()
  }
}

class PencilControl: BlurControl {
  @IBOutlet weak var bigPencilButton: UIButton!
  @IBOutlet weak var smallPencilButton: UIButton!
  @IBOutlet weak var whiteButton: UIButton!
  @IBOutlet weak var blackButton: UIButton!
  @IBOutlet weak var yellowButton: UIButton!
  @IBOutlet weak var redButton: UIButton!
  @IBOutlet weak var blueButton: UIButton!
  @IBOutlet weak var greenButton: UIButton!
  
  var selectedColor = UIColor.whiteColor()
  var pencilSize: CGFloat = 40
  let borderColor = UIColor(red: 232 / 255, green: 111 / 255, blue: 102 / 255, alpha: 1).CGColor
  
  override func awakeFromNib() {
    super.awakeFromNib()
    bigPencilButton.layer.borderColor = borderColor
    smallPencilButton.layer.borderColor = UIColor.whiteColor().CGColor
    whiteButton.layer.borderColor = borderColor
    blackButton.layer.borderColor = UIColor.clearColor().CGColor
    yellowButton.layer.borderColor = UIColor.clearColor().CGColor
    redButton.layer.borderColor = UIColor.clearColor().CGColor
    blueButton.layer.borderColor = UIColor.clearColor().CGColor
    greenButton.layer.borderColor = UIColor.clearColor().CGColor
  }
  
  // MARK: Pencil size
  @IBAction func bigPencilTapped(sender: UIButton) {
    pencilSize = 40
    delegate?.pencilControlSelectedSize(pencilSize)
    smallPencilButton.layer.borderColor = UIColor.clearColor().CGColor
    bigPencilButton.layer.borderColor = borderColor
  }
  
  @IBAction func smallPencilTapped(sender: UIButton) {
    pencilSize = 15
    delegate?.pencilControlSelectedSize(pencilSize)
    smallPencilButton.layer.borderColor = borderColor
    bigPencilButton.layer.borderColor = UIColor.clearColor().CGColor
  }
  
  // MARK: Pencil color
  @IBAction func whiteTapped(sender: UIButton) {
    selectedColor = UIColor.whiteColor()
    delegate?.pencilControlSelectedColor(selectedColor)
    highlightButton(sender)
  }
  
  @IBAction func blackTapped(sender: UIButton) {
    selectedColor = UIColor.blackColor()
    delegate?.pencilControlSelectedColor(selectedColor)
    highlightButton(sender)
  }
  
  @IBAction func yellowTapped(sender: UIButton) {
    selectedColor = UIColor.yellowColor()
    delegate?.pencilControlSelectedColor(selectedColor)
    highlightButton(sender)
  }
  
  @IBAction func redTapped(sender: UIButton) {
    selectedColor = UIColor.redColor()
    delegate?.pencilControlSelectedColor(selectedColor)
    highlightButton(sender)
  }
  
  @IBAction func blueTapped(sender: UIButton) {
    selectedColor = UIColor(red: 0 / 255, green: 122 / 255, blue: 255 / 255, alpha: 1)
    delegate?.pencilControlSelectedColor(selectedColor)
    highlightButton(sender)
  }
  
  @IBAction func greenTapped(sender: UIButton) {
    selectedColor = UIColor.greenColor()
    delegate?.pencilControlSelectedColor(selectedColor)
    highlightButton(sender)
  }
  
  func highlightButton(button: UIButton) {
    whiteButton.layer.borderColor = UIColor.clearColor().CGColor
    blackButton.layer.borderColor = UIColor.clearColor().CGColor
    yellowButton.layer.borderColor = UIColor.clearColor().CGColor
    redButton.layer.borderColor = UIColor.clearColor().CGColor
    blueButton.layer.borderColor = UIColor.clearColor().CGColor
    greenButton.layer.borderColor = UIColor.clearColor().CGColor
    
    button.layer.borderColor = borderColor
  }
}
