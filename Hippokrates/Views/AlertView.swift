//
//  AlertView.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

typealias ButtonType = (String, (() -> ())?)

class AlertView: UIView {
  var buttons: [ButtonType]?
  var cancelButton: ButtonType?
  var timer: NSTimer?
  lazy var contentView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor(red: 51 / 255, green: 112 / 255, blue: 151 / 255, alpha: 1)
    return view
    }()
  var callback: (() -> ())?
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  convenience init(text: String, callback: (() -> ())?) {
    self.init(text: text, horizontalPadding: 40, verticalPadding: 70, buttonHeight: 0)
    self.callback = callback
    timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: "dismiss", userInfo: nil, repeats: false)
  }
  
  convenience init(text: String, buttons: [ButtonType]?) {
    let buttonHeight: CGFloat = 70
    self.init(text: text, horizontalPadding: 20, verticalPadding: 20, buttonHeight: buttonHeight)
    
    self.buttons = buttons
    if buttons != nil && buttons!.count > 0 {
      for var i = 0; i < buttons!.count; i++ {
        let uibutton = UIButton.buttonWithType(.Custom) as! UIButton
        uibutton.frame = CGRect(x: CGFloat(i) * contentView.frame.width / 2, y: contentView.frame.height - buttonHeight, width: contentView.frame.width / 2, height: buttonHeight)
        uibutton.tag = i + 1
        uibutton.addTarget(self, action: "touchUpInside:", forControlEvents: .TouchUpInside)
        var attributes: [String: AnyObject] {
          if i == 0 {
            return [NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
          } else {
            return [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 16)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
          }
        }
        let attributedText = NSAttributedString(string: buttons![i].0, attributes: attributes)
        uibutton.setAttributedTitle(attributedText, forState: .Normal)
        contentView.addSubview(uibutton)
      }
    }
  }
  
  init(text: String, horizontalPadding: CGFloat, verticalPadding: CGFloat, buttonHeight: CGFloat) {
    let width = UIScreen.mainScreen().bounds.width - 2 * 20
//    super.init(frame: CGRect(origin: CGPointZero, size: CGSize(width: width, height: 100)))
    super.init(frame: UIScreen.mainScreen().bounds)
    contentView.frame = CGRect(origin: CGPointZero, size: CGSize(width: width, height: 100))
//    backgroundColor = UIColor(red: 51/255, green: 112/255, blue: 151/255, alpha: 1)
    
    var labelFrame = CGRect(x: horizontalPadding, y: verticalPadding, width: width - 2 * horizontalPadding, height: 0)
    
    let height = (text as NSString).boundingRectWithSize(labelFrame.size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 16)!], context: nil).height
    labelFrame.size.height = height
    
    contentView.frame = CGRect(origin: CGPointZero, size: CGSize(width: width, height: height + verticalPadding * 2 + buttonHeight))
    
    let label = UILabel(frame: labelFrame)
    label.backgroundColor = UIColor.clearColor()
    label.textColor = UIColor.whiteColor()
    label.numberOfLines = 0
    label.textAlignment = .Center
    label.font = UIFont(name: "HelveticaNeue", size: 16)
    label.text = text
    contentView.addSubview(label)
    
    addSubview(contentView)
    let tap = UITapGestureRecognizer(target: self, action: "tapped")
    addGestureRecognizer(tap)
  }
  
  init(buttons: [ButtonType]?, cancelButtonTitle: String?) {
    let buttonHeight: CGFloat = 70
//    super.init(frame: CGRect(origin: CGPointZero, size: CGSize(width: UIScreen.mainScreen().bounds.width - CGFloat(2 * 20), height: CGFloat(buttons?.count ?? 0) * buttonHeight + CGFloat(cancelButtonTitle != nil ? buttonHeight : 0))))
    super.init(frame: UIScreen.mainScreen().bounds)
    contentView.frame = CGRect(origin: CGPointZero, size: CGSize(width: UIScreen.mainScreen().bounds.width - CGFloat(2 * 20), height: CGFloat(buttons?.count ?? 0) * buttonHeight + CGFloat(cancelButtonTitle != nil ? buttonHeight : 0)))
    contentView.backgroundColor = UIColor(red: 51/255, green: 112/255, blue: 151/255, alpha: 1)
    self.buttons = buttons
    if buttons != nil && buttons!.count > 0 {
      for var i = 0; i < buttons!.count; i++ {
        let uibutton = UIButton.buttonWithType(.Custom) as! UIButton
        uibutton.frame = CGRect(x: 0, y: CGFloat(i) * buttonHeight, width: contentView.frame.width, height: buttonHeight)
        uibutton.tag = i + 1
        uibutton.addTarget(self, action: "touchUpInside:", forControlEvents: .TouchUpInside)
        let attributedText = NSAttributedString(string: buttons![i].0, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 16)!, NSForegroundColorAttributeName: UIColor.whiteColor()])
        uibutton.setAttributedTitle(attributedText, forState: .Normal)
        contentView.addSubview(uibutton)
      }
    }
    if cancelButtonTitle != nil {
      let uibutton = UIButton.buttonWithType(.Custom) as! UIButton
      uibutton.frame = CGRect(x: 0, y: contentView.frame.height - buttonHeight, width: contentView.frame.width, height: buttonHeight)
      uibutton.tag == 0
      uibutton.addTarget(self, action: "touchUpInside:", forControlEvents: .TouchUpInside)
      let attributedText = NSAttributedString(string: cancelButtonTitle!, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 16)!, NSForegroundColorAttributeName: UIColor.whiteColor()])
      uibutton.setAttributedTitle(attributedText, forState: .Normal)
      contentView.addSubview(uibutton)
      cancelButton = (cancelButtonTitle!, nil)
    }
    addSubview(contentView)
    let tap = UITapGestureRecognizer(target: self, action: "tapped")
    addGestureRecognizer(tap)
  }
  
  func alertInViewController(viewController: UIViewController) {
    alpha = 0
    UIApplication.sharedApplication().delegate?.window??.addSubview(self)
    contentView.frame = CGRect(x: 20, y: (UIScreen.mainScreen().bounds.height - contentView.bounds.height) / 2, width: UIScreen.mainScreen().bounds.width - 2 * 20, height: contentView.bounds.height)
    transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
    UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10, options: .CurveEaseIn, animations: {
      self.transform = CGAffineTransformIdentity
      self.alpha = 1
    }, completion: nil)
  }
  
  func tapped() {
    UIView.animateWithDuration(0.3, animations: {
      self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      self.alpha = 0
      }) { finished in
        self.dismiss()
    }
  }
  
  func dismiss() {
    timer?.invalidate()
    timer = nil
    callback?()
    removeFromSuperview()
  }
  
  func touchUpInside(sender: UIButton) {
    let index = sender.tag - 1
    if index >= 0 {
      buttons![index].1?()
      callback = nil
      dismiss()
    } else {
      UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
        self.alpha = 0
        }, completion: { finished in
          self.dismiss()
      })
    }
  }
}

class ToastView: UIView {
  var timer: NSTimer?
  var callback: (() -> ())?
  lazy var contentView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor(red: 51 / 255, green: 112 / 255, blue: 151 / 255, alpha: 1)
    return view
  }()
  lazy var tap: UITapGestureRecognizer = {
    UITapGestureRecognizer(target: self, action: "dismiss")
  }()
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  init(text: String, callback: () -> ()) {
    super.init(frame: CGRectZero)
    self.callback = callback
    timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: "dismiss", userInfo: nil, repeats: false)
    
    let horizontalPadding: CGFloat = 20
    let verticalPadding: CGFloat = 70
    let width = UIScreen.mainScreen().bounds.width - 4 * horizontalPadding
    var labelFrame = CGRect(x: horizontalPadding, y: verticalPadding, width: width, height: 0)
    
    let height = (text as NSString).boundingRectWithSize(labelFrame.size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 16)!], context: nil).height
    labelFrame.size.height = height
    
    contentView.frame = CGRect(x: (bounds.width - contentView.bounds.width) / 2, y: (bounds.height - contentView.bounds.height) / 2, width: width + 2 * horizontalPadding, height: height + 2 * verticalPadding)
    
    let label = UILabel(frame: labelFrame)
    label.backgroundColor = UIColor.clearColor()
    label.textColor = UIColor.whiteColor()
    label.numberOfLines = 0
    label.textAlignment = .Center
    label.font = UIFont(name: "HelveticaNeue", size: 16)
    label.text = text
    contentView.addSubview(label)
    
    addSubview(contentView)
    
    addGestureRecognizer(tap)
  }
  
  func alertInViewController(viewController: UIViewController) {
    alpha = 0
    frame = viewController.view.bounds
    contentView.frame = CGRect(origin: CGPoint(x: (bounds.width - contentView.bounds.width) / 2, y: (bounds.height - contentView.bounds.height) / 2), size: contentView.bounds.size)
    viewController.view.addSubview(self)
    contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
    UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10, options: .CurveEaseIn, animations: {
      self.contentView.transform = CGAffineTransformIdentity
      self.alpha = 1
      }, completion: nil)
  }
  
  func dismiss() {
    timer?.invalidate()
    timer = nil
    callback?()
    removeFromSuperview()
  }
}
