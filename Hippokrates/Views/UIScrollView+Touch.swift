//
//  UIScrollView+Touch.swift
//  Hippokrates
//
//  Created by Gilson Gil on 7/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UIScrollView {
  public override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    if !dragging {
      superview?.endEditing(true)
    } else {
      super.touchesEnded(touches, withEvent: event)
    }
  }
}
