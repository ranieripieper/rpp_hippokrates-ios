//
//  BarUserView.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/27/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BarUserView: UIView {
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  
  var callback: (() -> ())!
  
  class func initWithUser(user: User, callback: () -> ()) -> BarUserView {
    if let barUserView = R.nib.barUserView.firstView(self, options: nil) {
      let placeholder = R.image.img_placeholder
      barUserView.avatarImageView.image = placeholder
      barUserView.avatarImageView.setImageWithURL(NSURL(string: user.avatarURL ?? "")!, placeholderImage: placeholder)
      barUserView.nameLabel.text = user.name
      barUserView.frame = CGRect(x: 44, y: 20, width: UIScreen.mainScreen().bounds.width - 88, height: 44)
      barUserView.callback = callback
      return barUserView
    } else {
      let barUserView = BarUserView(frame: CGRect(x: 44, y: 20, width: UIScreen.mainScreen().bounds.width - 88, height: 44))
      return barUserView
    }
  }
  
  @IBAction func tapped(sender: UIButton) {
    callback()
  }
}