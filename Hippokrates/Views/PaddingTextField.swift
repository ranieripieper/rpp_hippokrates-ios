//
//  PaddingTextField.swift
//  Hippokrates
//
//  Created by Gilson Gil on 5/27/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PaddingTextField: UITextField {
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setPaddings()
  }
  
  func setPaddings() {
    setPaddings(20)
  }
  
  func setPaddings(width: CGFloat) {
    leftView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: width, height: bounds.height)))
    leftViewMode = .Always
    rightView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 20, height: bounds.height)))
    rightViewMode = .Always
  }
}
