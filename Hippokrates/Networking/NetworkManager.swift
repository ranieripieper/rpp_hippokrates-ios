//
//  NetworkManager.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/30/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NetworkManager: AFHTTPSessionManager {
  let provider = "ios"
  let domain = "us.hippokrates"
  
  class var sharedInstance: NetworkManager {
    struct Static {
      static var instance: NetworkManager?
      static var token: dispatch_once_t = 0
    }
    dispatch_once(&Static.token) {
      Static.instance = NetworkManager()
      if let authToken = SSKeychain.passwordForService(Static.instance!.domain, account: "authToken") {
        Static.instance!.requestSerializer.setValue(authToken, forHTTPHeaderField: "X-Token")
      }
      Static.instance!.requestSerializer.setValue(Static.instance!.provider, forHTTPHeaderField: "X-Provider")
      AFNetworkActivityIndicatorManager.sharedManager().enabled = true
    }
    return Static.instance!
  }
  
  init() {
    // Staging
//    super.init(baseURL: NSURL(string: "http://api-staging.hippokrates.us/v1/"), sessionConfiguration: nil)
    // Production
    super.init(baseURL: NSURL(string: "http://api.hippokrates.us/v1/"), sessionConfiguration: nil)
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
}

typealias APIResponse = [String: AnyObject]

// MARK: API
// MARK: Specialties
extension NetworkManager {
  func getSpecialties(completion: Result<APIResponse> -> ()) {
    GET("medical_specialities", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: Schools
extension NetworkManager {
  func getSchools(completion: Result<APIResponse> -> ()) {
    GET("colleges", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: Authentication
extension NetworkManager {
  func validateAuthToken(completion: Result<Bool> -> ()) {
    if SSKeychain.passwordForService(domain, account: "authToken") != nil {
      POST("users/auth/token", parameters: nil, success: { dataTask, response in
        if let newAuthToken = response[""] as? String {
          self.requestSerializer.setValue(newAuthToken, forHTTPHeaderField: "X-Token")
          SSKeychain.setPassword(newAuthToken, forService: self.domain, account: "authToken")
        }
        completion(Result(true))
        }) { dataTask, error in
          self.requestSerializer.setValue(nil, forHTTPHeaderField: "X-Token")
          SSKeychain.deletePasswordForService(self.domain, account: "authToken")
          completion(Result(Error(code: error.code, domain: error.domain, description: error.localizedDescription)))
      }
    } else {
      completion(Result(Error(code: -1, domain: self.domain, description: nil)))
    }
  }
  
//  curl -X POST -F "user[education_data][residency_program_instituition_name]=teste" -F "user[education_data][classroom_name]=Turma 205" -F "user[education_data][classroom_number]=12" -F "user[education_data][university_institution_name]=Pontifícia Universidade Católica de São Paulo" https://hipapp-staging.herokuapp.com/api/v1/users
  func register(email: String, password: String, firstName: String, lastName: String, crm: String, crmState: State, gender: Gender, specialty: Specialty?, occupation: String?, formationSchool: School?, classNumber: String?, residencySchool: School?, code: String, avatar: UIImage?, professor: Bool, completion: Result<APIResponse> -> ()) {
    var params: [String: AnyObject] = ["email": email, "password": password, "password_confirmation": password, "name": firstName, "last_name": lastName, "crm": ["number": crm, "state": crmState.UF()], "invite_code": code, "profile_type": (professor ? "teacher" : "doctor")]
    if count(gender.toAPIString()) > 0 {
      params["gender"] = gender.toAPIString()
    }
    if let specialty = specialty {
      params["medical_specialty_id"] = specialty.id
    }
    if let occupation = occupation {
      params["occupation"] = occupation
    }
    var education = [String: AnyObject]()
    if let formationSchool = formationSchool {
      education["university_institution_name"] = formationSchool.name
    }
    if let classNumber = classNumber {
      education["classroom_number"] = classNumber
    }
    if let residencySchool = residencySchool {
      education["residency_program_institution_name"] = residencySchool.name
    }
    if !education.isEmpty {
      params["education_data"] = education
    }
    if let persistedToken = NSUserDefaults.standardUserDefaults().stringForKey("PushToken") {
      params["device"] = ["platform": "ios", "token": persistedToken]
    }
    POST("users", parameters: ["user": params], constructingBodyWithBlock: {
      if let avatar = avatar {
        $0.appendPartWithFileData(UIImageJPEGRepresentation(avatar, 1), name: "user[profile_image]", fileName: "profile_image.jpeg", mimeType: "image/jpeg")
      }
    }, success: { dataTask, data in
      completion(self.handleSuccess(data, {
        if $0 {
          if let response = data as? [String: AnyObject], let authData = response["auth_data"] as? [String: AnyObject], let authToken = authData["auth_token"] as? String {
            self.requestSerializer.setValue(authToken, forHTTPHeaderField: "X-Token")
            SSKeychain.setPassword(authToken, forService: self.domain, account: "authToken")
            completion(Result(response["user_data"] as! APIResponse))
          }
        }
      }))
    }) {
      completion(self.handleError($1))
    }
  }
  
  func registerStudent(email: String, password: String, firstName: String, lastName: String, gender: Gender, formationSchool: School, highschoolState: State, highschoolYear: Int, code: String, avatar: UIImage?, completion: Result<APIResponse> -> ()) {
    var params = ["email": email, "password": password, "password_confirmation": password, "name": firstName, "last_name": lastName, "invite_code": code, "profile_type": "student", "education_data": ["university_institution_name": formationSchool.name, "college_degree_state": highschoolState.name(), "college_degree_year": highschoolYear]]
    if count(gender.toAPIString()) > 0 {
      params["gender"] = gender.toAPIString()
    }
    POST("users", parameters: ["user": params], constructingBodyWithBlock: {
      if let avatar = avatar {
        $0.appendPartWithFileData(UIImageJPEGRepresentation(avatar, 1), name: "user[profile_image]", fileName: "profile_image.jpeg", mimeType: "image/jpeg")
      }
      }, success: { dataTask, data in
        completion(self.handleSuccess(data, {
          if $0 {
            if let response = data as? [String: AnyObject], let authData = response["auth_data"] as? [String: AnyObject], let authToken = authData["auth_token"] as? String {
              self.requestSerializer.setValue(authToken, forHTTPHeaderField: "X-Token")
              SSKeychain.setPassword(authToken, forService: self.domain, account: "authToken")
              completion(Result(response["user_data"] as! APIResponse))
            }
          }
        }))
      }) {
        completion(self.handleError($1))
    }
  }

  func login(email: String, password: String, completion: Result<APIResponse> -> ()) {
    let params = ["email": email, "password": password, "provider": provider]
    POST("users/auth", parameters: params, success: { dataTask, response in
      if let responseData = response as? [String: AnyObject], let success = responseData["success"] as? Bool where success, let authData = responseData["auth_data"] as? [String: AnyObject], let authToken = authData["auth_token"] as? String {
        self.requestSerializer.setValue(authToken, forHTTPHeaderField: "X-Token")
        SSKeychain.setPassword(authToken, forService: self.domain, account: "authToken")
        completion(Result(responseData["user_data"] as! APIResponse))
        return
      }
      SSKeychain.deletePasswordForService(self.domain, account: "authToken")
      let error = Error(code: 401, domain: self.domain, description: nil)
      completion(Result(error))
      }) { dataTask, error in
        SSKeychain.deletePasswordForService(self.domain, account: "authToken")
        if let userInfo = error.userInfo, let data = userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? NSData, let json = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: nil) as? APIResponse, let err = json["error"] as? Bool where err, let status = json["code"] as? Int, let errors = json["errors"] as? [String], let message = errors.first {
          completion(Result(Error(code: status, domain: self.domain, description: message)))
        } else {
          completion(Result(Error(code: error.code, domain: error.domain, description: error.localizedDescription)))
        }
    }
  }
  
  func forgotPassword(email: String, completion: Result<Bool> -> ()) {
    POST("users/password_reset", parameters: ["user": ["email": email]], success: { dataTask, response in
      if let responseData = response as? [String: AnyObject] {
        if let success = responseData["success"] as? Bool {
          if success {
            completion(Result(success))
            return
          }
        }
      }
      completion(Result(Error(code: 500, domain: self.domain, description: nil)))
      }) { dataTask, error in
        completion(Result(Error(code: error.code, domain: error.domain, description: error.localizedDescription)))
    }
  }
  
  func newPassword(password: String, token: String, completion: Result<APIResponse> -> ()) {
    PUT("users/password_reset/\(token)", parameters: ["password": password, "password_confirmation": password], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func askForInvite(firstName: String, lastName: String?, email: String, gender: String?, physician: Bool, crm: String?, crmState: State?, school: School?, completion: Result<Bool> -> ()) {
    var params = ["first_name": firstName, "email": email, "profile_type": physician ? "doctor" : "student"]
    if let lastName = lastName where count(lastName) > 0 {
      params["last_name"] = lastName
    }
    if let gender = gender where count(gender) > 0 {
      params["gender"] = gender
    }
    if physician {
      params["crm_number"] = crm!
      params["crm_state"] = crmState!.UF()
    } else {
      params["university_institution_name"] = school!.name
    }
    POST("invites/ask", parameters: params, success: { dataTask, response in
      if let responseData = response as? [String: AnyObject] {
        if let success = responseData["success"] as? Bool {
          if success {
            completion(Result(success))
            return
          }
        }
      }
      completion(Result(Error(code: 401, domain: self.domain, description: nil)))
    }) { dataTask, error in
      completion(Result(Error(code: error.code, domain: error.domain, description: error.localizedDescription)))
    }
  }
  
  func validateInvitingCode(code: String, completion: Result<Bool> -> ()) {
    let params = ["code": code]
    GET("invites/validate", parameters: params, success: { dataTask, response in
      if let responseData = response as? [String: AnyObject] {
        if let success = responseData["success"] as? Bool where success {
          if let inviteData = responseData["invite_data"] as? [String: AnyObject], let status = inviteData["status"] as? String where status == "sent" {
            if let profileType = inviteData["receiver_user_profile_type"] as? String where profileType == "student" {
              completion(Result(false))
            } else {
              completion(Result(true))
            }
            return
          }
        }
      }
      completion(Result(Error(code: 401, domain: self.domain, description: "Código inválido")))
      }) { dataTask, error in
        if let userInfo = error.userInfo, let data = userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? NSData, let json = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: nil) as? APIResponse, let err = json["error"] as? Bool where err, let errors = json["errors"] as? [String], let message = errors.first {
          let status = json["status_code"] as? Int ?? json["code"] as? Int ?? 401
          completion(Result(Error(code: status, domain: self.domain, description: message)))
        } else {
          completion(Result(Error(code: error.code, domain: error.domain, description: error.localizedDescription)))
        }
    }
  }
  
  func validateCRM(crm: String, crmState: State, firstName: String, lastName: String, completion: Result<APIResponse> -> ()) {
    let params = ["number": crm, "state": crmState.UF(), "first_name": firstName, "last_name": lastName, "return_data": "true"]
    GET("crm/validate", parameters: params, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func logout() {
    DELETE("users/logout", parameters: nil, success: nil, failure: nil)
  }
}

// MARK: User
extension NetworkManager {
  func me(completion: Result<APIResponse> -> ()) {
    GET("users/me", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func myCircles(page: Int, completion: Result<APIResponse> -> ()) {
    GET("users/me/friendship_circles", parameters: ["page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func userWithId(id: Int, completion: Result<APIResponse> -> ()) {
    GET("users/\(id)/", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func feed(page: Int, completion: Result<APIResponse> -> ())  {
    GET("users/me/feed", parameters: ["page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func feedWithId(id: Int, page: Int, completion: Result<APIResponse> -> ())  {
    GET("users/\(id)/feed", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func notifications(page: Int, completion: Result<APIResponse> -> ()) {
    GET("users/me/notifications", parameters: ["per_page": 60, "page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func updateProfile(occupation: String?, specialtyId: Int?, formationSchoolName: String?, formationSchoolYear: Int?, professor: Bool?, avatar: UIImage?, password: String?, completion: Result<APIResponse> -> ()) {
    var params = [String: AnyObject]()
    if let occupation = occupation {
      params["occupation"] = occupation
    }
    if let specialtyId = specialtyId {
      params["medical_specialty_id"] = specialtyId
    }
    var educationData = [String: AnyObject]()
    if let formationSchoolName = formationSchoolName where count(formationSchoolName) > 0 {
      educationData["university_institution_name"] = formationSchoolName
    }
    if let formationSchoolYear = formationSchoolYear {
      educationData["classroom_number"] = formationSchoolYear
    }
    if !educationData.isEmpty {
      params["education_data"] = educationData
    }
    if let professor = professor {
      params["profile_type"] = professor ? "teacher" : "doctor"
    }
    if let password = password where count(password) > 0 {
      params["password"] = password
      params["password_confirmation"] = password
    }
    let request = requestSerializer.multipartFormRequestWithMethod("PUT", URLString: "\(NetworkManager.sharedInstance.baseURL!.absoluteString!)users/me", parameters: ["user": params], constructingBodyWithBlock: { (multipart: AFMultipartFormData!) -> Void in
      if let avatar = avatar {
        multipart.appendPartWithFileData(UIImageJPEGRepresentation(avatar, 1), name: "user[profile_image]", fileName: "profile_image.jpeg", mimeType: "image/jpeg")
      }
      }, error: nil)
    let manager = AFHTTPRequestOperationManager(baseURL: baseURL)
    let operation = manager.HTTPRequestOperationWithRequest(request, success: { req, data in
      completion(self.handleSuccess(data, nil))
      }) { req, error in
        completion(self.handleError(error))
    }
    operationQueue.addOperation(operation)
  }
  
  func updateStudentProfile(formationSchoolName: String?, avatar: UIImage?, password: String?, completion: Result<APIResponse> -> ()) {
    var params = [String: AnyObject]()
    var educationData = [String: AnyObject]()
    if let formationSchoolName = formationSchoolName where count(formationSchoolName) > 0 {
      educationData["university_institution_name"] = formationSchoolName
    }
    if !educationData.isEmpty {
      params["education_data"] = educationData
    }
    if let password = password where count(password) > 0 {
      params["password"] = password
      params["password_confirmation"] = password
    }
    let request = requestSerializer.multipartFormRequestWithMethod("PUT", URLString: "\(NetworkManager.sharedInstance.baseURL!.absoluteString!)users/me", parameters: ["user": params], constructingBodyWithBlock: { (multipart: AFMultipartFormData!) -> Void in
      if let avatar = avatar {
        multipart.appendPartWithFileData(UIImageJPEGRepresentation(avatar, 1), name: "user[profile_image]", fileName: "profile_image.jpeg", mimeType: "image/jpeg")
      }
      }, error: nil)
    let manager = AFHTTPRequestOperationManager(baseURL: baseURL)
    let operation = manager.HTTPRequestOperationWithRequest(request, success: { req, data in
      completion(self.handleSuccess(data, nil))
      }) { req, error in
        completion(self.handleError(error))
    }
    operationQueue.addOperation(operation)
  }
  
  func graduateStudent(crm: String, crmState: String, name: String, lastName: String, completion: Result<APIResponse> -> ()) {
    let params = ["user": ["profile_type": "doctor", "crm_number": crm, "crm_state": crmState]]
    PUT("users/me", parameters: params, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func changePrivacy(key: String, value: Bool, completion: Result<APIResponse> -> ()) {
    PUT("users/me/update_privacy", parameters: ["key": key, "value": value ? "true" : "false"], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func currentSubscription(completion: Result<APIResponse> -> ()) {
    GET("users/me/current_subscription", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func deleteAllPhotos(completion: Result<APIResponse> -> ()) {
    DELETE("users/me/posts", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func deletePhotosWithIds(ids: [Int], completion: Result<APIResponse> -> ()) {
    let idsString = ids.reduce("") {
      $0.0 + String($0.1) + ","
    }
    DELETE("users/me/posts", parameters: ["posts_ids": idsString], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func invite(users: [(String, String)], completion: Result<APIResponse> -> ()) {
    let usersString = users.reduce("") {
      $0.0 + $0.1.0 + "," + $0.1.1 + ";"
    }
    POST("users/me/invites", parameters: ["email_addresses": usersString], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func deleteAccount(completion: Result<APIResponse> -> ()) {
    DELETE("users/me/account", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func registerDevice(token: String, completion: Result<APIResponse> -> ()) {
    POST("users/me/devices", parameters: ["device": ["token": token, "platform": "ios"]], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func preferences(completion: Result<APIResponse> -> ()) {
    GET("users/me/preferences", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func updatePreferences(preferences: [PushNotification], completion: Result<APIResponse> -> ()) {
    var params = [String: AnyObject]()
    for preference in preferences {
      params[preference.name] = preference.activated ? "true" : "false"
    }
    PUT("users/me/preferences", parameters: ["preferences": params], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: Circle
extension NetworkManager {
  func circleInfo(id: Int, completion: Result<APIResponse> -> ()) {
    GET("friendship_circles/\(id)", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func postsForCircleId(id: Int, page: Int, completion: Result<APIResponse> -> ()) {
    GET("friendship_circles/\(id)/feed", parameters: ["page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func membersForCircleId(id: Int, page: Int, completion: Result<APIResponse> -> ()) {
    GET("friendship_circles/\(id)/members", parameters: ["page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func createCircle(name: String, image: UIImage?, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles", parameters: ["friendship_circle": ["name": name]], constructingBodyWithBlock: { (multipart: AFMultipartFormData!) -> Void in
      if let image = image {
        multipart.appendPartWithFileData(UIImageJPEGRepresentation(image, 1), name: "friendship_circle[profile_image]", fileName: "profile_image.jpeg", mimeType: "image/jpeg")
      }
    }, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func createStudentCircle(name: String, image: UIImage?, schoolName: String, subject: String?, fromDate: NSDate?, toDate: NSDate?, limit: Int, completion: Result<APIResponse> -> ()) {
    var params: [String: AnyObject] = ["name": name, "college_instituition_name": schoolName, "members_limit": String(limit)]
    if let subject = subject {
      params["college_subject"] = subject
    }
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    if let fromDate = fromDate {
      params["start_date"] = dateFormatter.stringFromDate(fromDate)
    }
    if let toDate = toDate {
      params["end_date"] = dateFormatter.stringFromDate(toDate)
    }
    POST("friendship_circles", parameters: ["friendship_circle": params], constructingBodyWithBlock: { (multipart: AFMultipartFormData!) -> Void in
      if let image = image {
        multipart.appendPartWithFileData(UIImageJPEGRepresentation(image, 1), name: "friendship_circle[profile_image]", fileName: "profile_image.jpeg", mimeType: "image/jpeg")
      }
      }, success: { dataTask, data in
        completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        dataTask.currentRequest.allHTTPHeaderFields
        completion(self.handleError(error))
    }
  }
  
  func inviteUser(userId: Int, toCircle circleId: Int, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles/\(circleId)/add_member", parameters: ["user_id": userId], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func joinCircle(code: String, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles/by_code/\(code)/join", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func leaveCircle(id: Int, completion: Result<APIResponse> -> ()) {
    DELETE("friendship_circles/\(id)/quit", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func deleteCircle(id: Int, completion: Result<APIResponse> -> ()) {
    DELETE("friendship_circles/\(id)/", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func removeUser(userId: Int, fromCircle circleId: Int, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles/\(circleId)/moderate/remove_users", parameters: ["user_id": userId], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func removePhoto(photoId: Int, fromCircle circleId: Int, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles/\(circleId)/moderate/remove_posts", parameters: ["post_id": photoId], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func getCirclePendingRequests(id: Int, page: Int, completion: Result<APIResponse> -> ()) {
    GET("friendship_circles/\(id)/pending_requests", parameters: ["page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func turnCircleAdmin(circleId: Int, userId: Int, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles/\(circleId)/moderate/transfer_admin", parameters: ["user_id": userId], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func updateCircleImage(circleId: Int, circleName: String, image: UIImage, completion: Result<APIResponse> -> ()) {
    let request = requestSerializer.multipartFormRequestWithMethod("PUT", URLString: "\(NetworkManager.sharedInstance.baseURL!.absoluteString!)friendship_circles/\(circleId)", parameters: ["friendship_circle": ["name": circleName]], constructingBodyWithBlock: { (multipart: AFMultipartFormData!) -> Void in
      multipart.appendPartWithFileData(UIImageJPEGRepresentation(image, 1), name: "friendship_circle[profile_image]", fileName: "profile_image.jpeg", mimeType: "image/jpeg")
      }, error: nil)
    let manager = AFHTTPRequestOperationManager(baseURL: baseURL)
    let operation = manager.HTTPRequestOperationWithRequest(request, success: { req, data in
      completion(self.handleSuccess(data, nil))
      }) { req, error in
        completion(self.handleError(error))
    }
    operationQueue.addOperation(operation)
  }
  
  func acceptMembers(ids: [Int], circleId: Int, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles/\(circleId)/moderate/approve_pending_users", parameters: ["user_ids": ids], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func declineMembers(ids: [Int], circleId: Int, completion: Result<APIResponse> -> ()) {
    POST("friendship_circles/\(circleId)/moderate/decline_pending_users", parameters: ["user_ids": ids], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: Photo
extension NetworkManager {
  func getPhotoWithId(id: Int, completion: Result<APIResponse> -> ()) {
    GET("posts/\(id)", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func uploadPhoto(photo: UIImage, text: String, specialtyId: Int, isPrivate: Bool, circles: [Int], completion: Result<APIResponse> -> ()) {
    let post = ["content": text, "privacy_status": (isPrivate ? "private" : "public"), "medical_specialty_id": specialtyId, "friendship_circles_ids": circles]
    POST("posts", parameters: ["post": post], constructingBodyWithBlock: {
      $0.appendPartWithFileData(UIImageJPEGRepresentation(photo, 1), name: "post[image]", fileName: "post.jpeg", mimeType: "image/jpeg")
      }, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func updatePhoto(id: Int, image: UIImage, text: String, completion: Result<APIResponse> -> ()) {
    let post = ["content": text]
    let request = requestSerializer.multipartFormRequestWithMethod("PUT", URLString: "\(NetworkManager.sharedInstance.baseURL!.absoluteString!)posts/\(id)", parameters: ["post": post], constructingBodyWithBlock: { multipart in
      multipart.appendPartWithFileData(UIImageJPEGRepresentation(image, 1), name: "post[image]", fileName: "post.jpeg", mimeType: "image/jpeg")
      }, error: nil)
    let manager = AFHTTPRequestOperationManager(baseURL: baseURL)
    let operation = manager.HTTPRequestOperationWithRequest(request, success: { req, data in
      completion(self.handleSuccess(data, nil))
      }) { req, error in
        completion(self.handleError(error))
    }
    operationQueue.addOperation(operation)
  }
  
  func deletePhoto(photoId: Int, completion: Result<APIResponse> -> ()) {
    DELETE("posts/\(photoId)", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func likePhoto(id: Int, completion: Result<APIResponse> -> ()) {
    POST("posts/\(id)/like", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func dislikePhoto(id: Int, completion: Result<APIResponse> -> ()) {
    DELETE("posts/\(id)/like", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func commentPhoto(photoId: Int, comment: String, completion: Result<APIResponse> -> ()) {
    POST("posts/\(photoId)/comment", parameters: ["comment": ["content": comment]], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func photoComments(photoId: Int, page: Int, completion: Result<APIResponse> -> ()) {
    GET("posts/\(photoId)/comments", parameters: ["per_page": 20, "page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func report(photoId: Int, type: Int, completion: Result<APIResponse> -> ()) {
    POST("posts/\(photoId)/report", parameters: ["type": type], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: Comment
extension NetworkManager {
  func editComment(id: Int, postId: Int, text: String, completion: Result<APIResponse> -> ()) {
    PUT("posts/\(postId)/comment/\(id)", parameters: ["comment": ["content": text]], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func reportComment(id: Int, postId: Int, completion: Result<APIResponse> -> ()) {
    POST("posts/\(postId)/comment/\(id)/report", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func deleteComment(id: Int, postId: Int, completion: Result<APIResponse> -> ()) {
    DELETE("posts/\(postId)/comment/\(id)", parameters: nil, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: Search
extension NetworkManager {
  func searchUser(user: String, page: Int, onlyPhysicians: Bool, completion: Result<APIResponse> -> ()) -> NSURLSessionDataTask {
    var params: [String: AnyObject] = ["name": user, "page": page]
    if onlyPhysicians {
      params["profile_types"] = ["doctor", "teacher"]
    }
    return GET("search/users", parameters: params, success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }!
  }
  
  func searchPosts(hashtag: String, completion: Result<APIResponse> -> ()) {
    GET("search/posts", parameters: ["hashtag": hashtag], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func searchPosts(specialtyId: Int, page: Int, completion: Result<APIResponse> -> ()) {
    GET("posts/by_medical_specialty/\(specialtyId)", parameters: ["page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func getHashtags(completion: Result<APIResponse> -> ()) {
    GET("search/hashtags", parameters: ["per_page": 100, "page": 1], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  func getHashtags(string: String, page: Int, completion: Result<APIResponse> -> ()) -> NSURLSessionDataTask {
    return GET("search/hashtags", parameters: ["q": string, "per_page": 100, "page": page], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }!
  }
}

// MARK: General
extension NetworkManager {
  func handleSuccess(response: AnyObject, _ callback: ((Bool) -> ())?) -> Result<APIResponse> {
    if let responseData = response as? [String: AnyObject] {
      callback?(true)
      return Result(responseData)
    } else if let responseData = response as? [AnyObject] {
      callback?(true)
      return Result(["data": responseData])
    }
    callback?(false)
    return Result(Error(code: 500, domain: self.domain, description: nil))
  }
  
  func handleError(error: NSError) -> Result<APIResponse> {
    if let userInfo = error.userInfo, let data = userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? NSData, let json = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: nil) as? APIResponse, let err = json["error"] as? Bool where err, let errors = json["errors"] as? [String], let message = errors.first {
      let status = json["code"] as? Int ?? json["status"] as? Int ?? json["status_code"] as? Int ?? -1
//      if status == 401 {
//        (UIApplication.sharedApplication().delegate as! AppDelegate).loadLoginFlow()
//      }
      return Result(Error(code: status, domain: domain, description: message))
    }
    return Result(Error(code: error.code, domain: error.domain, description: error.localizedDescription))
  }
}
