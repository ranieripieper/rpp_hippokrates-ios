//
//  RegisterStudentViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RegisterStudentViewController: UIViewController {
  @IBOutlet weak var welcomeLabel: UILabel!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var avatarLabel: UILabel!
  @IBOutlet weak var firstNameTextField: TextField!
  @IBOutlet weak var lastNameTextField: TextField!
  @IBOutlet weak var emailTextField: TextField!
  @IBOutlet weak var passwordTextField: TextField!
  @IBOutlet weak var passwordConfirmationTextField: TextField!
  @IBOutlet weak var femaleButton: UIButton!
  @IBOutlet weak var maleButton: UIButton!
  @IBOutlet weak var schoolTextField: TextField!
  @IBOutlet weak var graduationStateTextField: TextField!
  @IBOutlet weak var graduationYearTextField: TextField!
  @IBOutlet weak var registerButtonWidthConstraint: NSLayoutConstraint!
  
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var selectedFormationSchool: School?
  var inviteCode: String!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    welcomeLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 20 - 30
    avatarImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
    registerButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let destinationNavigationController = segue.destinationViewController as? UINavigationController {
      if let autocompleteViewController = destinationNavigationController.viewControllers.first as? AutocompleteViewController {
        if let textField = sender as? TextField {
          if textField == schoolTextField {
            let schools = School.all() {
              switch $0 {
              case .Success(let boxed):
                autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
            autocompleteViewController.datasource = schools.map() { $0 as NamedObject }
            autocompleteViewController.callback = {
              self.selectedFormationSchool = $0 as? School
              textField.text = self.selectedFormationSchool?.name
              self.dismissViewControllerAnimated(true, completion: nil)
            }
          }
        }
      }
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Avatar
  @IBAction func avatarTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    self.imagePicker = ImagePicker(viewController: self) { image in
      if image != nil {
        self.avatarImageView.image = image!
        self.avatarImageView.contentMode = .ScaleAspectFill
      }
    }
    self.imagePicker?.presentImagePicker()
  }
  
  // MARK: Gender
  @IBAction func genderTapped(sender: UIButton) {
    if !sender.selected {
      femaleButton.selected = sender == femaleButton
      maleButton.selected = sender == maleButton
    } else {
      femaleButton.selected = false
      maleButton.selected = false
    }
  }
  
  // MARK: Register
  @IBAction func registerTapped(sender: UIButton) {
    if validatePasswords() {
      if validateFields() {
        register()
      } else {
        invalidFields()
      }
    } else {
      invalidPasswords()
    }
  }
  
  func validatePasswords() -> Bool {
    return count(passwordTextField.text) > 0 && passwordTextField.text == passwordConfirmationTextField.text
  }
  
  func validateFields() -> Bool {
    return count(firstNameTextField.text) > 0 && count(lastNameTextField.text) > 0 && count(emailTextField.text) > 0 && count(schoolTextField.text) > 0 && count(graduationStateTextField.text) > 0 && count(graduationYearTextField.text) > 0
  }
  
  func invalidPasswords() {
    presentBannerWithText("Por favor, verifique se as senhas conferem")
  }
  
  func invalidFields() {
    presentBannerWithText("Por favor, preencha todos os campos abaixo")
  }
  
  func presentBannerWithText(text: String) {
    let bannerView = BannerView(text: text)
    bannerView.presentInViewController(self)
  }
  
  func register() {
    #if RELEASE
      Mixpanel.sharedInstance().track("Register", properties: ["profile_type": "student"])
    #endif
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    User.registerStudent(emailTextField.text, password: passwordTextField.text, firstName: firstNameTextField.text, lastName: lastNameTextField.text, gender: (femaleButton.selected ? Gender.Female : Gender.Male), formationSchool: selectedFormationSchool!, highschoolState: State(string: graduationStateTextField.text), highschoolYear: graduationYearTextField.text.toInt() ?? 0, code: inviteCode, avatar: avatarImageView.image) {
      switch $0 {
      case .Success:
        self.goHome()
      case .Failure(let error):
        self.presentBannerWithText(error.description)
      }
      JHProgressHUD.sharedHUD.hide()
    }
  }
  
  // MARK: Flow
  func goHome() {
    (UIApplication.sharedApplication().delegate as! AppDelegate).loadMainApplicationFlow()
  }
}

extension RegisterStudentViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    performSegueWithIdentifier(R.segue.segueSchool, sender: textField)
    return false
  }
}
