//
//  SettingsViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  @IBAction func editProfile(sender: UIButton) {
    if User.currentUser.type == .Student {
      if IOS_8() {
        performSegueWithIdentifier(R.segue.segueEditStudent, sender: nil)
      } else {
        performSegueWithIdentifier(R.segue.segueEditStudentIOS7, sender: nil)
      }
    } else {
      if IOS_8() {
        performSegueWithIdentifier(R.segue.segueEditPhysician, sender: nil)
      } else {
        performSegueWithIdentifier(R.segue.segueEditPhysicianIOS7, sender: nil)
      }
    }
  }
  
  @IBAction func notificationsTapped(sender: UIButton) {
    if IOS_8() {
      performSegueWithIdentifier(R.segue.segueNotifications, sender: nil)
    } else {
      performSegueWithIdentifier(R.segue.segueNotificationsIOS7, sender: nil)
    }
  }
  
  @IBAction func termsTapped(sender: UIButton) {
    if let termsViewController = R.storyboard.login.termsViewController {
      termsViewController.noButtons = true
      navigationController?.pushViewController(termsViewController, animated: true)
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  @IBAction func privacyTapped(sender: UIButton) {
    if IOS_8() {
      performSegueWithIdentifier(R.segue.seguePrivacy, sender: nil)
    } else {
      performSegueWithIdentifier(R.segue.seguePrivacyIOS7, sender: nil)
    }
  }
  
  @IBAction func deleteAccount(sender: UIButton) {
    alert("Tem certeza que deseja deletar sua conta?") {
      if $0 {
        User.currentUser.deleteAccount() {
          switch $0 {
          case .Success:
            SSKeychain.deletePasswordForService("us.hippokrates", account: "authToken")
            NetworkManager.sharedInstance.requestSerializer.setValue(nil, forHTTPHeaderField: "X-Token")
            (UIApplication.sharedApplication().delegate as! AppDelegate).loadLoginFlow()
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
        }
      }
    }
  }
  
  // MARK: Alert
  func alert(text: String, callback: Bool -> ()) {
    let alertView = AlertView(text: text, buttons: [("SIM", {
      callback(true)
    }), ("Não", {
      callback(false)
    })])
    alertView.alertInViewController(self)
  }
}
