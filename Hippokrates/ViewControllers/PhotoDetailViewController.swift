//
//  PhotoDetailViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PhotoDetailViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var photoDetailTableViewDelegate: PhotoDetailTableViewDelegate!
  @IBOutlet weak var likeButton: UIButton!
  @IBOutlet weak var commentsButton: UIButton!
  @IBOutlet weak var dateLabel: UILabel!
  
  var photo: Photo?
  var photoId: Int?
  var alerted = false
  var editCommentAlert: AlertTextField?
  
  var loading = false
  var endReached = false
  var currentPage = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(R.nib.photoCell.instance, forCellReuseIdentifier: R.reuseIdentifier.photoCell.identifier)
    tableView.registerNib(R.nib.commentCell.instance, forCellReuseIdentifier: R.reuseIdentifier.commentCell.identifier)
    if IOS_8() {
      tableView.estimatedRowHeight = 402
    }
    if let photo = photo {
      photoDetailTableViewDelegate.photo = photo
      configure()
      getComments()
    } else if let photoId = photoId {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      Photo.getPhotoWithId(photoId) {
        switch $0 {
        case .Success(let boxed):
          self.photo = boxed.unbox
          self.photoDetailTableViewDelegate.photo = self.photo
          self.configure()
          self.getComments()
          self.tableView.reloadData()
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let indexPath = tableView.indexPathForSelectedRow() {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    configure()
    tableView.reloadData()
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    if navigationController == nil {
      if let photoCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? PhotoCell {
        photoCell.delegate = nil
      }
      for index in 0...tableView.numberOfRowsInSection(1) {
        if let commentCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 1)) as? CommentCell {
          commentCell.delegate = nil
        }
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let commentsViewController = segue.destinationViewController as? CommentsViewController {
      commentsViewController.photo = photo
    } else if let userProfileViewController = segue.destinationViewController as? UserProfileViewController {
      if let boxedUser = sender as? Box<User> {
        userProfileViewController.user = boxedUser.unbox
      }
    }
  }
  
  // MARK: Configure
  func configure() {
    if let photo = photo {
      likeButton.selected = photo.liked
      likeButton.setTitle(String(photo.likesCount), forState: .Normal)
      commentsButton.setTitle(String(photo.commentsCount), forState: .Normal)
      dateLabel.text = photoDetailTableViewDelegate.dateFormatter.stringFromDate(photo.createDate)
    }
  }
  
  // MARK: Like
  @IBAction func likeTapped(sender: UIButton) {
    likeAtCell(tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! PhotoCell)
  }
  
  // MARK: Get Comments
  func getComments() {
    if loading || endReached {
      return
    }
    loading = true
    photo?.photoComments(currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.photo!.comments = []
        }
        self.endReached = !boxed.unbox.1
        self.photo!.comments += boxed.unbox.0
        self.loading = false
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        self.loading = false
      }
    }
  }
  
  // MARK: Comment
  func presentCommentOptions(index: Int) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("EDITAR COMENTÁRIO", {
      self.alerted = false
      self.editCommentAtIndex(index)
    }), ("DELETAR COMENTÁRIO", {
      self.alerted = false
      self.deleteCommentAtIndex(index)
    })])
  }
  
  func presentDeleteComment(index: Int) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("DELETAR COMENTÁRIO", {
      self.alerted = false
      self.deleteCommentAtIndex(index)
    })])
  }
  
  func presentReportComment(index: Int) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("DENUNCIAR COMENTÁRIO", {
      self.alerted = false
      self.reportCommentAtIndex(index)
    })])
  }
  
  func deleteCommentAtIndex(index: Int) {
    let comment = self.photo!.comments[index]
    comment.delete(self.photo!.id) {
      switch $0 {
      case .Success(let boxed):
        self.photo!.comments.removeAtIndex(index)
        self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 1
          )], withRowAnimation: .Automatic)
        self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func reportCommentAtIndex(index: Int) {
    let comment = self.photo!.comments[index]
    comment.report(self.photo!.id) {
      switch $0 {
      case .Success(let boxed):
        self.presentBannerWithText("Obrigado. O autor do comentário será notificado.")
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func editCommentAtIndex(index: Int) {
    let comment = photo!.comments[index]
    editCommentAlert = AlertTextField.editCommentTextField(comment.text) { newComment, _ in
      if let newComment = newComment {
        comment.edit(self.photo!.id, text: newComment) {
          switch $0 {
          case .Success(let boxed):
            if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 1)) as? CommentCell {
              cell.updateComment(comment.text)
            }
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
        }
      }
      self.editCommentAlert = nil
    }
  }
  
  // MARK: Photo
  func presentPhotoOptions() {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("EDITAR FOTO", {
      self.alerted = false
      self.editPhoto(self.photo!)
    }), ("DELETAR FOTO", {
      self.alerted = false
      self.presentAlertWithText("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") { yes in
        if yes {
          self.photo!.delete() {
            switch $0 {
            case .Success:
              self.navigationController?.popViewControllerAnimated(true)
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
        }
      }
    })])
  }
  
  func presentDeletePhoto(photo: Photo) {
    presentAlertWithButtons([("DELETAR FOTO", {
      self.presentAlertWithText("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          if let circleId = photo.circleId {
            photo.adminDeleteFromCircle(circleId) {
              switch $0 {
              case .Success(let boxed):
                self.navigationController?.popViewControllerAnimated(true)
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
          }
        }
      }
    })])
  }
  
  func editPhoto(photo: Photo) {
    if let editPictureViewController = R.storyboard.newPhoto.editPictureViewController {
      editPictureViewController.photo = photo
      let aNavigationController = UINavigationController(rootViewController: editPictureViewController)
      aNavigationController.navigationBar.barStyle = .Black
      aNavigationController.navigationBar.barTintColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1)
      aNavigationController.navigationBar.tintColor = UIColor.whiteColor()
      editPictureViewController.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: R.image.btn_camera_action_close, style: .Bordered, target: self, action: "cancelEditPhoto")]
      if let mainNavigationController = navigationController as? MainNavigationController {
        UIView.animateWithDuration(0.3) {
          mainNavigationController.photoDetailAnimator.barUserView?.alpha = 0
        }
      }
      presentViewController(aNavigationController, animated: true, completion: nil)
    }
  }
  
  func cancelEditPhoto() {
    if let mainNavigationController = self.navigationController as? MainNavigationController {
      UIView.animateWithDuration(0.3) {
        mainNavigationController.photoDetailAnimator.barUserView?.alpha = 1
      }
    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func presentReportPhoto(photo: Photo) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("DENUNCIAR FOTO", {
      self.alerted = false
      self.presentReportOptions(photo)
    })])
  }
  
  func presentReportOptions(photo: Photo) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("FOTO IMPRÓPRIA", {
      self.alerted = false
      photo.report(.Improper) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    }), ("IDENTIFIQUEI O PACIENTE", {
      self.alerted = false
      photo.report(.Patient) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    })])
  }
  
  func reportCallback(success: Bool) {
    if success {
      presentBannerWithText("Obrigado. O autor da foto será notificado.")
    } else {
      presentBannerWithText("Você já denunciou esta foto uma vez.")
    }
  }
  
  // MARK: Alert
  func presentAlertWithButtons(buttons: [ButtonType]) {
    let alertView = AlertView(buttons: buttons, cancelButtonTitle: nil)
    alertView.alertInViewController(self)
    alertView.callback = {
      self.alerted = false
    }
  }
  
  func presentAlertWithText(text: String, callback: Bool -> ()) {
    let alertView = AlertView(text: text, buttons: [("SIM", {
      callback(true)
    }), ("Não", {
      callback(false)
    })])
    alertView.alertInViewController(self)
  }
  
  func presentBannerWithText(text: String) {
    let bannerView = BannerView(text: text)
    bannerView.presentInViewController(self)
  }
  
  // MARK: Flow
  func goToUser(user: User?) {
    if user != nil {
      performSegueWithIdentifier(R.segue.segueUser, sender: Box(user!))
    }
  }
  
  func goToPhotoUser() {
    goToUser(photo!.user)
  }
}

extension PhotoDetailViewController: PhotoCellDelegate {
  func likeAtCell(cell: PhotoCell) {
    photo!.toggleLike() {
      switch $0 {
      case .Success(let boxed):
        self.likeButton.setTitle(String(self.photo!.likesCount), forState: .Normal)
        self.likeButton.selected = self.photo!.liked
        cell.updateLikes(self.photo!)
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  @IBAction func commentCell(cell: PhotoCell) {
    performSegueWithIdentifier(R.segue.segueComments, sender: nil)
  }
  
  func goToCircleAtCell(cell: PhotoCell) {
    if let circleViewController = R.storyboard.main.circleViewController {
      if let circle = photo!.circle where circle.type != "public" {
        circleViewController.circle = circle
      } else {
        circleViewController.specialty = photo?.specialty
      }
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(circleViewController, animated: true)
    }
  }
  
  func goToUserAtPhotoCell(cell: PhotoCell) {
    goToPhotoUser()
  }
  
  func optionsForCell(cell: PhotoCell) {
    if let photo = photo {
      if let userId = photo.userId where userId == User.currentUser.id {
        presentPhotoOptions()
      } else if let circle = photo.circle where circle.adminId == User.currentUser.id {
        presentDeletePhoto(photo)
      } else {
        presentReportPhoto(photo)
      }
    }
  }
}

extension PhotoDetailViewController: CommentCellDelegate {
  func goToUserAtCommentCell(cell: CommentCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let comment = photo?.comments[indexPath.row]
      let user = comment!.user
      goToUser(user)
    }
  }
  
  func presentOptionsAtCommentCell(cell: CommentCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let comment = photo?.comments[indexPath.row]
      let user = comment!.user!
      if User.currentUser.id == user.id {
        presentCommentOptions(indexPath.row)
      } else if User.currentUser.id == photo!.user!.id {
        presentDeleteComment(indexPath.row)
      } else {
        presentReportComment(indexPath.row)
      }
    }
  }
}
