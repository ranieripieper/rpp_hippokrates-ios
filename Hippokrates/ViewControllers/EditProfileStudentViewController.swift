//
//  EditProfileStudentViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class EditProfileStudentViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var avatarLabel: UILabel!
  @IBOutlet weak var passwordTextField: TextField!
  @IBOutlet weak var schoolTextField: TextField!
  @IBOutlet weak var schoolPrivateButton: UIButton!
  @IBOutlet weak var graduatedSwitch: UISwitch!
  @IBOutlet weak var saveButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var saveButtonTopSpaceConstraint: NSLayoutConstraint!
  
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var selectedSchool: School?
  var newAvatar: UIImage?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    avatarImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    saveButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    saveButtonTopSpaceConstraint.constant = UIScreen.mainScreen().bounds.height - 64 - 70
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    prefillData()
  }
  
  func prefillData() {
    let user = User.currentUser
    avatarImageView.setImageWithURL(NSURL(string: user.avatarURL ?? "")!)
    if let formation = user.formation {
      schoolTextField.text = formation.school.name
    }
  }
  
  // MARK: Avatar
  @IBAction func avatarTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    self.imagePicker = ImagePicker(viewController: self) { image in
      if let image = image {
        self.avatarImageView.image = image
        self.newAvatar = image
      }
    }
    self.imagePicker?.presentImagePicker()
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Private
  @IBAction func togglePrivate(sender: UIButton) {
    presentPrivateAlert(sender)
  }
  
  func presentPrivateAlert(sender: UIButton) {
    let alertView = AlertView(buttons: [("INFORMAÇÃO PÚBLICA", {
      sender.selected = false
      self.alert("Outros usuários terão acesso a esta informação no teu perfil.", callback: nil)
    }), ("INFORMAÇÃO PRIVADA", {
      sender.selected = true
      self.alert("Outros usuários não terão acesso a esta informação no teu perfil.", callback: nil)
    })], cancelButtonTitle: nil)
    alertView.alertInViewController(self)
  }
  
  func alert(text: String, callback: (() -> ())?) {
    let alertView = AlertView(text: text, callback: callback)
    alertView.alertInViewController(self)
  }
  
  // MARK: Graduation
  @IBAction func switchValueChanged(sender: UISwitch) {
    if sender.on {
      let crmValidationViewController = R.storyboard.login.cRMValidationViewController!
      crmValidationViewController.graduation = true
      navigationController?.pushViewController(crmValidationViewController, animated: true)
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  // MARK: Save
  @IBAction func saveTapped(sender: UIButton) {
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    User.currentUser.updateStudentProfile(schoolTextField.text, avatar: newAvatar, password: count(passwordTextField.text) > 0 ? passwordTextField.text : nil) {
      switch $0 {
      case .Success(let boxed):
        BannerView.present("Perfil salvo com sucesso!", inViewController: self)
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
      JHProgressHUD.sharedHUD.hide()
    }
  }
}

extension EditProfileStudentViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if textField == passwordTextField {
      if let newPasswordViewController = R.storyboard.login.newPasswordViewController {
        navigationController?.pushViewController(newPasswordViewController, animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      }
    } else if let aNavigationController = R.storyboard.login.autocompleteNavigationController {
      if let autocompleteViewController = aNavigationController.viewControllers.first as? AutocompleteViewController {
        let schools = School.all() {
          switch $0 {
          case .Success(let boxed):
            autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
        }
        autocompleteViewController.datasource = schools.map() { $0 as NamedObject }
        autocompleteViewController.callback = {
          self.selectedSchool = $0 as? School
          textField.text = self.selectedSchool?.name
          self.dismissViewControllerAnimated(true, completion: nil)
        }
        presentViewController(aNavigationController, animated: true, completion: nil)
      }
    }
    return false
  }
}
