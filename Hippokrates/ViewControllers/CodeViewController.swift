//
//  CodeViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MessageUI

class CodeViewController: UIViewController {
  @IBOutlet weak var codeTextField: TextField!
  @IBOutlet weak var bannerLabel: UILabel!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  
  var inviteCode: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    bannerLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 60
    prefillCode()
    #if RELEASE
      Mixpanel.sharedInstance().track("Start Register Flow")
    #endif
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textFieldDelegate.returnCallback = textFieldDidReturn
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    textFieldDelegate.returnCallback = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let crmvalidationViewController = segue.destinationViewController as? CRMValidationViewController {
      crmvalidationViewController.signupInfo = ["code": codeTextField.text]
    } else if let termsViewController = segue.destinationViewController as? TermsViewController {
      termsViewController.studentCode = codeTextField.text
    }
  }
  
  // MARK: AppLinks
  func prefillCode() {
    if let inviteode = inviteCode {
      codeTextField.text = inviteCode
      sendCodeTapped(nil)
    }
  }
  
  // MARK: Send
  @IBAction func sendCodeTapped(sender: UIButton?) {
    if validateCode() {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      NetworkManager.sharedInstance.validateInvitingCode(codeTextField.text.uppercaseString) { result in
        switch result {
        case .Success(let boxedBool):
          if boxedBool.unbox { // physician
            self.performSegueWithIdentifier(R.segue.segueCRM, sender: nil)
          } else { // student
            self.performSegueWithIdentifier(R.segue.segueTerms, sender: nil)
          }
          #if RELEASE
            Mixpanel.sharedInstance().track("Validate Code", properties: ["valid": true])
          #endif
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
          #if RELEASE
            Mixpanel.sharedInstance().track("Validate Code", properties: ["valid": false])
          #endif
        }
        JHProgressHUD.sharedHUD.hide()
      }
    } else {
      invalidCode()
    }
  }
  
  func validateCode() -> Bool {
    return count(codeTextField.text) > 0
  }
  
  func invalidCode() {
    let bannerView = BannerView(text: "Código inválido. Por favor, tente novamente.")
    bannerView.presentInViewController(self)
  }
  
  // MARK: TextField
  func textFieldDidReturn(textField: UITextField) {
    sendCodeTapped(nil)
  }
  
  // MARK: Contact
  @IBAction func contactTapped(sender: UIButton) {
    var composer = MFMailComposeViewController()
    composer.setToRecipients(["contato@hippokrates.us"])
    composer.delegate = self
    composer.mailComposeDelegate = self
    presentViewController(composer, animated: true, completion: nil)
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
}

extension CodeViewController: UINavigationControllerDelegate {
  
}

extension CodeViewController: MFMailComposeViewControllerDelegate {
  func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
