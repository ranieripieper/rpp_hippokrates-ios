//
//  EditPictureViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class EditPictureViewController: UIViewController {
  @IBOutlet weak var pictureImageView: UIImageView!
  @IBOutlet weak var drawControl: DrawControl!
  @IBOutlet weak var blurControl: BlurControl!
  @IBOutlet weak var pencilControl: PencilControl!
  @IBOutlet weak var blurControlLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var pencilControlLeadingConstraint: NSLayoutConstraint!
  
  var photo: Photo?
  var image: UIImage!
  var drawView: DrawView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let image = image {
      pictureImageView.image = image
      let imageRatio = image.size.width / image.size.height
      let pictureImageViewAspectRatioConstraint = NSLayoutConstraint(item: pictureImageView, attribute: .Width, relatedBy: .Equal, toItem: pictureImageView, attribute: .Height, multiplier: imageRatio, constant: 0)
      pictureImageView.addConstraint(pictureImageViewAspectRatioConstraint)
      view.layoutIfNeeded()
    } else if let photo = photo {
      pictureImageView.setImageWithURLRequest(NSURLRequest(URL: NSURL(string: photo.imageURL ?? "")!), placeholderImage: nil, success: { req, res, image in
        self.image = image
        self.pictureImageView.image = image
        let imageRatio = image.size.width / image.size.height
        let pictureImageViewAspectRatioConstraint = NSLayoutConstraint(item: self.pictureImageView, attribute: .Width, relatedBy: .Equal, toItem: self.pictureImageView, attribute: .Height, multiplier: imageRatio, constant: 0)
        self.pictureImageView.addConstraint(pictureImageViewAspectRatioConstraint)
        self.view.layoutIfNeeded()
      }, failure: { req, res, error in
        
      })
    }
    drawView = DrawView()
    drawView.frame = pictureImageView.bounds
    drawView.backgroundColor = UIColor.clearColor()
    pictureImageView.addSubview(drawView)
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    drawControl.delegate = self
    blurControl.delegate = self
    pencilControl.delegate = self
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    pencilControl.delegate = nil
    blurControl.delegate = nil
    drawControl.delegate = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let createPhotoViewController = segue.destinationViewController as? CreatePhotoViewController {
      createPhotoViewController.image = drawView.getEditedImage()
      createPhotoViewController.photo = photo
    }
  }
  
  // MARK: Done
  @IBAction func doneTapped(sender: UIButton) {
    performSegueWithIdentifier(R.segue.segueCreate, sender: nil)
    #if RELEASE
      Mixpanel.sharedInstance().track("Edit Picture")
    #endif
  }
}

extension EditPictureViewController: DrawControlDelegate {
  func blurTapped() {
    blurControlLeadingConstraint.constant = -view.bounds.width
    drawView.drawingMode = .Blur
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  func pencilTapped() {
    pencilControlLeadingConstraint.constant = -view.bounds.width
    drawView.drawingMode = .Paint
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  func resetTapped() {
    drawView.drawingMode = .None
    drawView.image = UIImage()
    drawView.setNeedsDisplay()
  }
}

extension EditPictureViewController: PencilControlDelegate {
  func pencilControlCanceled() {
    blurControlLeadingConstraint.constant = 0
    pencilControlLeadingConstraint.constant = 0
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  func pencilControlSelectedColor(color: UIColor) {
    drawView.selectedColor = color
  }
  
  func pencilControlSelectedSize(size: CGFloat) {
    drawView.selectedSize = size
  }
}
