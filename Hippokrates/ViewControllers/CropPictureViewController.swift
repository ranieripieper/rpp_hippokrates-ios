//
//  CropPictureViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CropPictureViewController: UIViewController {
  var cropView: CropView!
  
  var image: UIImage!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let editPictureViewController = segue.destinationViewController as? EditPictureViewController {
      if let image = sender as? UIImage {
        editPictureViewController.image = image
      }
    }
  }
  
  // MARK: View
  func configureView() {
    cropView = CropView(image: image, andMaxSize: CGSize(width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height - 64 - 70 - 20))
    view.addSubview(cropView)
    cropView.center = view.center
    cropView.imageView.layer.shadowColor = UIColor.blackColor().CGColor
    cropView.imageView.layer.shadowRadius = 3
    cropView.imageView.layer.shadowOpacity = 0.8
    cropView.imageView.layer.shadowOffset = CGSize(width: 1, height: 1)
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
  }
  
  // MARK: Done
  @IBAction func cropTapped(sender: UIButton) {
    let croppedImage = cropView.croppedImage()
    performSegueWithIdentifier(R.segue.segueEdit, sender: croppedImage)
  }
}
