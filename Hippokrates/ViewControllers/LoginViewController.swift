//
//  LoginViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  @IBOutlet weak var usernameTextField: TextField!
  @IBOutlet weak var passwordTextField: TextField!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textFieldDelegate.returnCallback = textFieldDidReturn
    if UIApplication.sharedApplication().canPerformAction("registerForRemoteNotifications", withSender: nil) {
      let settings = UIUserNotificationSettings(forTypes: .Alert | .Badge | .Sound, categories: nil)
      UIApplication.sharedApplication().registerUserNotificationSettings(settings)
      UIApplication.sharedApplication().registerForRemoteNotifications()
    } else {
      UIApplication.sharedApplication().registerForRemoteNotificationTypes(.Alert | .Badge | .Sound)
    }
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    textFieldDelegate.returnCallback = nil
  }
  
  // MARK: Forgot Password
  @IBAction func forgotPasswordTapped(sender: UIButton) {
    if validateEmail() {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      NetworkManager.sharedInstance.forgotPassword(usernameTextField.text) { result in
        switch result {
        case .Success(let boxedBool):
          self.sentEmail()
        case .Failure(let error):
          self.presentBannerWithText(error.description)
        }
        JHProgressHUD.sharedHUD.hide()
      }
    } else {
      missingEmail()
    }
  }
  
  func validateEmail() -> Bool {
    return count(usernameTextField.text) > 0
  }
  
  func sentEmail() {
    presentBannerWithText("Uma nova senha foi enviada para seu email.")
  }
  
  // MARK: Login
  @IBAction func loginTapped(sender: UIButton?) {
    if validateEmail() {
      if validatePassword() {
        JHProgressHUD.sharedHUD.showInView(navigationController!.view)
        User.login(usernameTextField.text, password: passwordTextField.text) {
          switch $0 {
          case .Success(let boxed):
            (UIApplication.sharedApplication().delegate as! AppDelegate).loadMainApplicationFlow()
            PushNotification.registerToken()
          case .Failure(let error):
            self.presentBannerWithText(error.description)
          }
          JHProgressHUD.sharedHUD.hide()
        }
      } else {
        missingPassword()
      }
    } else {
      missingEmail()
    }
  }
  
  func validatePassword() -> Bool {
    return count(passwordTextField.text) > 0
  }
  
  func missingEmail() {
    presentBannerWithText("Por favor, preencha o email.")
  }
  
  func missingPassword() {
    presentBannerWithText("Por favor, preencha a senha.")
  }
  
  func invalidInputs() {
    presentBannerWithText("Nome ou senha inválidos.")
  }
  
  // MARK: General
  func presentBannerWithText(text: String) {
    let bannertView = BannerView(text: text)
    bannertView.presentInViewController(self)
  }
  
  func textFieldDidReturn(textField: UITextField) {
    if textField == passwordTextField {
      loginTapped(nil)
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
}
