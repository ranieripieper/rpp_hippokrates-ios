//
//  InviteViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol InviteDelegate {
  func cancel()
  func invitesSent()
}

class InviteViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var remainingInvitesLabel: UILabel!
  @IBOutlet weak var inviteButton: UIButton!
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var writeEmailButton: UIBarButtonItem!
  @IBOutlet weak var inviteTableViewDelegate: InviteTableViewDelegate!
  
  var delegate: InviteDelegate?
  var alertTextField: AlertTextField?
  
  var invitesAvailable = User.currentUser.remainingInvites
  var invites = [Int: (String, String)]() {
    didSet {
      let remaining = invitesAvailable - invites.count - extraInvites.count
      remainingInvitesLabel.text = String(remaining)
      writeEmailButton.enabled = remaining > 0
    }
  }
  var extraInvites = [(String, String)]() {
    didSet {
      let remaining = invitesAvailable - invites.count - extraInvites.count
      remainingInvitesLabel.text = String(remaining)
      writeEmailButton.enabled = remaining > 0
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 70 + 70, right: 0)
    tableView.scrollIndicatorInsets = tableView.contentInset
    remainingInvitesLabel.text = String(invitesAvailable)
    if invitesAvailable == 0 {
      showAlertNoMoreInvites()
      writeEmailButton.enabled = false
    }
  }
  
  // MARK: Actions
  @IBAction func cancelTapped(sender: UIButton) {
    delegate?.cancel()
  }
  
  @IBAction func sendInvites(sender: UIButton) {
    var invites = [(String, String)]()
    for key in self.invites.keys {
      invites.append(self.invites[key]!)
    }
    User.currentUser.invite(invites + extraInvites) {
      switch $0 {
      case .Success:
        BannerView.present("Convites enviados com sucesso!", inViewController: self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
          Int64(1 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            self.navigationController?.popViewControllerAnimated(true)
        }
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
    delegate?.invitesSent()
  }
  
  @IBAction func writeEmailTapped(sender: UIButton) {
    alertTextField = AlertTextField.inviteTextField() { name, email in
      if let name = name, let email = email {
        let tuple = (name, email)
        self.extraInvites.append(tuple)
        self.inviteTableViewDelegate.extraDatasource = self.extraInvites.map() { ($0.0, $0.1, nil, true) }
        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: self.extraInvites.count - 1, inSection: 0)], withRowAnimation: .Automatic)
      }
      self.alertTextField = nil
    }
  }
  
  // MARK: Selection
  func shouldSelectItem() -> Bool {
    return invitesAvailable - invites.count > 0
  }
  
  func selectRecord(record: (String, String, UIImage?, Bool), atIndex index: Int) {
    invites[index] = (record.0, record.1)
    view.endEditing(true)
  }
  
  func deselectRecordAtIndex(index: Int) {
    invites.removeValueForKey(index)
    view.endEditing(true)
  }
  
  func deselectExtraRecordAtIndex(index: Int) {
    extraInvites.removeAtIndex(index)
    inviteTableViewDelegate.extraDatasource = extraInvites.map() { ($0.0, $0.1, nil, true) }
    tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
  }
  
  // MARK: No more invites
  func showAlertNoMoreInvites() {
    tableView.hidden = true
    inviteButton.hidden = true
    cancelButton.hidden = true
    let label = UILabel(frame: CGRect(x: 40, y: 60, width: view.bounds.width - 80, height: view.bounds.height - 60))
    label.textColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
    label.font = UIFont(name: "HelveticaNeue-Light", size: 20)
    label.numberOfLines = 0
    label.text = "Você já utilizou todos os seus convites!\nAguarde mais alguns dias e você terá novos convites para enviar."
    view.addSubview(label)
  }
}

extension InviteViewController: UISearchBarDelegate {
  func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
    inviteTableViewDelegate.filteredDatasource = inviteTableViewDelegate.datasource.filter() { record -> Bool in
      count(searchText) == 0 || record.0.rangeOfString(searchText) != nil || record.1.rangeOfString(searchText) != nil
    }
    tableView.reloadData()
  }
  
  func searchBarCancelButtonClicked(searchBar: UISearchBar) {
    inviteTableViewDelegate.filteredDatasource = inviteTableViewDelegate.datasource
    tableView.reloadData()
  }
  
  func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
}
