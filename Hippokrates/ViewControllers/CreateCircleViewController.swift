//
//  CreateCircleViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CreateCircleViewController: UIViewController {
  @IBOutlet weak var circlePictureImageView: UIImageView!
  @IBOutlet weak var circleLabel: UILabel!
  @IBOutlet weak var nameTextField: TextField!
  
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: nil)
    keyboardNotificationObserver.customCallback = handleKeyboard
    keyboardNotificationObserver.installNotifications()
    circlePictureImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let createCircleMembersViewController = segue.destinationViewController as? CreateCircleMembersViewController, let circle = sender as? Circle {
      createCircleMembersViewController.circle = circle
    }
  }
  
  // MARK: Picture
  @IBAction func pictureTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    self.imagePicker = ImagePicker(viewController: self) { image in
      if image != nil {
        self.circlePictureImageView.image = image!
      }
      self.imagePicker = nil
    }
    self.imagePicker?.presentImagePicker()
  }
  
  // MARK: Cancel
  @IBAction func cancelTapped(sender: UIButton) {
    cancel()
  }
  
  // MARK: Invite
  @IBAction func doneTapped(sender: UIButton) {
    if validateFields() {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      Circle.createCircle(nameTextField.text, image: circlePictureImageView.image) {
        switch $0 {
        case .Success(let boxed):
          self.performSegueWithIdentifier(R.segue.segueInvite, sender: boxed.unbox)
        case .Failure(let error):
          let bannerView = BannerView(text: "Por favor, tente novamente.")
          bannerView.presentInViewController(self)
        }
        JHProgressHUD.sharedHUD.hide()
      }
    } else {
      let bannerView = BannerView(text: "Por favor, preencha todos os campos abaixo.")
      bannerView.presentInViewController(self)
    }
  }
  
  func validateFields() -> Bool {
    return count(nameTextField.text) > 0
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Keyboard
  func handleKeyboard(height: CGFloat, duration: CGFloat) {
    if UIScreen.mainScreen().bounds.height == 480 {
      UIView.animateWithDuration(NSTimeInterval(duration)) {
        if height == 0 {
          self.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 0)
        } else {
          self.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -50)
        }
      }
    }
  }
}

extension CreateCircleViewController: InviteDelegate {
  func invitesSent() {
    
  }
  
  func cancel() {
    dismissViewControllerAnimated(true, completion: nil)
  }
}

extension CreateCircleViewController: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
