//
//  BulkDeletePhotosViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BulkDeletePhotosViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  
  var photos = [Photo]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.allowsMultipleSelection = true
  }
  
  // MARK: Confirm
  @IBAction func deleteTapped(sender: UIBarButtonItem) {
    let photosToDelete = collectionView.indexPathsForSelectedItems().map() {
      self.photos[$0.item].id
    }
    if photosToDelete.count > 0 {
      alert("Você tem certeza que deseja deletar as fotos selecionadas? Você não poderá recuperá-las depois.") {
        if $0 {
          self.deletePhotos(photosToDelete, atIndexPaths: self.collectionView.indexPathsForSelectedItems() as! [NSIndexPath])
        }
      }
    }
  }
  
  func deletePhotos(photos: [Int], atIndexPaths indexPaths: [NSIndexPath]) {
    User.currentUser.deletePhotosWithIds(photos) {
      switch $0 {
      case .Success:
        self.photos = self.photos.filter {
          find(photos, $0.id) == nil
        }
        User.currentUser.photos = User.currentUser.photos.filter {
          find(photos, $0.id) == nil
        }
        NSNotificationCenter.defaultCenter().postNotificationName("DeletePhotos", object: nil, userInfo: ["ids": photos])
        self.collectionView.deleteItemsAtIndexPaths(indexPaths)
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  // MARK: Alerts
  func alert(text: String, callback: Bool -> ()) {
    let alertView = AlertView(text: text, buttons: [("SIM", {
      callback(true)
    }), ("Não", {
      callback(false)
    })])
    alertView.alertInViewController(self)
  }
}
