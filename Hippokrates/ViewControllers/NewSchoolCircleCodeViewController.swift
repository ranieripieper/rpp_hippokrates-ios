//
//  NewSchoolCircleCodeViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 6/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NewSchoolCircleCodeViewController: UIViewController {
  @IBOutlet weak var circleImageView: UIImageView!
  @IBOutlet weak var circleNameLabel: UILabel!
  @IBOutlet weak var circleSchoolNameLabel: UILabel!
  @IBOutlet weak var codeLabel: UILabel!
  @IBOutlet weak var limitLabel: UILabel!
  
  var circle: Circle!
  var schoolName: String!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.hidesBackButton = true
    circleImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
    configure()
  }
  
  func configure() {
    circleImageView.setImageWithURL(NSURL(string: circle.pictureURL ?? "")!)
    circleNameLabel.text = circle.name
    circleSchoolNameLabel.text = schoolName
    codeLabel.text = circle.inviteCode ?? "..."
    limitLabel.text = String(circle.remainingMemberCount) + " alunos poderão utilizar este código."
  }
  
  // MARK: Done
  @IBAction func okTapped(sender: UIButton) {
    navigationController?.popToRootViewControllerAnimated(true)
  }
}
