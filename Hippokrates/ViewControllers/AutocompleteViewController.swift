//
//  AutocompleteViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AutocompleteViewController: UIViewController {
  @IBOutlet weak var textField: TextField!
  @IBOutlet weak var tableView: UITableView?
  @IBOutlet weak var autocompleteTableViewDelegate: AutocompleteTableViewDelegate!
  
  var datasource = [NamedObject]() {
    didSet {
      self.autocompleteTableViewDelegate.datasource = datasource
      self.tableView?.reloadData()
    }
  }
  var callback: (AnyObject? -> ())?
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    if IOS_8() {
      tableView?.estimatedRowHeight = 44
    }
    tableView?.registerNib(R.nib.autocompleteCell.instance, forCellReuseIdentifier: R.reuseIdentifier.autocompleteCell.identifier)
  }
  
  func selectedIndex(index: Int) {
    callback?(autocompleteTableViewDelegate.datasource[index] as? AnyObject)
  }
  
  @IBAction func cancel(sender: UIBarButtonItem) {
    callback?(nil)
  }
}

extension AutocompleteViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let text = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
    if count(text) > 0 {
      autocompleteTableViewDelegate.datasource = datasource.filter() {
        $0.name.rangeOfString(text, options: .CaseInsensitiveSearch | .DiacriticInsensitiveSearch) != nil
      }
    } else {
      autocompleteTableViewDelegate.datasource = datasource
    }
    tableView?.reloadData()
    return true
  }
}
