//
//  CreatePhotoViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CreatePhotoViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var privateLabel: UILabel!
  @IBOutlet weak var privateSwitch: UISwitch!
  @IBOutlet weak var descriptionTextView: UITextView!
  @IBOutlet weak var categoryTextField: TextField!
  @IBOutlet weak var shareLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var sendButton: UIButton!
  @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var sendButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var createPhotoCirclesTableViewDelegate: CreatePhotoCirclesTableViewDelegate!
  @IBOutlet weak var sendButtonTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var privateSwitchTopConstraint: NSLayoutConstraint!
  
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  var photo: Photo?
  var image: UIImage!
  var selectedSpecialty: Specialty?
  
  let descriptionPlaceholder = "Descrição"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imageView.image = image
    tableViewHeightConstraint.constant = 50 * CGFloat(User.currentUser.circles.count)
    sendButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    descriptionTextView.layer.borderColor = UIColor(red: 215 / 255, green: 215 / 255, blue: 215 / 255, alpha: 1).CGColor
    view.layoutIfNeeded()
    if let photo = photo {
      privateLabel.hidden = true
      privateSwitch.hidden = true
      descriptionTextView.text = photo.description
      categoryTextField.hidden = true
      categoryTextField.frame = CGRectZero
      shareLabel.hidden = true
      tableViewHeightConstraint.constant = 0
      tableView.hidden = true
      sendButton.setTitle("SALVAR FOTO", forState: .Normal)
      sendButtonTopConstraint.constant = max(descriptionTextView.frame.origin.y + descriptionTextView.bounds.height + 20, UIScreen.mainScreen().bounds.height - 70)
    } else {
      sendButtonTopConstraint.constant = tableView.frame.origin.y + tableViewHeightConstraint.constant + 20
    }
    if User.currentUser.type == .Student {
      privateLabel.hidden = true
      privateSwitch.hidden = true
      privateSwitchTopConstraint.constant = -30
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let autocompleteViewController = segue.destinationViewController as? AutocompleteViewController {
      let specialties = Specialty.all() {
        switch $0 {
        case .Success(let boxed):
          autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
      }
      autocompleteViewController.datasource = specialties.map() { $0 as NamedObject }
      autocompleteViewController.callback = {
        self.selectedSpecialty = $0 as? Specialty
        self.categoryTextField.text = self.selectedSpecialty?.name
        self.navigationController?.popViewControllerAnimated(true)
      }
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Private
  @IBAction func privateSwitchChanged(sender: UISwitch) {
    activateShare(sender.on)
  }
  
  func activateShare(activated: Bool) {
    shareLabel.textColor = activated ? UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1) : UIColor(red: 219 / 255, green: 219 / 255, blue: 219 / 255, alpha: 1)
    tableView.userInteractionEnabled = activated
    tableView.alpha = activated ? 1 : 0.14
  }
  
  // MARK: Send
  @IBAction func sendTapped(sender: UIButton) {
    #if RELEASE
      Mixpanel.sharedInstance().timeEvent("Upload Picture")
    #endif
    if let photo = photo {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      photo.update(image, text: descriptionTextView.text) {
        switch $0 {
        case .Success(let boxed):
          self.dismissViewControllerAnimated(true, completion: nil)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
        JHProgressHUD.sharedHUD.hide()
        #if RELEASE
          Mixpanel.sharedInstance().track("Upload Picture", properties: ["success": !$0.failed, "edit": true])
        #endif
      }
    } else {
      if validateFields() {
        JHProgressHUD.sharedHUD.showInView(navigationController!.view)
        Photo.upload(image, text: descriptionTextView.text, specialtyId: selectedSpecialty!.id, isPrivate: privateSwitch.on, circles: createPhotoCirclesTableViewDelegate.datasource.filter() { $0.1 }.map() { $0.0 }) {
          switch $0 {
          case .Success(let boxed):
            NSNotificationCenter.defaultCenter().postNotificationName("Refresh", object: nil, userInfo: nil)
            self.dismissViewControllerAnimated(true, completion: nil)
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
          JHProgressHUD.sharedHUD.hide()
          #if RELEASE
            Mixpanel.sharedInstance().track("Upload Picture", properties: ["success": !$0.failed, "edit": false])
          #endif
        }
      }
    }
  }
  
  func validateFields() -> Bool {
    return image != nil && selectedSpecialty != nil
  }
}

extension CreatePhotoViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    descriptionTextView.resignFirstResponder()
    performSegueWithIdentifier(R.segue.segueCategories, sender: nil)
    return false
  }
}

extension CreatePhotoViewController: UITextViewDelegate {
  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    if textView.text == descriptionPlaceholder {
      textView.text = ""
    }
    return true
  }
  
  func textViewShouldEndEditing(textView: UITextView) -> Bool {
    if count(textView.text) == 0 {
      textView.text = descriptionPlaceholder
    }
    return true
  }
}
