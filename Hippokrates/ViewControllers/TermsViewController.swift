//
//  TermsViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {
  @IBOutlet weak var termsWebView: UIWebView!
  @IBOutlet weak var acceptButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var declineButtonHeightConstraint: NSLayoutConstraint!

  var signupInfo: [String: String]!
  var studentCode: String!
  var noButtons = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if UIApplication.sharedApplication().canPerformAction("registerForRemoteNotifications", withSender: nil) {
      let settings = UIUserNotificationSettings(forTypes: .Alert | .Badge | .Sound, categories: nil)
      UIApplication.sharedApplication().registerUserNotificationSettings(settings)
      UIApplication.sharedApplication().registerForRemoteNotifications()
    } else {
      UIApplication.sharedApplication().registerForRemoteNotificationTypes(.Alert | .Badge | .Sound)
    }
    if let file = NSBundle.mainBundle().pathForResource("Terms", ofType: "htm"), let string = String(contentsOfFile: file, encoding: NSUTF8StringEncoding, error: nil) {
      termsWebView.loadHTMLString(string, baseURL: nil)
    }
    if noButtons {
      acceptButtonHeightConstraint.constant = 0
      declineButtonHeightConstraint.constant = 0
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let registerPhysicianViewController = segue.destinationViewController as? RegisterPhysicianViewController {
      registerPhysicianViewController.signupInfo = signupInfo
    } else if let registerStudentViewController = segue.destinationViewController as? RegisterStudentViewController {
      registerStudentViewController.inviteCode = studentCode
    }
  }
  
  @IBAction func acceptTapped(sender: UIButton) {
    #if RELEASE
      Mixpanel.sharedInstance().track("Accepted Terms")
    #endif
    if let studentCode = studentCode {
      performSegueWithIdentifier(R.segue.segueStudent, sender: studentCode)
    } else if let signupInfo = signupInfo {
      performSegueWithIdentifier(R.segue.seguePhysician, sender: signupInfo)
    }
  }
  
  @IBAction func declineTapped(sender: UIButton) {
    navigationController?.popViewControllerAnimated(true)
  }
}
