//
//  UserProfileViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var listButton: UIButton!
  @IBOutlet weak var gridButton: UIButton!
  @IBOutlet weak var detailsButton: UIButton!
  @IBOutlet weak var noPhotosLabel: UILabel!
  @IBOutlet weak var userProfileCollectionDelegate: UserProfileCollectionDelegate!
  
  var user: User?
  var viewType: ViewType = .Details
  var loading = false
  var endReached = false
  var currentPage = 0
  
  enum ViewType {
    case List, Grid, Details
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if IOS_8() {
      tableView.estimatedRowHeight = 60
    }
    tableView.registerNib(R.nib.photoCell.instance, forCellReuseIdentifier: R.reuseIdentifier.photoCell.identifier)
    collectionView.registerNib(R.nib.photoThumbCell.instance, forCellWithReuseIdentifier: R.reuseIdentifier.photoThumbCell.identifier)
    avatarImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
    configureUser()
    getUserInfo()
    getUserFeed()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let indexPath = tableView.indexPathForSelectedRow() {
      tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    } else if let indexPaths = tableView.indexPathsForVisibleRows() as? [NSIndexPath] {
      tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: .None)
    }
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    if navigationController == nil && viewType == .List {
      nullifyPhotoCells()
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let bulkDeletePhotosViewController = segue.destinationViewController as? BulkDeletePhotosViewController {
      bulkDeletePhotosViewController.photos = user!.photos
      NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshUser:", name: "DeletePhotos", object: nil)
    }
  }
  
  // MARK: User
  func configureUser() {
    avatarImageView.setImageWithURL(NSURL(string: user?.avatarURL ?? "")!, placeholderImage: R.image.img_placeholder)
    nameLabel.text = user?.name ?? ""
    if user?.id == User.currentUser.id && user?.photos.count > 0 {
      addDeletePhotosButton()
    }
  }
  
  func getUserInfo() {
    User.userWithId(user!.id) {
      switch $0 {
      case .Success(let boxed):
        self.user?.appendUser(boxed.unbox)
        self.userProfileCollectionDelegate.user = self.user
        self.configureUser()
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func getUserFeed() {
    if loading || endReached {
      return
    }
    loading = true
    user?.feedWithId(currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.user?.photos.removeAll(keepCapacity: false)
        }
        self.user?.photos += boxed.unbox.0
        self.endReached = !boxed.unbox.1
        self.loading = false
        self.userProfileCollectionDelegate.posts = self.user?.photos ?? []
        self.tableView.reloadData()
        self.collectionView.reloadData()
        if self.viewType != .Details {
          self.setEmptyPosts()
        }
        self.configureUser()
      case .Failure(let error):
        self.loading = false
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func refreshUser(notification: NSNotification) {
    user = User.currentUser
    userProfileCollectionDelegate.posts = user?.photos ?? []
    NSNotificationCenter.defaultCenter().removeObserver(self, name: "RefreshUser", object: nil)
    tableView.reloadData()
    collectionView.reloadData()
  }
  
  // MARK: View Logic
  @IBAction func toggleListView(sender: UIButton) {
    if userProfileCollectionDelegate.posts.count == 0 {
      tableView.hidden = true
      collectionView.hidden = true
    } else {
      tableView.hidden = false
      collectionView.hidden = true
    }
    viewType = .List
    selectAtIndex(1)
    tableView.estimatedRowHeight = 402
    tableView.reloadData()
  }
  
  @IBAction func toggleGridView(sender: UIButton) {
    nullifyPhotoCells()
    if self.userProfileCollectionDelegate.posts.count == 0 {
      tableView.hidden = true
      collectionView.hidden = true
    } else {
      tableView.hidden = true
      collectionView.hidden = false
    }
    viewType = .Grid
    selectAtIndex(2)
    collectionView.reloadData()
  }
  
  @IBAction func toggleDetailsView(sender: UIButton) {
    nullifyPhotoCells()
    tableView.hidden = false
    collectionView.hidden = true
    viewType = .Details
    selectAtIndex(3)
    tableView.estimatedRowHeight = 60
    tableView.reloadData()
  }
  
  func selectAtIndex(index: Int) {
    listButton.selected = index == 1
    gridButton.selected = index == 2
    detailsButton.selected = index == 3
  }
  
  func addDeletePhotosButton() {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
    view.backgroundColor = UIColor.clearColor()
    let button = UIButton.buttonWithType(.Custom) as! UIButton
    button.frame = view.bounds
    button.setImage(R.image.icn_my_detail_delete, forState: .Normal)
    button.contentHorizontalAlignment = .Right
    button.addTarget(self, action: "deletePhotos", forControlEvents: .TouchUpInside)
    view.addSubview(button)
    let label = UILabel(frame: view.bounds)
    label.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
    label.textColor = UIColor.whiteColor()
    label.text = "Deletar Fotos"
    view.addSubview(label)
    navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: view)]
  }
  
  func setEmptyPosts() {
    tableView.hidden = self.userProfileCollectionDelegate.posts.count == 0
    collectionView.hidden = self.userProfileCollectionDelegate.posts.count == 0
  }
  
  func nullifyPhotoCells() {
    for row in 0...userProfileCollectionDelegate.posts.count {
      if let photoCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0)) as? PhotoCell {
        photoCell.delegate = nil
      }
    }
  }
  
  // MARK: Flow
  func goToUser(user: User) {
    if let userProfileViewController = R.storyboard.main.userProfileViewController {
      userProfileViewController.user = user
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(userProfileViewController, animated: true)
    }
  }
  
  func goToPhoto(photo: Photo) {
    if let photoDetailViewController = R.storyboard.main.photoDetailViewController {
      photoDetailViewController.photo = photo
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(photoDetailViewController, animated: true)
    }
  }
  
  // MARK: Delete Photos
  func deletePhotos() {
    let alert = AlertView(buttons: [("DELETAR TODAS AS FOTOS", {
      let confirmAlert = AlertView(text: "Você tem certeza que deseja deletar todas as suas fotos? Você não poderá recuperá-las depois.", buttons: [("SIM", {
        self.deleteAllPhotos()
      }), ("Não", {
        
      })])
      confirmAlert.alertInViewController(self)
    }), ("SELECIONAR FOTOS", {
      self.performSegueWithIdentifier(R.segue.segueBulkDelete, sender: nil)
    })], cancelButtonTitle: nil)
    alert.alertInViewController(self)
  }
  
  func deleteAllPhotos() {
    self.user?.deleteAllPhotos() {
      switch $0 {
      case .Success:
        self.user?.photos.removeAll(keepCapacity: false)
        self.userProfileCollectionDelegate.posts.removeAll(keepCapacity: false)
        self.tableView.reloadData()
        self.collectionView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func presentEditDeletePhoto(photo: Photo) {
    alert([("EDITAR FOTO", {
      self.editPhoto(photo)
    }), ("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          photo.delete() {
            switch $0 {
            case .Success(let boxed):
              self.userProfileCollectionDelegate.posts = self.userProfileCollectionDelegate.posts.filter() {
                $0.id != photo.id
              }
              self.tableView.reloadData()
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
        }
      }
    })])
  }
  
  func presentDeletePhoto(photo: Photo) {
    alert([("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          if let circleId = photo.circleId {
            photo.adminDeleteFromCircle(circleId) {
              switch $0 {
              case .Success(let boxed):
                self.userProfileCollectionDelegate.posts = self.userProfileCollectionDelegate.posts.filter() {
                  $0.id != photo.id
                }
                self.tableView.reloadData()
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
          }
        }
      }
    })])
  }
  
  func editPhoto(photo: Photo) {
    if let editPictureViewController = R.storyboard.newPhoto.editPictureViewController {
      editPictureViewController.photo = photo
      let aNavigationController = UINavigationController(rootViewController: editPictureViewController)
      aNavigationController.navigationBar.barStyle = .Black
      aNavigationController.navigationBar.barTintColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1)
      aNavigationController.navigationBar.tintColor = UIColor.whiteColor()
      editPictureViewController.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: R.image.btn_camera_action_close, style: .Bordered, target: self, action: "cancelEditPhoto")]
      presentViewController(aNavigationController, animated: true, completion: nil)
    }
  }
  
  func cancelEditPhoto() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func presentReportPhoto(photo: Photo) {
    alert([("DENUNCIAR FOTO", {
      self.presentReportOptions(photo)
    })])
  }
  
  func presentReportOptions(photo: Photo) {
    alert([("FOTO IMPRÓPRIA", {
      photo.report(.Improper) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    }), ("IDENTIFIQUEI O PACIENTE", {
      photo.report(.Patient) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    })])
  }
  
  func reportCallback(success: Bool) {
    if success {
      banner("Obrigado. O autor da foto será notificado.")
    } else {
      banner("Você já denunciou esta foto uma vez.")
    }
  }
  
  // MARK: Alerts
  func alert(buttons: [ButtonType]) {
    let alertView = AlertView(buttons: buttons, cancelButtonTitle: nil)
    alertView.alertInViewController(self)
  }
  
  func alert(text: String, callback: Bool -> ()) {
    let alertView = AlertView(text: text, buttons: [("SIM", {
      callback(true)
    }), ("Não", {
      callback(false)
    })])
    alertView.alertInViewController(self)
  }
  
  func banner(text: String) {
    let banner = BannerView(text: text)
    banner.presentInViewController(self)
  }
}

extension UserProfileViewController: PhotoCellDelegate {
  func likeAtCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = userProfileCollectionDelegate.posts[indexPath.row]
      photo.toggleLike() {
        switch $0 {
        case .Success(let boxed):
          cell.updateLikes(photo)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
  }
  
  func commentCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell), commentsViewController = R.storyboard.main.commentsViewController {
      let photo = userProfileCollectionDelegate.posts[indexPath.row]
      commentsViewController.photo = photo
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(commentsViewController, animated: true)
    }
  }
  
  func goToCircleAtCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell), circleViewController = R.storyboard.main.circleViewController {
      let photo = userProfileCollectionDelegate.posts[indexPath.row]
      if let circle = photo.circle where circle.type != "public" {
        circleViewController.circle = circle
      } else {
        circleViewController.specialty = photo.specialty
      }
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(circleViewController, animated: true)
    }
  }
  
  func goToUserAtPhotoCell(cell: PhotoCell) {
    
  }
  
  func optionsForCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = userProfileCollectionDelegate.posts[indexPath.row]
      if photo.userId! == User.currentUser.id {
        presentEditDeletePhoto(photo)
      } else if photo.circle?.adminId == User.currentUser.id {
        presentDeletePhoto(photo)
      } else {
        presentReportPhoto(photo)
      }
    }
  }
}
