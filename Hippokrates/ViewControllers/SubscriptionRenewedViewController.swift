//
//  SubscriptionRenewedViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SubscriptionRenewedViewController: UIViewController {
  @IBOutlet weak var subscriptionTypeLabel: UILabel!
  @IBOutlet weak var subscriptionExpirationLabel: UILabel!
  @IBOutlet weak var contributionLabel: UILabel!
  @IBOutlet weak var contributionDescriptionLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
  }
  
  func configure() {
    subscriptionTypeLabel.text = "TESTE"
    subscriptionExpirationLabel.text = "31/12/2015"
  }
  
  // MARK: Not Contribute
  @IBAction func notContribute(sender: UIButton) {
    navigationController?.popToRootViewControllerAnimated(true)
  }
  
  // MARK: Contribute
  @IBAction func contribute(sender: UIButton) {
    
  }
}
