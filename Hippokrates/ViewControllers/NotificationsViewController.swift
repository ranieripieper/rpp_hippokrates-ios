//
//  NotificationsViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var notificationsTableViewDelegate: NotificationsTableViewDelegate!
  
  var loading = false
  var endReached = false
  var currentPage = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    if IOS_8() {
      tableView.estimatedRowHeight = 44
    }
    getNotifications()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    tableView.dataSource = notificationsTableViewDelegate
    tableView.delegate = notificationsTableViewDelegate
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    tableView.dataSource = nil
    tableView.delegate = nil
  }
  
  func getNotifications() {
    if loading || endReached {
      return
    }
    loading = true
    User.currentUser.notifications(currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.notificationsTableViewDelegate.datasource = []
        }
        self.endReached = !boxed.unbox.1
        self.notificationsTableViewDelegate.datasource += boxed.unbox.0
        self.tableView.reloadData()
        self.currentPage++
        self.loading = false
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        self.loading = false
      }
    }
  }
}
