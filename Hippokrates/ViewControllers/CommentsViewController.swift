//
//  CommentsViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var commentsTableViewDelegate: CommentsTableViewDelegate!
  @IBOutlet weak var photoImageView: UIImageView!
  @IBOutlet weak var photoDescriptionLabel: UILabel!
  @IBOutlet weak var textInputView: UIView!
  @IBOutlet weak var textInputTextField: UITextField!
  @IBOutlet weak var okButton: UIButton!
  @IBOutlet weak var textInputViewBottomSpaceConstraint: NSLayoutConstraint!
  
  var keyboardNotificationObserver: KeyboardNotificationObserver?
  
  var photo: Photo?
  var photoId: Int?
  var loading = false
  var endReached = false
  var currentPage = 0
  
  var alerted = false
  var editCommentAlert: AlertTextField?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(R.nib.commentCell.instance, forCellReuseIdentifier: R.reuseIdentifier.commentCell.identifier)
    tableView.contentInset = UIEdgeInsets(top: 110, left: 0, bottom: 50, right: 0)
    if IOS_8() {
      tableView.estimatedRowHeight = 60
    }
    if let photo = photo {
      configure()
    } else if let photoId = photoId {
      Photo.getPhotoWithId(photoId) {
        switch $0 {
        case .Success(let boxed):
          self.photo = boxed.unbox
          self.configure()
        case .Failure(let error):
          if error.code == 401 {
            (UIApplication.sharedApplication().delegate as! AppDelegate).loadLoginFlow()
          } else {
            BannerView.present(error.description, inViewController: self)
          }
        }
      }
    }
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    if navigationController == nil {
      keyboardNotificationObserver?.customCallback = nil
      for row in 0...tableView.numberOfRowsInSection(0) {
        if let commentCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0)) as? CommentCell {
          commentCell.delegate = nil
        }
      }
    }
  }
  
  // MARK: Configure
  func configure() {
    photoImageView.setImageWithURL(NSURL(string: photo!.imageURL ?? "")!, placeholderImage: R.image.img_placeholder_photo)
    photoDescriptionLabel.text = photo!.description
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: tableView)
    keyboardNotificationObserver?.installNotifications()
    keyboardNotificationObserver?.customCallback = handleNewKeyboardHeight
    getComments()
  }
  
  // MARK: Keyboard
  func handleNewKeyboardHeight(height: CGFloat, animationDuration: CGFloat) {
    textInputViewBottomSpaceConstraint.constant = height
    tableView.contentInset = UIEdgeInsets(top: tableView.contentInset.top, left: tableView.contentInset.left, bottom: height + 50, right: tableView.contentInset.right)
    tableView.scrollIndicatorInsets = tableView.contentInset
    UIView.animateWithDuration(NSTimeInterval(animationDuration)) {
      self.view.layoutIfNeeded()
    }
  }
  
  // MARK: Create Comment
  @IBAction func okTapped(sender: UIButton) {
    if validateComment() {
      textInputTextField.resignFirstResponder()
      let text = textInputTextField.text
      textInputTextField.text = nil
      photo!.comment(text) {
        switch $0 {
        case .Success(let boxed):
          self.photo?.commentsCount++
          self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
        
      }
    } else {
      BannerView.present("Comentário inválido", inViewController: self)
    }
  }
  
  func validateComment() -> Bool {
    return count(textInputTextField.text) > 0
  }
  
  func insertNewlyCreatedComment(comment: Comment) {
    photo?.comments.append(comment)
    tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: photo!.comments.count + 1, inSection: 0)], withRowAnimation: .Top)
  }
  
  // MARK: Get Comments
  func getComments() {
    if loading || endReached {
      return
    }
    loading = true
    if let photo = photo {
      photo.photoComments(currentPage + 1) {
        switch $0 {
        case .Success(let boxed):
          if self.currentPage == 0 {
            photo.comments = []
          }
          self.endReached = !boxed.unbox.1
          photo.comments += boxed.unbox.0
          self.loading = false
          self.tableView.reloadData()
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
          self.loading = false
        }
      }
    }
  }
  
  func goToPhotoUser() {
    if let userProfileViewController = R.storyboard.main.userProfileViewController {
      userProfileViewController.user = photo!.user
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(userProfileViewController, animated: true)
    }
  }
  
  // MARK: Comment Actions
  func presentCommentOptions(index: Int) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("EDITAR COMENTÁRIO", {
      self.alerted = false
      self.editCommentAtIndex(index)
    }), ("DELETAR COMENTÁRIO", {
      self.alerted = false
      self.deleteCommentAtIndex(index)
    })])
  }
  
  func presentDeleteComment(index: Int) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("DELETAR COMENTÁRIO", {
      self.alerted = false
      self.deleteCommentAtIndex(index)
    })])
  }
  
  func presentReportComment(index: Int) {
    if alerted {
      return
    }
    alerted = true
    presentAlertWithButtons([("DENUNCIAR COMENTÁRIO", {
      self.alerted = false
      self.reportCommentAtIndex(index)
    })])
  }
  
  func deleteCommentAtIndex(index: Int) {
    let comment = self.photo!.comments[index]
    comment.delete(self.photo!.id) {
      switch $0 {
      case .Success(let boxed):
        self.photo!.comments.removeAtIndex(index)
        self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func reportCommentAtIndex(index: Int) {
    let comment = photo!.comments[index]
    comment.report(photo!.id) {
      switch $0 {
      case .Success(let boxed):
        self.presentBannerWithText("Obrigado. O autor do comentário será notificado.")
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func editCommentAtIndex(index: Int) {
    let comment = photo!.comments[index]
    editCommentAlert = AlertTextField.editCommentTextField(comment.text) { newComment, _ in
      if let newComment = newComment where newComment != comment.text {
        comment.edit(self.photo!.id, text: newComment) {
          switch $0 {
          case .Success(let boxed):
            if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? CommentCell {
              cell.updateComment(comment.text)
            }
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
        }
      }
      self.editCommentAlert = nil
    }
  }
  
  func presentAlertWithButtons(buttons: [ButtonType]) {
    let alertView = AlertView(buttons: buttons, cancelButtonTitle: nil)
    alertView.alertInViewController(self)
    alertView.callback = {
      self.alerted = false
    }
  }
  
  func presentBannerWithText(text: String) {
    let bannerView = BannerView(text: text)
    bannerView.presentInViewController(self)
  }
}

extension CommentsViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let newString = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
    okButton.enabled = count(newString) > 0
    return true
  }
}

extension CommentsViewController: CommentCellDelegate {
  func goToUserAtCommentCell(cell: CommentCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let comment = photo!.comments[indexPath.row]
      let user = comment.user!
      if let userProfileViewController = R.storyboard.main.userProfileViewController {
        userProfileViewController.user = user
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
        navigationController?.pushViewController(userProfileViewController, animated: true)
      }
    }
  }
  
  func presentOptionsAtCommentCell(cell: CommentCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let comment = photo?.comments[indexPath.row]
      let user = comment!.user!
      if User.currentUser.id == user.id {
        presentCommentOptions(indexPath.row)
      } else if User.currentUser.id == photo!.user!.id {
        presentDeleteComment(indexPath.row)
      } else {
        presentReportComment(indexPath.row)
      }
    }
  }
}
