//
//  StartViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
  @IBOutlet weak var exclusiveLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    exclusiveLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 90
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
  }
}
