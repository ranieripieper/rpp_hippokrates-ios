//
//  RegisterPhysicianViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RegisterPhysicianViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var avatarLabel: UILabel!
  @IBOutlet weak var emailTextField: TextField!
  @IBOutlet weak var passwordTextField: TextField!
  @IBOutlet weak var passwordConfirmationTextField: TextField!
  @IBOutlet weak var femaleButton: UIButton!
  @IBOutlet weak var maleButton: UIButton!
  @IBOutlet weak var formationSchoolTextField: TextField!
  @IBOutlet weak var formationSchoolNumberTextField: TextField!
  @IBOutlet weak var specialtyTextField: TextField!
  @IBOutlet weak var occupationTextField: TextField!
  @IBOutlet weak var residencySchoolTextField: TextField!
  @IBOutlet weak var professorSwitch: UISwitch!
  @IBOutlet weak var registerButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var bannerLabel: UILabel!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var selectedFormationSchool: School?
  var selectedSpecialty: Specialty?
  var selectedResidencySchool: School?
  var signupInfo: [String: String]!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    avatarImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
    bannerLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 50
    registerButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let destinationNavigationController = segue.destinationViewController as? UINavigationController {
      if let autocompleteViewController = destinationNavigationController.viewControllers.first as? AutocompleteViewController {
        if let textField = sender as? TextField {
          if textField == formationSchoolTextField || textField == residencySchoolTextField {
            let schools = School.all() {
              switch $0 {
              case .Success(let boxed):
                autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
            autocompleteViewController.datasource = schools.map() { $0 as NamedObject }
            autocompleteViewController.callback = {
              if let school = $0 as? School {
                if textField == self.formationSchoolTextField {
                  self.selectedFormationSchool = school
                } else {
                  self.selectedResidencySchool = school
                }
                textField.text = school.name
              }
              self.dismissViewControllerAnimated(true, completion: nil)
            }
          } else if textField == specialtyTextField {
            let specialties = Specialty.all() {
              switch $0 {
              case .Success(let boxed):
                autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
            autocompleteViewController.datasource = specialties.map() { $0 as NamedObject }
            autocompleteViewController.callback = {
              if let specialty = $0 as? Specialty {
                self.selectedSpecialty = specialty
                textField.text = specialty.name
              }
              self.dismissViewControllerAnimated(true, completion: nil)
            }
          }
        }
      }
    }
  }
  
  // MARK: Avatar
  @IBAction func avatarTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    self.imagePicker = ImagePicker(viewController: self) { image in
      if image != nil {
        self.avatarImageView.image = image!
        self.avatarImageView.contentMode = .ScaleAspectFill
      }
      self.imagePicker = nil
    }
    self.imagePicker?.presentImagePicker()
  }
  
  // MARK: Gender
  @IBAction func genderTapped(sender: UIButton) {
    if !sender.selected {
      femaleButton.selected = sender == femaleButton
      maleButton.selected = sender == maleButton
    } else {
      femaleButton.selected = false
      maleButton.selected = false
    }
  }
  
  // MARK: Register
  @IBAction func registerTapped(sender: UISwitch) {
    if validatePasswords() {
      if validateFields() {
        register()
      } else {
        invalidFields()
      }
    } else {
      invalidPasswords()
    }
  }
  
  func validatePasswords() -> Bool {
    return count(passwordTextField.text) > 0 && passwordTextField.text == passwordConfirmationTextField.text
  }
  
  func validateFields() -> Bool {
    return count(emailTextField.text) > 0
  }
  
  func invalidPasswords() {
    presentBannerWithText("Por favor, verifique se as senhas conferem")
  }
  
  func invalidFields() {
    presentBannerWithText("Por favor, preencha todos os campos abaixo")
  }
  
  func presentBannerWithText(text: String) {
    let bannerView = BannerView(text: text)
    bannerView.presentInViewController(self)
  }
  
  func register() {
    #if RELEASE
      Mixpanel.sharedInstance().track("Register", properties: ["profile_type": professorSwitch.on ? "teacher" : "doctor"])
    #endif
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    User.register(emailTextField.text, password: passwordTextField.text, firstName: signupInfo["name"]!, lastName: signupInfo["lastName"]!, crm: signupInfo["crm"] ?? "", crmState: signupInfo["crmState"] ?? "", gender: (femaleButton.selected ? Gender.Female : maleButton.selected ? Gender.Male : Gender.Undefinied), specialty: selectedSpecialty, occupation: occupationTextField.text, formationSchool: selectedFormationSchool, formationClassNumber: formationSchoolNumberTextField.text, residencySchool: selectedResidencySchool, code: signupInfo["code"] ?? "", avatar: avatarImageView.image, professor: professorSwitch.on) {
      switch $0 {
      case .Success(let boxed):
        self.registered()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
      JHProgressHUD.sharedHUD.hide()
    }
  }
  
  func registered() {
    goHome()
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Flow
  func goHome() {
    (UIApplication.sharedApplication().delegate as! AppDelegate).loadMainApplicationFlow()
  }
}

extension RegisterPhysicianViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    performSegueWithIdentifier(R.segue.segueAutocomplete, sender: textField)
    return false
  }
}
