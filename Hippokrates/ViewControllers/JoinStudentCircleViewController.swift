//
//  JoinStudentCircleViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 6/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class JoinStudentCircleViewController: UIViewController {
  @IBOutlet weak var textField: TextField!
  
  func send() {
    if validateFields() {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      Circle.joinCircle(textField.text) {
        switch $0 {
        case .Success:
          User.currentUser.myCircles(1)
          self.dismiss()
        case .Failure(let error):
          self.banner("Código inválido. Por favor, tente novamente.")
        }
        JHProgressHUD.sharedHUD.hide()
      }
    } else {
      banner("Por favor, preencha o código no campo abaixo.")
    }
  }
  
  func validateFields() -> Bool {
    return count(textField.text) > 0
  }
  
  // MARK: Flow
  func dismiss() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  // MARK: Alerts
  func banner(text: String) {
    let banner = BannerView(text: text)
    banner.presentInViewController(self)
  }
  
  // MARK: Actions
  @IBAction func doneTapped(sender: UIButton) {
    send()
  }
  
  @IBAction func cancelTapped(sender: UIButton) {
    dismiss()
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
}

extension JoinStudentCircleViewController: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if validateFields() {
      send()
    }
    return true
  }
}
