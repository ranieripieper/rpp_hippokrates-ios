//
//  SearchViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum SearchType {
  case Specialty, User, Hashtag
}

class SearchViewController: UIViewController {
  @IBOutlet weak var textField: TextField!
  @IBOutlet weak var categoryButton: UIButton!
  @IBOutlet weak var userButton: UIButton!
  @IBOutlet weak var hashtagButton: UIButton!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var menuView: UIView!
  @IBOutlet weak var menuViewBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var searchTableViewDelegate: SearchTableViewDelegate!
  
  var loading = false
  var endReached = false
  var currentPage = 0
  var selectedType: SearchType = .User
  var mainNavigationController: MainNavigationController? {
    if let mainNavigationController = navigationController as? MainNavigationController {
      return mainNavigationController
    }
    return nil
  }
  var currentTask: NSURLSessionDataTask?
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    tableView.registerNib(R.nib.autocompleteCell.instance, forCellReuseIdentifier: R.reuseIdentifier.autocompleteCell.identifier)
    tableView.registerNib(R.nib.userCell.instance, forCellReuseIdentifier: R.reuseIdentifier.userCell.identifier)
    textField.becomeFirstResponder()
  }
  
  override func viewWillAppear(animated: Bool) {
    if let indexPath = tableView.indexPathForSelectedRow() {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let searchResultsViewController = segue.destinationViewController as? SearchResultsViewController {
      if let specialty = sender as? Specialty {
        searchResultsViewController.searchSpecialty = specialty
      } else if let hashtag = sender as? String {
        searchResultsViewController.searchHashtag = hashtag
      }
    }
  }
  
  // MARK: Type
  @IBAction func selectSpecialty(sender: UIButton) {
    selectButton(sender)
    categoryButton.backgroundColor = UIColor(red: 236 / 255, green: 111 / 255, blue: 102 / 255, alpha: 1)
    userButton.backgroundColor = UIColor(red: 246 / 255, green: 246 / 255, blue: 246 / 255, alpha: 1)
    hashtagButton.backgroundColor = UIColor(red: 246 / 255, green: 246 / 255, blue: 246 / 255, alpha: 1)
    searchTableViewDelegate.datasource = Specialty.all() { result in
      switch result {
      case .Success(let boxed):
        self.searchTableViewDelegate.datasource = boxed.unbox
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
    changeToType(.Specialty)
    textField.becomeFirstResponder()
  }
  
  @IBAction func selectUser(sender: UIButton) {
    selectButton(sender)
    categoryButton.backgroundColor = UIColor(red: 246 / 255, green: 246 / 255, blue: 246 / 255, alpha: 1)
    userButton.backgroundColor = UIColor(red: 236 / 255, green: 111 / 255, blue: 102 / 255, alpha: 1)
    hashtagButton.backgroundColor = UIColor(red: 246 / 255, green: 246 / 255, blue: 246 / 255, alpha: 1)
    searchTableViewDelegate.datasource = []
    changeToType(.User)
    textField.becomeFirstResponder()
  }
  
  @IBAction func selectHashtag(sender: UIButton) {
    selectButton(sender)
    categoryButton.backgroundColor = UIColor(red: 246 / 255, green: 246 / 255, blue: 246 / 255, alpha: 1)
    userButton.backgroundColor = UIColor(red: 246 / 255, green: 246 / 255, blue: 246 / 255, alpha: 1)
    hashtagButton.backgroundColor = UIColor(red: 236 / 255, green: 111 / 255, blue: 102 / 255, alpha: 1)
    searchTableViewDelegate.datasource = []
    changeToType(.Hashtag)
    textField.becomeFirstResponder()
  }
  
  func selectButton(button: UIButton) {
    if button.selected {
      return
    }
    categoryButton.selected = button == categoryButton
    userButton.selected = button == userButton
    hashtagButton.selected = button == hashtagButton
  }
  
  func changeToType(type: SearchType) {
    selectedType = type
    tableView.reloadData()
  }
  
  // MARK: Search
  func searchUserWithText(text: String) {
    if loading || endReached {
      return
    }
    loading = true
    User.searchUsers(text, page: currentPage + 1, onlyPhysicians: false) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.searchTableViewDelegate.datasource.removeAll(keepCapacity: false)
        }
        self.searchTableViewDelegate.datasource += boxed.unbox.0.map() { $0 as AnyObject }
        self.endReached = !boxed.unbox.1
        self.loading = false
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        self.endReached = true
        self.loading = false
      }
    }
  }
  
  // MARK: Hashtags
  func getHashtags(string: String) {
    if loading || endReached {
      return
    }
    loading = true
    currentTask = Photo.getHashtags(string, page: currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.searchTableViewDelegate.datasource.removeAll(keepCapacity: false)
        }
        self.searchTableViewDelegate.datasource += boxed.unbox.0.map() { $0 as AnyObject }
        self.endReached = !boxed.unbox.1
        self.loading = false
        self.tableView.reloadData()
      case .Failure(let error):
        if error.description != "cancelled" {
          BannerView.present(error.description, inViewController: self)
        }
        self.endReached = true
        self.loading = false
      }
    }
  }
  
  // MARK: Result
  func resultsWithSpecialty(specialty: Specialty) {
    if IOS_8() {
      performSegueWithIdentifier(R.segue.segueResults, sender: specialty)
    } else {
      performSegueWithIdentifier(R.segue.segueResultsIOS7, sender: specialty)
    }
  }
  
  func resultsWithUser(user: User) {
    if let userProfileViewController = R.storyboard.main.userProfileViewController {
      userProfileViewController.user = user
      navigationController?.pushViewController(userProfileViewController, animated: true)
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func resultsWithHashtag(hashtag: String) {
    if IOS_8() {
      performSegueWithIdentifier(R.segue.segueResults, sender: hashtag)
    } else {
      performSegueWithIdentifier(R.segue.segueResultsIOS7, sender: hashtag)
    }
  }
  
  // MARK: Menu
  @IBAction func timelineTapped(sender: UIButton) {
    mainNavigationController?.showTimeline()
  }
  
  @IBAction func photoTapped(sender: UIButton) {
    presentViewController(R.storyboard.newPhoto.initialViewController!, animated: true, completion: nil)
  }
  
  @IBAction func menuTapped(sender: UIButton) {
    mainNavigationController?.performSegueWithIdentifier(R.segue.segueMenu, sender: nil)
  }
  
  func hideMenuAtIndex(index: CGFloat, animated: Bool) {
    menuViewBottomSpaceConstraint.constant = -menuView.bounds.height * index
    if animated {
      UIView.animateWithDuration(0.3) {
        self.view.layoutIfNeeded()
      }
    }
  }
}

extension SearchViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let text = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
    switch selectedType {
    case .User:
      endReached = false
      currentPage = 0
      searchUserWithText(text)
    case .Hashtag:
      currentTask?.cancel()
      currentTask = nil
      endReached = false
      currentPage = 0
      loading = false
      getHashtags(text)
    case .Specialty:
      searchTableViewDelegate.filteredDatasource = searchTableViewDelegate.datasource.filter {
        if let specialty = $0 as? Specialty {
          return specialty.name.rangeOfString(text, options: .CaseInsensitiveSearch | .DiacriticInsensitiveSearch) != nil
        }
        return false
      }
      tableView.reloadData()
    }
    return true
  }
}
