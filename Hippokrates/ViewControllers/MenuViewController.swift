//
//  MenuViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var circlesTableView: UITableView!
  @IBOutlet weak var menuClosePanGesture: UIPanGestureRecognizer!
  @IBOutlet weak var mainNavigationController: MainNavigationController!
  @IBOutlet weak var circlesTableViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var circlesButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var inviteFriendsHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var subscriptionHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var leftConstraint: NSLayoutConstraint!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    circlesButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width * 0.9
    circlesTableViewHeightConstraint.constant = 0
    view.addGestureRecognizer(menuClosePanGesture)
    if User.currentUser.type == .Student {
      inviteFriendsHeightConstraint.constant = 0
      subscriptionHeightConstraint.constant = 0
    }
    if !IOS_8() {
      view.backgroundColor = scrollView.backgroundColor
      leftConstraint.constant = -0.1 * UIScreen.mainScreen().bounds.width
    }
  }
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .LightContent
  }
  
  // MARK: Close
  @IBAction func menuClosePanGestureRecognizer(menuPanGestureRecognizer: UIPanGestureRecognizer) {
    if menuPanGestureRecognizer.state == .Began {
      let menuAnimator = transitioningDelegate as! MenuAnimator
      menuAnimator.interactive = true
      dismissViewControllerAnimated(true, completion: nil)
    } else if menuPanGestureRecognizer.state == .Changed {
      let translation = menuPanGestureRecognizer.translationInView(view)
      let d = max(0, translation.x / CGRectGetWidth(view.bounds))
      let menuAnimator = transitioningDelegate as! MenuAnimator
      menuAnimator.updateInteractiveTransition(d)
    } else if menuPanGestureRecognizer.state == .Ended {
      let menuAnimator = transitioningDelegate as! MenuAnimator
      if menuPanGestureRecognizer.velocityInView(view).x > 0 {
        menuAnimator.finishInteractiveTransition()
      } else {
        menuAnimator.cancelInteractiveTransition()
      }
      menuAnimator.interactive = false
    }
  }
  
  @IBAction func closeTapped(sender: UIButton) {
    close(nil)
  }
  
  func close(completion: (() -> ())?) {
    dismissViewControllerAnimated(true, completion: completion)
  }
  
  // MARK: Circles
  @IBAction func toggleCircles(sender: UIButton) {
    sender.selected = !sender.selected
    if sender.selected {
      let numberOfSections = circlesTableView.numberOfSections()
      circlesTableViewHeightConstraint.constant = (CGFloat(numberOfSections - 1) + CGFloat(circlesTableView.numberOfRowsInSection(numberOfSections - 1))) * circlesTableView.rowHeight
    } else {
      circlesTableViewHeightConstraint.constant = 0
    }
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  func createNewCircle() {
    dispatch_async(dispatch_get_main_queue()) {
      self.close() {
        self.mainNavigationController.pushViewController(R.storyboard.main.createCircleViewController!, animated: true)
        (self.mainNavigationController.viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      }
    }
  }
  
  func createNewStudentCircle() {
    dispatch_async(dispatch_get_main_queue()) {
      self.close() {
        if let createSchoolCircleViewController = R.storyboard.login.createSchoolCircleViewController {
          self.mainNavigationController.pushViewController(createSchoolCircleViewController, animated: true)
          (self.mainNavigationController.viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
        }
      }
    }
  }
  
  func joinStudentCircle() {
    dispatch_async(dispatch_get_main_queue()) {
      self.close() {
        if let joinStudentCircleNavigationController = R.storyboard.main.joinStudentCircleNavigationController {
          self.mainNavigationController.presentViewController(joinStudentCircleNavigationController, animated: true, completion: nil)
        }
      }
    }
  }
  
  func goToCircle(circle: Circle) {
    dispatch_async(dispatch_get_main_queue()) {
      self.close() {
        let circleViewController = R.storyboard.main.circleViewController!
        circleViewController.circle = circle
        self.mainNavigationController.pushViewController(circleViewController, animated: true)
        (self.mainNavigationController.viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      }
    }
  }
  
  // MARK: Notifications
  @IBAction func notificationsTapped(sender: UIButton) {
    close() {
      self.mainNavigationController.showNotifications()
    }
  }
  
  // MARK: History
  @IBAction func historyTapped(sender: UIButton) {
    close() {
      self.mainNavigationController.showHistory()
    }
  }
  
  // MARK: Invite Friends
  @IBAction func inviteFriendsTapped(sender: UIButton) {
    close() {
      self.mainNavigationController.showInvite()
    }
  }
  
  // MARK: Subscription
  @IBAction func subscriptionTapped(sender: UIButton) {
    close() {
      self.mainNavigationController.showSubscription()
    }
  }
  
  // MARK: Settings
  @IBAction func settingsTapped(sender: UIButton) {
    close() {
      self.mainNavigationController.showSettings()
    }
  }
  
  // MARK: About
  @IBAction func aboutTapped(sender: UIButton) {
    close() {
      self.mainNavigationController.showAbout()
    }
  }
  
  // MARK: Logout
  @IBAction func logoutTapped(sender: UIButton) {
    SSKeychain.deletePasswordForService("us.hippokrates", account: "authToken")
    NetworkManager.sharedInstance.logout()
    NetworkManager.sharedInstance.requestSerializer.setValue(nil, forHTTPHeaderField: "X-Token")
    NSUserDefaults.standardUserDefaults().removeObjectForKey("PushTokenRegistered")
    NSUserDefaults.standardUserDefaults().synchronize()
    dismissViewControllerAnimated(false, completion: nil)
    (UIApplication.sharedApplication().delegate as! AppDelegate).loadLoginFlow()
  }
}
