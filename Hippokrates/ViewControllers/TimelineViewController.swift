//
//  TimelineViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TimelineViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var menuViewBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var menuView: UIView!
  @IBOutlet weak var photosTableViewDelegate: PhotosTableViewDelegate!
  
  var mainNavigationController: MainNavigationController? {
    if let mainNavigationController = navigationController as? MainNavigationController {
      return mainNavigationController
    }
    return nil
  }
  var loading = false
  var endReached = false
  var currentPage = 0
  
  var refreshControl = UIRefreshControl()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(R.nib.photoCell.instance, forCellReuseIdentifier: R.reuseIdentifier.photoCell.identifier)
    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
//    if User.currentUser.remainingSubscriptionDays < 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    if IOS_8() {
      tableView.estimatedRowHeight = 402
    }
    refreshControl.tintColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1)
    refreshControl.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
    tableView.addSubview(refreshControl)
    loadPhotos()
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "deletePhotosWithIds:", name: "DeletePhotos", object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "refresh", name: "Refresh", object: nil)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let indexPath = tableView.indexPathForSelectedRow() {
      tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    } else if let indexPaths = tableView.indexPathsForVisibleRows() as? [NSIndexPath] {
      tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: .None)
    }
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    if navigationController == nil {
      for row in 0...tableView.numberOfRowsInSection(0) {
        if let photoCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0)) as? PhotoCell {
          photoCell.delegate = nil
        }
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let photoDetailViewController = segue.destinationViewController as? PhotoDetailViewController {
      if let photo = sender as? Photo {
        photoDetailViewController.photo = photo
      }
    }
  }
  
  // MARK: Data
  func loadPhotos() {
    if loading || endReached {
      return
    }
    loading = true
    User.currentUser.feed(currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.photosTableViewDelegate.datasource.removeAll(keepCapacity: false)
        }
        self.photosTableViewDelegate.datasource += boxed.unbox.0
        self.currentPage++
        self.endReached = !boxed.unbox.1
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
      self.refreshControl.endRefreshing()
      self.loading = false
      JHProgressHUD.sharedHUD.hide()
    }
  }
  
  func refresh() {
    loading = false
    endReached = false
    currentPage = 0
    loadPhotos()
    User.currentUser.me(nil)
  }
  
  func deletePhotosWithIds(notification: NSNotification) {
    if let userInfo = notification.userInfo, let ids = userInfo["ids"] as? [Int] {
      photosTableViewDelegate.datasource = photosTableViewDelegate.datasource.filter {
        find(ids, $0.id) == nil
      }
      tableView.reloadData()
      refresh()
    }
  }
  
  // MARK: Flow
  func goToPhoto(photo: Photo) {
    performSegueWithIdentifier(R.segue.seguePhotoDetail, sender: photo)
  }
  
  func goToUser(user: User) {
    if let userProfileViewController = R.storyboard.main.userProfileViewController {
      userProfileViewController.user = user
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(userProfileViewController, animated: true)
    }
  }
 
  // MARK: Menu
  @IBAction func searchTapped(sender: UIButton) {
    mainNavigationController?.showSearch()
  }
  
  @IBAction func photoTapped(sender: UIButton) {
    presentViewController(R.storyboard.newPhoto.initialViewController!, animated: true, completion: nil)
  }
  
  @IBAction func menuTapped(sender: UIButton) {
    mainNavigationController?.performSegueWithIdentifier(R.segue.segueMenu, sender: nil)
  }
  
  func hideMenuAtIndex(index: CGFloat, animated: Bool) {
    menuViewBottomSpaceConstraint.constant = -menuView.bounds.height * index
    if animated {
      UIView.animateWithDuration(0.3) {
        self.view.layoutIfNeeded()
      }
    }
  }
  
  func presentEditDeletePhoto(photo: Photo) {
    alert([("EDITAR FOTO", {
      self.editPhoto(photo)
    }), ("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          photo.delete() {
            switch $0 {
            case .Success(let boxed):
              self.photosTableViewDelegate.datasource = self.photosTableViewDelegate.datasource.filter() {
                $0.id != photo.id
              }
              self.tableView.reloadData()
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
        }
      }
    })])
  }
  
  func presentDeletePhoto(photo: Photo) {
    alert([("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          if let circleId = photo.circleId {
            photo.adminDeleteFromCircle(circleId) {
              switch $0 {
              case .Success(let boxed):
                self.photosTableViewDelegate.datasource = self.photosTableViewDelegate.datasource.filter() {
                  $0.id != photo.id
                }
                self.tableView.reloadData()
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
          }
        }
      }
    })])
  }
  
  func editPhoto(photo: Photo) {
    if let editPictureViewController = R.storyboard.newPhoto.editPictureViewController {
      editPictureViewController.photo = photo
      let aNavigationController = UINavigationController(rootViewController: editPictureViewController)
      aNavigationController.navigationBar.barStyle = .Black
      aNavigationController.navigationBar.barTintColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1)
      aNavigationController.navigationBar.tintColor = UIColor.whiteColor()
      editPictureViewController.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: R.image.btn_camera_action_close, style: .Bordered, target: self, action: "cancelEditPhoto")]
      presentViewController(aNavigationController, animated: true, completion: nil)
    }
  }
  
  func cancelEditPhoto() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func presentReportPhoto(photo: Photo) {
    alert([("DENUNCIAR FOTO", {
      self.presentReportOptions(photo)
    })])
  }
  
  func presentReportOptions(photo: Photo) {
    alert([("FOTO IMPRÓPRIA", {
      photo.report(.Improper) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    }), ("IDENTIFIQUEI O PACIENTE", {
      photo.report(.Patient) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    })])
  }
  
  func reportCallback(success: Bool) {
    if success {
      banner("Obrigado. O autor da foto será notificado.")
    } else {
      banner("Você já denunciou esta foto uma vez.")
    }
  }
  
  // MARK: Alerts
  func alert(buttons: [ButtonType]) {
    let alertView = AlertView(buttons: buttons, cancelButtonTitle: nil)
    alertView.alertInViewController(self)
  }
  
  func alert(text: String, callback: Bool -> ()) {
    let alertView = AlertView(text: text, buttons: [("SIM", {
      callback(true)
    }), ("Não", {
      callback(false)
    })])
    alertView.alertInViewController(self)
  }
  
  func banner(text: String) {
    let banner = BannerView(text: text)
    banner.presentInViewController(self)
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: "DeletePhotos", object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: "photoUploaded", object: nil)
  }
}

extension TimelineViewController: PhotoCellDelegate {
  func likeAtCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = photosTableViewDelegate.datasource[indexPath.row]
      photo.toggleLike() {
        switch $0 {
        case .Success(let boxed):
          cell.updateLikes(photo)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
  }
  
  func commentCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell), commentsViewController = R.storyboard.main.commentsViewController {
      let photo = photosTableViewDelegate.datasource[indexPath.row]
      commentsViewController.photo = photo
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(commentsViewController, animated: true)
    }
  }
  
  func goToCircleAtCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell), circleViewController = R.storyboard.main.circleViewController {
      let photo = photosTableViewDelegate.datasource[indexPath.row]
      if let circle = photo.circle where circle.type != "public" {
        circleViewController.circle = circle
      } else {
        circleViewController.specialty = photo.specialty
      }
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(circleViewController, animated: true)
    }
  }
  
  func goToUserAtPhotoCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = photosTableViewDelegate.datasource[indexPath.row]
      let user = photo.user!
      goToUser(user)
    }
  }
  
  func optionsForCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = photosTableViewDelegate.datasource[indexPath.row]
      if photo.userId! == User.currentUser.id {
        presentEditDeletePhoto(photo)
      } else if photo.circle?.adminId == User.currentUser.id {
        presentDeletePhoto(photo)
      } else {
        presentReportPhoto(photo)
      }
    }
  }
}
