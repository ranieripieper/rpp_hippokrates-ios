//
//  SubscriptionRenewalViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SubscriptionRenewalViewController: UIViewController {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var monthlyValueLabel: UILabel!
  @IBOutlet weak var semesterlyValueLabel: UILabel!
  @IBOutlet weak var yearlyValueLabel: UILabel!
  
  var products = [String]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    configure()
  }
  
  func configure() {
    RMStore.defaultStore().requestProducts(Set(["us.hippokrates.onemonth", "us.hippokrates.sixmonth", "us.hippokrates.oneyear"]), success: { products, invalidProducts in
      let numberFormatter = NSNumberFormatter()
      numberFormatter.formatterBehavior = .Behavior10_4
      numberFormatter.numberStyle = .CurrencyStyle
      if let products = products as? [SKProduct] {
        for product in products {
          if product.localizedTitle == "Plano Mensal" {
            numberFormatter.locale = product.priceLocale
            self.monthlyValueLabel.text = numberFormatter.stringFromNumber(product.price)
          } else if product.localizedTitle == "Plano Semestral" {
            numberFormatter.locale = product.priceLocale
            self.semesterlyValueLabel.text = numberFormatter.stringFromNumber(product.price)
          } else if product.localizedTitle == "Plano Anual" {
            numberFormatter.locale = product.priceLocale
            self.yearlyValueLabel.text = numberFormatter.stringFromNumber(product.price)
          }
        }
        self.products = products.map() { $0.productIdentifier }
      }
    }) { error in
      BannerView.present(error.description, inViewController: self)
    }
    nameLabel.text = User.currentUser.name
  }
  
  // MARK: Renew
  @IBAction func renewMonthly(sender: UIButton) {
    let monthly = products.filter() {
      $0 == "us.hippokrates.onemonth"
    }.first
    if monthly != nil {
      RMStore.defaultStore().addPayment(monthly, success: { transaction in
        if transaction.transactionState == .Purchased {
          if let receiptURL = NSBundle.mainBundle().appStoreReceiptURL, let receipt = NSData(contentsOfURL: receiptURL) {
            
          }
        }
      }, failure: { transaction, error in
        BannerView.present(error.localizedDescription, inViewController: self)
      })
    }
  }
  
  @IBAction func renewSemesterly(sender: UIButton) {
    let semesterly = products.filter() {
      $0 == "us.hippokrates.sixmonth"
      }.first
    if semesterly != nil {
      RMStore.defaultStore().addPayment(semesterly, success: { transaction in
        if transaction.transactionState == .Purchased {
          if let receiptURL = NSBundle.mainBundle().appStoreReceiptURL, let receipt = NSData(contentsOfURL: receiptURL) {
            
          }
        }
        }, failure: { transaction, error in
          BannerView.present(error.localizedDescription, inViewController: self)
      })
    }
  }
  
  @IBAction func renewYearly(sender: UIButton) {
    let yearly = products.filter() {
      $0 == "us.hippokrates.oneyear"
      }.first
    if yearly != nil {
      RMStore.defaultStore().addPayment(yearly, success: { transaction in
        if transaction.transactionState == .Purchased {
          if let receiptURL = NSBundle.mainBundle().appStoreReceiptURL, let receipt = NSData(contentsOfURL: receiptURL) {
            
          }
        }
        }, failure: { transaction, error in
          BannerView.present(error.localizedDescription, inViewController: self)
      })
    }
  }
}
