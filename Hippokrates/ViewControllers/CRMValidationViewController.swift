//
//  CRMValidationViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MessageUI

class CRMValidationViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var crmTextField: TextField!
  @IBOutlet weak var crmStateTextField: TextField!
  @IBOutlet weak var firstNameTextField: TextField!
  @IBOutlet weak var lastNameTextField: TextField!
  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var crmValidateButtonTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var crmValidateButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var signupInfo: [String: String]!
  var graduation = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    crmValidateButtonTopSpaceConstraint.constant = UIScreen.mainScreen().bounds.height - 64 - 70
    crmValidateButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    if graduation {
      navigationItem.title = ""
      headerLabel.text = "Parabéns!\nPor favor, valide deu CRM."
//      if User.currentUser.remainingSubscriptionDays > 60 {
//        TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//      }
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textFieldDelegate.returnCallback = textFieldDidReturn
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    textFieldDelegate.returnCallback = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let termsViewController = segue.destinationViewController as? TermsViewController {
      termsViewController.signupInfo = ["name": firstNameTextField.text, "lastName": lastNameTextField.text, "crm": crmTextField.text, "crmState": crmStateTextField.text, "code": signupInfo["code"]!]
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Validate
  @IBAction func validateTapped(sender: UIButton?) {
    if validateFields() {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      if !graduation {
        User.validateCRM(crmTextField.text, crmState: State(string: crmStateTextField.text), firstName: firstNameTextField.text, lastName: lastNameTextField.text) { result in
          switch result {
          case .Success(let boxed):
            #if RELEASE
              Mixpanel.sharedInstance().track("Validate CRM", properties: ["valid": true])
            #endif
            self.performSegueWithIdentifier(R.segue.segueTerms, sender: nil)
          case .Failure(let error):
            #if RELEASE
              Mixpanel.sharedInstance().track("Validate CRM", properties: ["valid": false])
            #endif
            self.invalidFields()
          }
          JHProgressHUD.sharedHUD.hide()
        }
      } else {
        User.currentUser.graduate(crmTextField.text, crmState: State(string: crmStateTextField.text), firstName: firstNameTextField.text, lastName: lastNameTextField.text) {
          switch $0 {
          case .Success:
            self.navigationController?.popToRootViewControllerAnimated(true)
            NSNotificationCenter.defaultCenter().postNotificationName("Refresh", object: nil)
            #if RELEASE
              Mixpanel.sharedInstance().track("Validate CRM", properties: ["valid": true, "graduation": true])
            #endif
          case .Failure(let error):
            #if RELEASE
              Mixpanel.sharedInstance().track("Validate CRM", properties: ["valid": true, "graduation": false])
            #endif
            self.invalidFields()
          }
          JHProgressHUD.sharedHUD.hide()
        }
      }
    } else {
      invalidFields()
    }
  }
  
  func validateFields() -> Bool {
    return count(crmTextField.text) > 0 && count(crmStateTextField.text) > 0 && count(firstNameTextField.text) > 0 && count(lastNameTextField.text) > 0
  }
  
  func invalidFields() {
    let bannerView = BannerView(text: "Infelizmente não conseguimos validar as informações. Por favor, preencha corretamente os campos abaixo.")
    bannerView.presentInViewController(self)
  }
  
  func textFieldDidReturn(textField: UITextField) {
    if textField == lastNameTextField {
      validateTapped(nil)
    }
  }
  
  // MARK: Contact
  @IBAction func contactTapped(sender: UIButton) {
    var composer = MFMailComposeViewController()
    composer.setToRecipients(["contato@hippokrates.us"])
    presentViewController(composer, animated: true, completion: nil)
  }
}

extension CRMValidationViewController: UINavigationControllerDelegate {
  
}

extension CRMValidationViewController: MFMailComposeViewControllerDelegate {
  func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
