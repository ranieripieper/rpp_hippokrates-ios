//
//  EditNotificationsViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class EditNotificationsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var editNotificationsTableViewDelegate: EditNotificationsTableViewDelegate!
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    if IOS_8() {
      tableView.estimatedRowHeight = 44
    }
    getPreferences()
  }
  
  func getPreferences() {
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    User.currentUser.preferences() {
      switch $0 {
      case .Success(let boxed):
        self.editNotificationsTableViewDelegate.datasource = boxed.unbox
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
      JHProgressHUD.sharedHUD.hide()
    }
  }
  
  @IBAction func backTapped(sender: UIBarButtonItem) {
    save() {
      self.navigationController?.popViewControllerAnimated(true)
    }
  }
  
  func save(completion: () -> ()) {
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    User.currentUser.updatePreferences(editNotificationsTableViewDelegate.datasource) {
      JHProgressHUD.sharedHUD.hide()
      switch $0 {
      case .Success:
        completion()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
          Int64(0.3 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            completion()
        }
      }
    }
  }
}
