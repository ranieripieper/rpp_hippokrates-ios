//
//  CreateSchoolCircleViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CreateSchoolCircleViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var circlePictureImageView: UIImageView!
  @IBOutlet weak var circlePictureLabel: UILabel!
  @IBOutlet weak var schoolTextField: TextField!
  @IBOutlet weak var classTextField: TextField!
  @IBOutlet weak var classFromTextField: TextField!
  @IBOutlet weak var classToTextField: TextField!
  @IBOutlet weak var circleNameTextField: TextField!
  @IBOutlet weak var circleNumberOfStudents: TextField!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  @IBOutlet weak var inviteButtonWidthConstraint: NSLayoutConstraint!
  
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var delegate: InviteDelegate?
  var selectedSchool: School?
  var datePicker: UIDatePicker = {
    let datePicker = UIDatePicker()
    datePicker.datePickerMode = .Date
    return datePicker
  }()
  var toolbar: UIToolbar?
  var fromDate: NSDate?
  var toDate: NSDate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    circlePictureImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
    inviteButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    classFromTextField.inputView = datePicker
    classFromTextField.inputAccessoryView = toolbar
    classToTextField.inputView = datePicker
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textFieldDelegate.returnCallback = textFieldDidReturn
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    textFieldDelegate.returnCallback = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let destinationNavigationController = segue.destinationViewController as? UINavigationController {
      if let autocompleteViewController = destinationNavigationController.viewControllers.first as? AutocompleteViewController {
        if let textField = sender as? TextField {
          let schools = School.all() {
            switch $0 {
            case .Success(let boxed):
              autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
          autocompleteViewController.datasource = schools.map() { $0 as NamedObject }
          autocompleteViewController.callback = {
            self.selectedSchool = $0 as? School
            textField.text = self.selectedSchool?.name
            self.dismissViewControllerAnimated(true, completion: nil)
          }
        }
      }
    } else if let newSchoolCircleCodeViewController = segue.destinationViewController as? NewSchoolCircleCodeViewController, let circle = sender as? Circle {
      newSchoolCircleCodeViewController.circle = circle
      newSchoolCircleCodeViewController.schoolName = schoolTextField.text
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  func textFieldDidReturn(textField: UITextField) {
    if textField == circleNumberOfStudents {
      inviteTapped(nil)
    }
  }
  
  // MARK: Picture
  @IBAction func pictureTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    self.imagePicker = ImagePicker(viewController: self) { image in
      if image != nil {
        self.circlePictureImageView.image = image!
        self.circlePictureImageView.contentMode = .ScaleAspectFill
      }
      self.imagePicker = nil;
    }
    self.imagePicker?.presentImagePicker()
  }
  
  // MARK: Invite
  @IBAction func inviteTapped(sender: UIButton?) {
    if validateFields() {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      Circle.createStudentCircle(circleNameTextField.text, image: circlePictureImageView.image, schoolName: schoolTextField.text, subject: classTextField.text, fromDate: fromDate, toDate: toDate, limit: circleNumberOfStudents.text.toInt() ?? 0) {
        switch $0 {
        case .Success(let boxed):
          self.performSegueWithIdentifier(R.segue.segueSuccess, sender: boxed.unbox)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
        JHProgressHUD.sharedHUD.hide()
      }
    } else {
      let bannerView = BannerView(text: "Por favor, preencha todos os campos abaixo.")
      bannerView.presentInViewController(self)
    }
  }
  
  func validateFields() -> Bool {
    return count(schoolTextField.text) > 0 && count(circleNameTextField.text) > 0 && count(circleNumberOfStudents.text) > 0
  }
  
  func cancelFromDate() {
    classFromTextField.resignFirstResponder()
    fromDate = nil
    classFromTextField.text = ""
  }
  
  func cancelToDate() {
    classToTextField.resignFirstResponder()
    toDate = nil
    classToTextField.text = ""
  }
}

extension CreateSchoolCircleViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if textField == schoolTextField {
      performSegueWithIdentifier(R.segue.segueSchool, sender: schoolTextField)
      return false
    } else if textField == classFromTextField {
      toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 44))
      toolbar?.items = [UIBarButtonItem(title: "Cancelar", style: .Bordered, target: self, action: "cancelFromDate"), UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Ok", style: .Bordered, target: classFromTextField, action: "resignFirstResponder")]
      textField.inputAccessoryView = toolbar!
    } else if textField == classToTextField {
      toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 44))
      toolbar?.items = [UIBarButtonItem(title: "Cancelar", style: .Bordered, target: self, action: "cancelFromDate"), UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Ok", style: .Bordered, target: classToTextField, action: "resignFirstResponder")]
      textField.inputAccessoryView = toolbar!
    }
    return true
  }
  
  func textFieldDidEndEditing(textField: UITextField) {
    if textField == classFromTextField {
      fromDate = datePicker.date
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "dd/MM/yyyy"
      textField.text = dateFormatter.stringFromDate(datePicker.date)
    } else if textField == classToTextField {
      toDate = datePicker.date
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "dd/MM/yyyy"
      textField.text = dateFormatter.stringFromDate(datePicker.date)
    }
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == circleNameTextField {
      circleNumberOfStudents.becomeFirstResponder()
    }
    return true
  }
}
