//
//  SubscriptionViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SubscriptionViewController: UIViewController {
  @IBOutlet weak var subscriptionTypeLabel: UILabel!
  @IBOutlet weak var subscriptionExpirationLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    configure()
  }
  
  func configure() {
    subscriptionTypeLabel.text = "TESTE"
    subscriptionExpirationLabel.text = "31/12/2015"
  }
}
