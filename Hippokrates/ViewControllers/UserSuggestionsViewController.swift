//
//  UserSuggestionsViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class UserSuggestionsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var userSuggestionsTableViewDelegate: UserSuggestionsTableViewDelegate!
  @IBOutlet weak var headerLabel: UILabel!
  
  weak var circleViewController: CircleViewController?
  
  var loading = false
  var endReached = false
  var currentPage = 0
  
  var total = 0
  var acceptedIds = [Int]()
  var declinedIds = [Int]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(R.nib.userCell.instance, forCellReuseIdentifier: R.reuseIdentifier.userCell.identifier)
    tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
    getMembers()
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    if navigationController == nil {
      for row in 0...tableView.numberOfRowsInSection(0) {
        if let userCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0)) as? UserCell {
          userCell.delegate = nil
        }
      }
    }
  }
  
  func getMembers() {
    if loading || endReached {
      return
    }
    loading = true
    circleViewController!.circle!.getPendingRequests(currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.userSuggestionsTableViewDelegate.datasource.removeAll(keepCapacity: false)
        }
        self.userSuggestionsTableViewDelegate.datasource += boxed.unbox.0
        self.currentPage++
        self.loading = false
        self.endReached = !boxed.unbox.2
        if boxed.unbox.1 > 0 {
          self.total = boxed.unbox.1
          self.updateHeaderLabel()
        }
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
  
  func updateHeaderLabel() {
    headerLabel.text = String(total - acceptedIds.count - declinedIds.count) + " sugestões de usuários"
  }
  
  // MARK: Done
  @IBAction func saveTapped(sender: UIButton) {
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    if acceptedIds.count > 0 {
      circleViewController?.circle?.acceptMembers(acceptedIds) {
        switch $0 {
        case .Success:
//          if let circleViewController = self.circleViewController, let circle = circleViewController.circle {
//            circleViewController.configureWithCircle(circle)
//          }
          self.circleViewController?.circle?.numberOfMembers += self.acceptedIds.count
          self.circleViewController?.shouldReload = true
          self.circleViewController?.circle?.numberOfSuggestedMembers -= self.acceptedIds.count
          self.acceptedIds.removeAll(keepCapacity: true)
          if self.declinedIds.count == 0 {
            JHProgressHUD.sharedHUD.hide()
            self.navigationController?.popViewControllerAnimated(true)
          }
        case .Failure(let error):
          if self.declinedIds.count == 0 {
            JHProgressHUD.sharedHUD.hide()
          }
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
    if declinedIds.count > 0 {
      circleViewController?.circle?.declineMembers(declinedIds) {
        switch $0 {
        case .Success:
//          if let circleViewController = self.circleViewController, let circle = circleViewController.circle {
//            circleViewController.configureWithCircle(circle)
//          }
          self.circleViewController?.circle?.numberOfSuggestedMembers -= self.declinedIds.count
          self.declinedIds.removeAll(keepCapacity: false)
          self.circleViewController?.shouldReload = true
          if self.acceptedIds.count == 0 {
            JHProgressHUD.sharedHUD.hide()
            self.navigationController?.popViewControllerAnimated(true)
          }
        case .Failure(let error):
          if self.acceptedIds.count == 0 {
            JHProgressHUD.sharedHUD.hide()
          }
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
  }
}

extension UserSuggestionsViewController: UserCellDelegate {
  func acceptUserAtCell(cell: UserCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let user = userSuggestionsTableViewDelegate.datasource[indexPath.row]
      acceptedIds.append(user.id)
      userSuggestionsTableViewDelegate.datasource.removeAtIndex(indexPath.row)
      tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
      updateHeaderLabel()
    }
  }
  
  func declineUserAtCell(cell: UserCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let user = userSuggestionsTableViewDelegate.datasource[indexPath.row]
      declinedIds.append(user.id)
      userSuggestionsTableViewDelegate.datasource.removeAtIndex(indexPath.row)
      tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
      updateHeaderLabel()
    }
  }
  
  func optionsForUserAtCell(cell: UserCell) {
    
  }
}
