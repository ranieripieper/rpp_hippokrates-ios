//
//  SearchResultsViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchResultsTableViewDelegate: SearchResultsTableViewDelegate!
  
  var searchSpecialty: Specialty?
  var searchHashtag: String?
  var loading = false
  var endReached = false
  var currentPage = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    tableView.registerNib(R.nib.photoCell.instance, forCellReuseIdentifier: R.reuseIdentifier.photoCell.identifier)
    tableView.registerNib(R.nib.searchResultsHeader.instance, forHeaderFooterViewReuseIdentifier: "SearchResultsHeader")
    if IOS_8() {
      tableView.estimatedRowHeight = 400
    }
    getPhotos()
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    if navigationController == nil {
      for row in 0...tableView.numberOfRowsInSection(0) {
        if let photoCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0)) as? PhotoCell {
          photoCell.delegate = nil
        }
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let photoDetailViewController = segue.destinationViewController as? PhotoDetailViewController {
      if let photo = sender as? Photo {
        photoDetailViewController.photo = photo
      }
    }
  }
  
  // MARK: Photos
  func getPhotos() {
    if loading || endReached {
      return
    }
    loading = true
    if let specialty = searchSpecialty {
      getPhotosFromSpecialty(specialty)
    } else if let hashtag = searchHashtag {
      getPhotosFromHashtag(hashtag)
    }
  }
  
  func getPhotosFromSpecialty(specialty: Specialty) {
    Photo.search(specialty.id, page: currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        self.endReached = !boxed.unbox.2
        if self.currentPage == 0 {
          self.searchResultsTableViewDelegate.datasource.removeAll(keepCapacity: false)
        }
        self.searchResultsTableViewDelegate.datasource += boxed.unbox.0
        self.loading = false
        self.currentPage++
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        self.loading = false
      }
    }
  }
  
  func getPhotosFromHashtag(hashtag: String) {
    Photo.search(hashtag) {
      switch $0 {
      case .Success(let boxed):
        self.endReached = !boxed.unbox.1
        if self.currentPage == 0 {
          self.searchResultsTableViewDelegate.datasource.removeAll(keepCapacity: false)
        }
        self.searchResultsTableViewDelegate.datasource += boxed.unbox.0
        self.loading = false
        self.currentPage++
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        self.loading = false
      }
    }
  }
  
  // MARK: Selection
  func goToPhoto(photo: Photo) {
    if let photoDetailViewController = R.storyboard.main.photoDetailViewController {
      photoDetailViewController.photo = photo
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(photoDetailViewController, animated: true)
    }
  }
  
  func goToUser(user: User) {
    if let userProfileViewController = R.storyboard.main.userProfileViewController {
      userProfileViewController.user = user
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(userProfileViewController, animated: true)
    }
  }
  
  // MARK: Alert
  func presentEditDeletePhoto(photo: Photo) {
    alert([("EDITAR FOTO", {
      self.editPhoto(photo)
    }), ("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          photo.delete() {
            switch $0 {
            case .Success(let boxed):
              self.searchResultsTableViewDelegate.datasource = self.searchResultsTableViewDelegate.datasource.filter() {
                $0.id != photo.id
              }
              self.tableView.reloadData()
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
        }
      }
    })])
  }
  
  func presentDeletePhoto(photo: Photo) {
    alert([("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          if let circleId = photo.circleId {
            photo.adminDeleteFromCircle(circleId) {
              switch $0 {
              case .Success(let boxed):
                self.searchResultsTableViewDelegate.datasource = self.searchResultsTableViewDelegate.datasource.filter() {
                  $0.id != photo.id
                }
                self.tableView.reloadData()
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
          }
        }
      }
    })])
  }
  
  func editPhoto(photo: Photo) {
    if let editPictureViewController = R.storyboard.newPhoto.editPictureViewController {
      editPictureViewController.photo = photo
      let aNavigationController = UINavigationController(rootViewController: editPictureViewController)
      aNavigationController.navigationBar.barStyle = .Black
      aNavigationController.navigationBar.barTintColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1)
      aNavigationController.navigationBar.tintColor = UIColor.whiteColor()
      editPictureViewController.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: R.image.btn_camera_action_close, style: .Bordered, target: self, action: "cancelEditPhoto")]
      presentViewController(aNavigationController, animated: true, completion: nil)
    }
  }
  
  func cancelEditPhoto() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func presentReportPhoto(photo: Photo) {
    alert([("DENUNCIAR FOTO", {
      self.presentReportOptions(photo)
    })])
  }
  
  func presentReportOptions(photo: Photo) {
    alert([("FOTO IMPRÓPRIA", {
      photo.report(.Improper) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    }), ("IDENTIFIQUEI O PACIENTE", {
      photo.report(.Patient) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    })])
  }
  
  func reportCallback(success: Bool) {
    if success {
      banner("Obrigado. O autor da foto será notificado.")
    } else {
      banner("Você já denunciou esta foto uma vez.")
    }
  }
  
  // MARK: Alerts
  func alert(buttons: [ButtonType]) {
    let alertView = AlertView(buttons: buttons, cancelButtonTitle: nil)
    alertView.alertInViewController(self)
  }
  
  func alert(text: String, callback: Bool -> ()) {
    let alertView = AlertView(text: text, buttons: [("SIM", {
      callback(true)
    }), ("Não", {
      callback(false)
    })])
    alertView.alertInViewController(self)
  }
  
  func banner(text: String) {
    let banner = BannerView(text: text)
    banner.presentInViewController(self)
  }
}

extension SearchResultsViewController: PhotoCellDelegate {
  func likeAtCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = searchResultsTableViewDelegate.datasource[indexPath.row]
      photo.toggleLike() {
        switch $0 {
        case .Success(let boxed):
          cell.updateLikes(photo)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
  }
  
  func commentCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell), commentsViewController = R.storyboard.main.commentsViewController {
      let photo = searchResultsTableViewDelegate.datasource[indexPath.row]
      commentsViewController.photo = photo
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(commentsViewController, animated: true)
    }
  }
  
  func goToCircleAtCell(cell: PhotoCell) {
    if searchSpecialty == nil {
      if let indexPath = tableView.indexPathForCell(cell), circleViewController = R.storyboard.main.circleViewController {
        let photo = searchResultsTableViewDelegate.datasource[indexPath.row]
        if let circle = photo.circle where circle.type != "public" {
          circleViewController.circle = circle
        } else {
          circleViewController.specialty = photo.specialty
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
        navigationController?.pushViewController(circleViewController, animated: true)
      }
    }
  }
  
  func goToUserAtPhotoCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = searchResultsTableViewDelegate.datasource[indexPath.row]
      let user = photo.user!
      goToUser(user)
    }
  }
  
  func optionsForCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = searchResultsTableViewDelegate.datasource[indexPath.row]
      if photo.userId! == User.currentUser.id {
        presentEditDeletePhoto(photo)
      } else if photo.circle?.adminId == User.currentUser.id {
        presentDeletePhoto(photo)
      } else {
        presentReportPhoto(photo)
      }
    }
  }
}
