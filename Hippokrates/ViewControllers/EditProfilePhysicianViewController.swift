//
//  EditProfilePhysicianViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class EditProfilePhysicianViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var avatarLabel: UILabel!
  @IBOutlet weak var passwordTextField: TextField!
  @IBOutlet weak var occupationTextField: TextField!
  @IBOutlet weak var occupationPrivateButton: UIButton!
  @IBOutlet weak var specialtyTextField: TextField!
  @IBOutlet weak var specialtyPrivateButton: UIButton!
  @IBOutlet weak var formationSchoolTextField: TextField!
  @IBOutlet weak var formationSchoolPrivateButton: UIButton!
  @IBOutlet weak var formationSchoolNumberTextField: TextField!
  @IBOutlet weak var formationSchoolNumberPrivateButton: UIButton!
  @IBOutlet weak var professorSwitch: UISwitch!
  @IBOutlet weak var saveButtonWidthConstraint: NSLayoutConstraint!
  
  var newAvatar: UIImage?
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var selectedSpecialty: Specialty?
  var selectedSchool: School?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    avatarImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    saveButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    prefillData()
  }
  
  func prefillData() {
    let user = User.currentUser
    avatarImageView.setImageWithURL(NSURL(string: user.avatarURL ?? "")!)
    if let occupation = user.occupation {
      occupationTextField.text = occupation
    }
    if let specialty = user.specialty {
      specialtyTextField.text = specialty.name
    }
    if let formation = user.formation {
      formationSchoolTextField.text = formation.school.name
      formationSchoolNumberTextField.text = String(formation.year)
    }
    professorSwitch.on = user.type == .Professor
    for key in user.privacy {
      switch key {
      case "occupation":
        occupationPrivateButton.selected = true
      case "medical_specialty":
        specialtyPrivateButton.selected = true
      case "education_data.university_institution_name":
        formationSchoolPrivateButton.selected = true
      case "education_data.classroom_number":
        formationSchoolNumberPrivateButton.selected = true
      default:
        break
      }
    }
  }
  
  // MARK: Avatar
  @IBAction func avatarTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    self.imagePicker = ImagePicker(viewController: self) { image in
      if let image = image {
        self.newAvatar = image
        self.avatarImageView.image = image
      }
      self.imagePicker = nil
    }
    self.imagePicker?.presentImagePicker()
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Private
  @IBAction func togglePrivate(sender: UIButton) {
    presentPrivateAlert(sender)
  }
  
  func presentPrivateAlert(sender: UIButton) {
    let alertView = AlertView(buttons: [("INFORMAÇÃO PÚBLICA", {
      sender.selected = false
      self.changePrivacyForButton(sender, changeToPrivate: false)
      self.alert("Outros usuários terão acesso a esta informação no teu perfil.", callback: nil)
    }), ("INFORMAÇÃO PRIVADA", {
      sender.selected = true
      self.changePrivacyForButton(sender, changeToPrivate: true)
      self.alert("Outros usuários não terão acesso a esta informação no teu perfil.", callback: nil)
    })], cancelButtonTitle: nil)
    alertView.alertInViewController(self)
  }
  
  func changePrivacyForButton(button: UIButton, changeToPrivate: Bool) {
    let key: String
    switch button {
    case occupationPrivateButton:
      key = "occupation"
    case specialtyPrivateButton:
      key = "medical_specialty"
    case formationSchoolPrivateButton:
      key = "education_data.university_institution_name"
    case formationSchoolNumberPrivateButton:
      key = "education_data.classroom_number"
    default:
      key = ""
    }
    if count(key) > 0 {
      User.currentUser.changePrivacy(key, value: changeToPrivate) {
        switch $0 {
        case .Success(let boxed):
          break
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
  }
  
  func alert(text: String, callback: (() -> ())?) {
    let alertView = AlertView(text: text, callback: callback)
    alertView.alertInViewController(self)
  }
  
  // MARK: Professor
  @IBAction func switchValueChanged(sender: UISwitch) {
    if !sender.on {
      presentBanner("Você não poderá mais convidar alunos para participar do Hippokrates.")
    }
  }
  
  func presentBanner(text: String) {
    let bannerView = BannerView(text: text)
    bannerView.presentInViewController(self)
  }
  
  // MARK: Save
  @IBAction func saveTapped(sender: UIButton) {
    let user = User.currentUser
    let newOccupation = occupationTextField.text != user.occupation
    let newSpecialty = specialtyTextField.text != user.specialty?.name
    let newFormationName = formationSchoolTextField.text != user.formation?.school.name
    let newFormationYear = formationSchoolNumberTextField.text != String(user.formationYear ?? 0)
    
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    user.updateProfile(newOccupation ? occupationTextField.text : nil, specialtyId: newSpecialty ? specialtyTextField.text.toInt() : nil, formationSchoolName: newFormationName ? formationSchoolTextField.text : nil, formationSchoolYear: newFormationYear ? formationSchoolNumberTextField.text.toInt() : nil, professor: (professorSwitch.on != (user.type == .Professor)) ? professorSwitch.on : nil, avatar: newAvatar, password: passwordTextField.text) {
      switch $0 {
      case .Success(let boxed):
        BannerView.present("Perfil salvo com sucesso!", inViewController: self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
          Int64(1 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            self.navigationController?.popViewControllerAnimated(true)
        }
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
      JHProgressHUD.sharedHUD.hide()
    }
  }
}

extension EditProfilePhysicianViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if textField == passwordTextField {
      if let newPasswordViewController = R.storyboard.login.newPasswordViewController {
        navigationController?.pushViewController(newPasswordViewController, animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      }
    } else if let aNavigationController = R.storyboard.login.autocompleteNavigationController {
      if let autocompleteViewController = aNavigationController.viewControllers.first as? AutocompleteViewController {
        if textField == specialtyTextField {
          let specialties = Specialty.all() {
            switch $0 {
            case .Success(let boxed):
              autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
          autocompleteViewController.datasource = specialties.map() { $0 as NamedObject }
          autocompleteViewController.callback = {
            self.selectedSpecialty = $0 as? Specialty
            textField.text = self.selectedSpecialty?.name
            self.dismissViewControllerAnimated(true, completion: nil)
          }
        } else if textField == formationSchoolTextField {
          let schools = School.all() {
            switch $0 {
            case .Success(let boxed):
              autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
          autocompleteViewController.datasource = schools.map() { $0 as NamedObject }
          autocompleteViewController.callback = {
            self.selectedSchool = $0 as? School
            textField.text = self.selectedSchool?.name
            self.dismissViewControllerAnimated(true, completion: nil)
          }
        }
        presentViewController(aNavigationController, animated: true, completion: nil)
      }
    }
    return false
  }
}
