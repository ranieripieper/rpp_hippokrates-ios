//
//  CircleViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CircleViewController: UIViewController {
  @IBOutlet weak var headerView: UIView!
  @IBOutlet weak var adminHeaderView: UIView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var circleImageView: UIImageView!
  @IBOutlet weak var circleNameLabel: UILabel!
  @IBOutlet weak var postsButton: UIButton!
  @IBOutlet weak var membersButton: UIButton!
  @IBOutlet weak var membersSuggestionButton: UIButton!
  @IBOutlet weak var inviteCodeLabel: UILabel!
  @IBOutlet weak var adminHeaderViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var circleTableViewDelegate: CircleTableViewDelegate!
  
  var circle: Circle?
  var circleId: Int?
  var specialty: Specialty?
  var photos = [Photo]()
  var members = [User]()
  var linkedUsers = [User]()
  
  var photosLoading = false
  var membersLoading = false
  var photosEndReached = false
  var membersEndReached = false
  var currentPhotosPage = 0
  var currentMembersPage = 0
  var noPublicPostsLabel: UILabel?
  
  var shouldReload = false
  
  var imagePicker: ImagePicker?
  
  var alerted = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(R.nib.photoCell.instance, forCellReuseIdentifier: R.reuseIdentifier.photoCell.identifier)
    tableView.registerNib(R.nib.userCell.instance, forCellReuseIdentifier: R.reuseIdentifier.userCell.identifier)
    if IOS_8() {
      tableView.estimatedRowHeight = 410
    }
    circleImageView.layer.borderColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1).CGColor
    JHProgressHUD.sharedHUD.showInView(navigationController!.view)
    if let circle = circle {
      configureWithCircle(circle)
      getPhotos()
      circle.info() { _ in
        self.configureWithCircle(circle)
        JHProgressHUD.sharedHUD.hide()
      }
    } else if let specialty = specialty {
      configureWithSpecialty(specialty)
    } else if let circleId = circleId {
      Circle.getCircleWithId(circleId) {
        switch $0 {
        case .Success(let boxed):
          self.circle = boxed.unbox
          self.configureWithCircle(self.circle!)
          self.photosEndReached = false
          self.photosLoading = false
          self.currentPhotosPage = 0
          self.getPhotos()
          self.tableView.reloadData()
        case .Failure(let error):
          if error.code == 401 || error.code == 404 {
            self.navigationController?.popViewControllerAnimated(true)
          } else {
            BannerView.present(error.description, inViewController: self)
          }
        }
        JHProgressHUD.sharedHUD.hide()
      }
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let circle = circle {
      configureWithCircle(circle)
      if shouldReload {
        self.membersEndReached = false
        self.membersLoading = false
        self.currentMembersPage = 0
        getMembers()
        shouldReload = false
      }
    }
    if let indexPath = tableView.indexPathForSelectedRow() {
      tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    if navigationController == nil {
      switch circleTableViewDelegate.circleShowMode {
      case .Photos:
        nullifyPhotoCells()
      case .Members:
        nullifyUserCells()
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let userSuggestionsViewController = segue.destinationViewController as? UserSuggestionsViewController {
      userSuggestionsViewController.circleViewController = self
    } else if let createCircleMembersViewController = segue.destinationViewController as? CreateCircleMembersViewController {
      createCircleMembersViewController.circle = circle
      createCircleMembersViewController.circleViewController = self
    }
  }
  
  // MARK: Circle
  func configureWithCircle(circle: Circle) {
    if circle.adminId == User.currentUser.id {
      if let inviteCode = circle.inviteCode {
        adminHeaderViewHeightConstraint.constant = 40
        membersSuggestionButton.hidden = true
        inviteCodeLabel.hidden = false
        inviteCodeLabel.text = "Código de acesso: \(inviteCode)"
      } else {
        if circle.numberOfSuggestedMembers > 0 {
          adminHeaderViewHeightConstraint.constant = 40
          membersSuggestionButton.setTitle("+ \(circle.numberOfSuggestedMembers) sugestões de usuários", forState: .Normal)
          membersSuggestionButton.hidden = false
        } else {
          membersSuggestionButton.hidden = true
          adminHeaderViewHeightConstraint.constant = 0
        }
        inviteCodeLabel.hidden = true
      }
      addAdminButton()
    } else {
      adminHeaderViewHeightConstraint.constant = 0
      addOptionsButton()
    }
    view.layoutIfNeeded()
    tableView.scrollIndicatorInsets = tableView.contentInset
    circleImageView.setImageWithURL(NSURL(string: circle.pictureURL ?? "")!, placeholderImage: R.image.img_placeholder_circle)
    circleNameLabel.text = circle.name
    postsButton.setTitle("\(circle.numberOfPhotos) posts", forState: .Normal)
    membersButton.setTitle("\(circle.numberOfMembers) participantes", forState: .Normal)
  }
  
  // MARK: Specialty
  func configureWithSpecialty(specialty: Specialty) {
    adminHeaderViewHeightConstraint.constant = 0
    view.layoutIfNeeded()
    tableView.scrollIndicatorInsets = tableView.contentInset
    circleImageView.image = R.image.img_placeholder_circle
    circleNameLabel.text = specialty.name
    membersButton.hidden = true
  }
  
  func getPhotos() {
    if photosLoading || photosEndReached {
      return
    }
    photosLoading = true
    let completion: Result<([Photo], [User], Bool)> -> () = { result in
      switch result {
      case .Success(let boxed):
        self.photosEndReached = !boxed.unbox.2
        if self.currentPhotosPage == 0 {
          self.photos.removeAll(keepCapacity: false)
          self.linkedUsers = []
          if boxed.unbox.0.count == 0 {
            self.showNoPublicPhotosAlert()
          }
        }
        self.photos += boxed.unbox.0
        self.linkedUsers += boxed.unbox.1
        self.currentPhotosPage++
        self.photosLoading = false
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        self.photosLoading = false
      }
      JHProgressHUD.sharedHUD.hide()
    }
    if let circle = circle {
      circle.posts(currentPhotosPage + 1, completion: completion)
    } else if let specialty = specialty {
      Photo.search(specialty.id, page: currentPhotosPage + 1) {
        switch $0 {
        case .Success(let boxed):
          if boxed.unbox.3 > 0 {
            self.postsButton.setTitle("\(boxed.unbox.3) posts", forState: .Normal)
          }
          let tuple = (boxed.unbox.0, boxed.unbox.1, boxed.unbox.2)
          completion(Result(tuple))
        case .Failure(let error):
          completion(Result(error))
        }
        JHProgressHUD.sharedHUD.hide()
      }
    }
  }
  
  func getMembers() {
    if membersLoading || membersEndReached {
      return
    }
    membersLoading = true
    circle!.members(currentMembersPage + 1) {
      switch $0 {
      case .Success(let boxed):
        self.membersEndReached = !boxed.unbox.1
        if self.currentMembersPage == 0 {
          self.members = []
        }
        self.members += boxed.unbox.0
        self.currentMembersPage++
        self.membersLoading = false
        self.tableView.reloadData()
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
        self.membersLoading = false
      }
    }
  }
  
  @IBAction func photosTapped(sender: UIButton) {
    nullifyUserCells()
    if photos.count == 0 {
      showNoPublicPhotosAlert()
    }
    if IOS_8() {
      tableView.estimatedRowHeight = 410
    }
    circleTableViewDelegate.circleShowMode = .Photos
    tableView.reloadData()
    tableView.contentOffset = CGPoint(x: 0, y: -64 - 100)
  }
  
  @IBAction func membersTapped(sender: UIButton) {
    nullifyPhotoCells()
    removeNoPublicPhotosAlert()
    if IOS_8() {
      tableView.estimatedRowHeight = 50
    }
    circleTableViewDelegate.circleShowMode = .Members
    if members.count == 0 {
      getMembers()
    }
    tableView.reloadData()
    tableView.contentOffset = CGPoint(x: 0, y: -64 - 100)
  }
  
  func nullifyPhotoCells() {
    for row in 0...photos.count {
      if let photoCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0)) as? PhotoCell {
        photoCell.delegate = nil
      }
    }
  }
  
  func nullifyUserCells() {
    for row in 0...members.count {
      if let userCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0)) as? UserCell {
        userCell.delegate = nil
      }
    }
  }
  
  func addOptionsButton() {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
    view.backgroundColor = UIColor.clearColor()
    let button = UIButton.buttonWithType(.Custom) as! UIButton
    button.frame = view.bounds
    button.setImage(R.image.icn_details_options, forState: .Normal)
    button.contentHorizontalAlignment = .Right
    button.addTarget(self, action: "presentOptions", forControlEvents: .TouchUpInside)
    view.addSubview(button)
    let label = UILabel(frame: view.bounds)
    label.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
    label.textColor = UIColor.whiteColor()
    label.text = "Mais opções"
    view.addSubview(label)
    navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: view)]
  }
  
  func addAdminButton() {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 130, height: 44))
    view.backgroundColor = UIColor.clearColor()
    let button = UIButton.buttonWithType(.Custom) as! UIButton
    button.frame = view.bounds
    button.setImage(R.image.icn_details_opcoes_admin, forState: .Normal)
    button.contentHorizontalAlignment = .Right
    button.addTarget(self, action: "presentAdminOptions", forControlEvents: .TouchUpInside)
    view.addSubview(button)
    let label = UILabel(frame: view.bounds)
    label.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
    label.textColor = UIColor.whiteColor()
    label.text = "Opções do Admin"
    view.addSubview(label)
    navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: view)]
  }
  
  func showNoPublicPhotosAlert() {
    if noPublicPostsLabel != nil {
      return
    }
    tableView.hidden = true
    noPublicPostsLabel = UILabel(frame: view.bounds)
    noPublicPostsLabel?.textAlignment = .Center
    noPublicPostsLabel?.textColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
    noPublicPostsLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 20)
    noPublicPostsLabel?.text = "Nenhum post público."
    view.addSubview(noPublicPostsLabel!)
  }
  
  func removeNoPublicPhotosAlert() {
    tableView.hidden = false
    noPublicPostsLabel?.removeFromSuperview()
    noPublicPostsLabel = nil
  }
  
  // MARK: Detail
  func goToPhotoDetail(photo: Photo) {
    if let photoDetailViewController = R.storyboard.main.photoDetailViewController {
      photoDetailViewController.photo = photo
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(photoDetailViewController, animated: true)
    }
  }
  
  func goToUser(user: User) {
    if let userProfileViewController = R.storyboard.main.userProfileViewController {
      userProfileViewController.user = user
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(userProfileViewController, animated: true)
    }
  }
  
  // MARK: Suggestions
  @IBAction func suggestedUsersTapped(sender: UIButton) {
    performSegueWithIdentifier(R.segue.segueSuggestions, sender: nil)
  }
  
  // MARK: Circle Image
  func editImage() {
    if imagePicker != nil {
      return
    }
    self.imagePicker = ImagePicker(viewController: self) { image in
      if let image = image {
        self.circleImageView.image = image
        JHProgressHUD.sharedHUD.showInView(self.navigationController!.view)
        self.circle?.updateImage(image) {
          switch $0 {
          case .Success:
            self.circleImageView.image = image
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
          JHProgressHUD.sharedHUD.hide()
        }
      }
      self.imagePicker = nil
    }
    self.imagePicker?.presentImagePicker()
  }
  
  // MARK: Options
  func presentOptions() {
    if alerted {
      return
    }
    alerted = true
    if let type = circle!.type where type == "students" {
      alert([("DEIXAR CÍRCULO", {
        self.alerted = false
        self.leaveCircle()
      })])
    } else {
      alert([("DEIXAR CÍRCULO", {
        self.alerted = false
        self.leaveCircle()
      }), ("INDICAR PARTICIPANTE", {
        self.alerted = false
        self.performSegueWithIdentifier(R.segue.segueInvite, sender: nil)
      })])
    }
  }
  
  func leaveCircle() {
    if alerted {
      return
    }
    alerted = true
    alert("Você tem certeza que deseja deixar o círculo \"\(circle!.name)\"? Você não terá mais acesso ao conteúdo privado.") { yes in
      self.alerted = false
      if yes {
        self.circle!.leave() {
          switch $0 {
          case .Success:
            User.currentUser.circles = User.currentUser.circles.filter() {
              $0.id != self.circle!.id
            }
            self.navigationController?.popViewControllerAnimated(true)
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
        }
      }
    }
  }
  
  func presentAdminOptions() {
    if alerted {
      return
    }
    alerted = true
    if let type = circle!.type where type == "students" {
      alert([("EDITAR IMAGEM DO CÍRCULO", {
        self.alerted = false
        self.editImage()
      }), ("DELETAR CÍRCULO", {
        self.alerted = false
        self.deleteCircle()
      }), ("DEIXAR CÍRCULO", {
        self.alerted = false
        self.leaveCircle()
      })])
    } else {
      alert([("EDITAR IMAGEM DO CÍRCULO", {
        self.alerted = false
        self.editImage()
      }), ("ADICIONAR AMIGO AO CÍRCULO", {
        self.alerted = false
        self.performSegueWithIdentifier(R.segue.segueInvite, sender: nil)
      }), ("DELETAR CÍRCULO", {
        self.alerted = false
        self.deleteCircle()
      }), ("DEIXAR CÍRCULO", {
        self.alerted = false
        self.leaveCircle()
      })])
    }
  }
  
  func deleteCircle() {
    if alerted {
      return
    }
    alerted = true
    alert("Você tem certeza que deseja deletar o círculo \"\(circle!.name)\"?") { yes in
      self.alerted = false
      if yes {
        self.circle!.delete() {
          switch $0 {
          case .Success:
            User.currentUser.circles = User.currentUser.circles.filter() {
              $0.id != self.circle!.id
            }
            self.navigationController?.popViewControllerAnimated(true)
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
        }
      }
    }
  }
  
  func presentRemoveUser(user: User) {
    if alerted {
      return
    }
    alerted = true
    alert([("EXCLUIR USUÁRIO", {
      self.alert("Você tem certeza que deseja excluir \(user.name) do círculo \(self.circle!.name)?") { yes in
        self.alerted = false
        if yes {
          self.circle!.removeUser(user.id) {
            switch $0 {
            case .Success:
              self.members = self.members.filter() {
                $0.id != user.id
              }
              self.membersButton.setTitle("\(self.circle!.numberOfMembers) participantes", forState: .Normal)
              self.tableView.reloadData()
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
        }
      }
    })])
  }
  
  func presentTurnAdminUser(user: User) {
    if alerted {
      return
    }
    alerted = true
    alert([("TORNAR ADMIN DO CÍRCULO", {
      self.alert("Você tem certeza que deseja tornar \(user.name) admin do círculo \(self.circle!.name)?") { yes in
        self.alerted = false
        self.circle!.turnCircleAdmin(user.id) {
          switch $0 {
          case .Success:
            self.configureWithCircle(self.circle!)
          case .Failure(let error):
            BannerView.present(error.description, inViewController: self)
          }
        }
      }
    })])
  }
  
  func optionsForSelectedUser(user: User) {
    if alerted {
      return
    }
    if circle!.adminId == User.currentUser.id && user.id != User.currentUser.id {
      alerted = true
      alert([("EXCLUIR USUÁRIO", {
        self.alerted = false
        self.alert("Você tem certeza que deseja excluir \(user.name) do círculo \(self.circle!.name)?") { yes in
          if yes {
            self.circle!.removeUser(user.id) {
              switch $0 {
              case .Success:
                self.members = self.members.filter() {
                  $0.id != user.id
                }
                self.membersButton.setTitle("\(self.circle!.numberOfMembers) participantes", forState: .Normal)
                self.tableView.reloadData()
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
          }
        }
      }), ("TORNAR ADMIN DO CÍRCULO", {
        self.alerted = false
        self.alert("Você tem certeza que deseja tornar \(user.name) admin do círculo \(self.circle!.name)?") { yes in
          self.circle!.turnCircleAdmin(user.id) {
            switch $0 {
            case .Success:
              self.configureWithCircle(self.circle!)
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
        }
      })])
    }
  }
  
  func presentEditDeletePhoto(photo: Photo) {
    if alerted {
      return
    }
    alerted = true
    alert([("EDITAR FOTO", {
      self.alerted = false
      self.editPhoto(photo)
    }), ("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        self.alerted = false
        if $0 {
          photo.delete() {
            switch $0 {
            case .Success(let boxed):
              self.photos = self.photos.filter() {
                $0.id != photo.id
              }
              self.tableView.reloadData()
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
        }
      }
    })])
  }
  
  func presentDeletePhoto(photo: Photo) {
    alert([("DELETAR FOTO", {
      self.alert("Você tem certeza que deseja deletar esta foto? Ela será apagada e não poderá ser recuperada depois.") {
        if $0 {
          if let circleId = photo.circleId {
            photo.adminDeleteFromCircle(circleId) {
              switch $0 {
              case .Success(let boxed):
                self.photos = self.photos.filter() {
                  $0.id != photo.id
                }
                self.tableView.reloadData()
              case .Failure(let error):
                BannerView.present(error.description, inViewController: self)
              }
            }
          }
        }
      }
    })])
  }
  
  func editPhoto(photo: Photo) {
    if let editPictureViewController = R.storyboard.newPhoto.editPictureViewController {
      editPictureViewController.photo = photo
      let aNavigationController = UINavigationController(rootViewController: editPictureViewController)
      aNavigationController.navigationBar.barStyle = .Black
      aNavigationController.navigationBar.barTintColor = UIColor(red: 117 / 255, green: 188 / 255, blue: 193 / 255, alpha: 1)
      aNavigationController.navigationBar.tintColor = UIColor.whiteColor()
      editPictureViewController.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: R.image.btn_camera_action_close, style: .Bordered, target: self, action: "cancelEditPhoto")]
      presentViewController(aNavigationController, animated: true, completion: nil)
    }
  }
  
  func cancelEditPhoto() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func presentReportPhoto(photo: Photo) {
    if alerted {
      return
    }
    alerted = true
    alert([("DENUNCIAR FOTO", {
      self.alerted = false
      self.presentReportOptions(photo)
    })])
  }
  
  func presentReportOptions(photo: Photo) {
    if alerted {
      return
    }
    alerted = true
    alert([("FOTO IMPRÓPRIA", {
      self.alerted = false
      photo.report(.Improper) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    }), ("IDENTIFIQUEI O PACIENTE", {
      self.alerted = false
      photo.report(.Patient) {
        switch $0 {
        case .Success:
          self.reportCallback(true)
        case .Failure(let error):
          self.reportCallback(false)
        }
      }
    })])
  }
  
  func reportCallback(success: Bool) {
    if success {
      banner("Obrigado. O autor da foto será notificado.")
    } else {
      banner("Você já denunciou esta foto uma vez.")
    }
  }
  
  // MARK: Alerts
  func alert(buttons: [ButtonType]) {
    let alertView = AlertView(buttons: buttons, cancelButtonTitle: nil)
    alertView.alertInViewController(self)
    alertView.callback = {
      self.alerted = false
    }
  }
  
  func alert(text: String, callback: Bool -> ()) {
    let alertView = AlertView(text: text, buttons: [("SIM", {
      callback(true)
    }), ("Não", {
      callback(false)
    })])
    alertView.alertInViewController(self)
  }
  
  func banner(text: String) {
    let banner = BannerView(text: text)
    banner.presentInViewController(self)
  }
}

extension CircleViewController: PhotoCellDelegate {
  func likeAtCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = photos[indexPath.row]
      photo.toggleLike() {
        switch $0 {
        case .Success(let boxed):
          cell.updateLikes(photo)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
      }
    }
  }
  
  func commentCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell), commentsViewController = R.storyboard.main.commentsViewController {
      let photo = photos[indexPath.row]
      commentsViewController.photo = photo
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      navigationController?.pushViewController(commentsViewController, animated: true)
    }
  }
  
  func goToCircleAtCell(cell: PhotoCell) {
    
  }
  
  func goToUserAtPhotoCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = photos[indexPath.row]
      let user = photo.user!
      goToUser(user)
    }
  }
  
  func optionsForCell(cell: PhotoCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let photo = photos[indexPath.row]
      if photo.userId! == User.currentUser.id {
        presentEditDeletePhoto(photo)
      } else if photo.circle?.adminId == User.currentUser.id {
        presentDeletePhoto(photo)
      } else {
        presentReportPhoto(photo)
      }
    }
  }
}

extension CircleViewController: UserCellDelegate {
  func optionsForUserAtCell(cell: UserCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      optionsForSelectedUser(members[indexPath.row])
    }
  }
  
  func acceptUserAtCell(cell: UserCell) {
    
  }
  
  func declineUserAtCell(cell: UserCell) {
    
  }
}
