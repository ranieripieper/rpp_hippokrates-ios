//
//  CreateCircleMembersViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CreateCircleMembersViewController: UIViewController {
  @IBOutlet weak var textField: TextField!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var doneButton: UIButton?
  @IBOutlet weak var searchUserCollectionDelegate: SearchUserCollectionDelegate!
  
  var circle: Circle!
  weak var circleViewController: CircleViewController?
  
  var loading = false
  var endReached = false
  var currentPage = 0
  
  var currentDataTask: NSURLSessionDataTask?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(R.nib.userCell.instance, forCellReuseIdentifier: R.reuseIdentifier.userCell.identifier)
    if circleViewController != nil {
      removeDoneButton()
    } else {
      removeBackButton()
    }
    textField.becomeFirstResponder()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    searchUserCollectionDelegate.delegate = self
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    searchUserCollectionDelegate.delegate = nil
  }
  
  func removeBackButton() {
    navigationItem.hidesBackButton = true
  }
  
  func removeDoneButton() {
    doneButton?.removeFromSuperview()
  }
  
  // MARK: Get users
  func getUsers(text: String) {
    if count(text) == 0 {
      searchUserCollectionDelegate.datasource.removeAll(keepCapacity: false)
      tableView.reloadData()
      return
    }
    if loading || endReached {
      return
    }
    loading = true
    currentDataTask = User.searchUsers(text, page: currentPage + 1, onlyPhysicians: true) {
      switch $0 {
      case .Success(let boxed):
        if self.currentPage == 0 {
          self.searchUserCollectionDelegate.datasource.removeAll(keepCapacity: false)
        }
        self.searchUserCollectionDelegate.datasource += boxed.unbox.0
        self.currentPage++
        self.loading = false
        self.endReached = !boxed.unbox.1
        self.tableView.reloadData()
      case .Failure(let error):
        if error.description != "cancelled" {
          BannerView.present(error.description, inViewController: self)
        }
        self.loading = false
      }
    }
  }
  
  // MARK: Invite
  @IBAction func doneTapped(sender: UIButton) {
    if let mainNavigationViewController = navigationController as? MainNavigationController {
      mainNavigationViewController.goToCircle(circle)
      mainNavigationViewController.viewControllers = mainNavigationViewController.viewControllers.filter() {
        $0 is TimelineViewController || $0 is SearchViewController || $0 is CircleViewController
      }
    }
  }
  
  // MARK: Banner
  func banner(text: String) {
    let banner = BannerView(text: text)
    banner.presentInViewController(self)
  }
}

extension CreateCircleMembersViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    currentPage = 0
    loading = false
    endReached = false
    if let currentDataTask = currentDataTask {
      currentDataTask.cancel()
      self.currentDataTask = nil
    }
    getUsers((textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string))
    return true
  }
}

extension CreateCircleMembersViewController: SearchUserCollectionDelegateDelegate {
  func didSelectUser(user: User) {
    circle.inviteUser(user.id) {
      switch $0 {
      case .Success:
        if self.circle.adminId == User.currentUser.id {
          self.banner("Usuário adicionado com sucesso.")
          self.circle.numberOfMembers++
          self.circleViewController?.shouldReload = true
        } else {
          self.banner("Usuário indicado com sucesso.")
        }
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self)
      }
    }
  }
}
