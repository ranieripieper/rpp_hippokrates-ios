//
//  NoInvitationViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NoInvitationViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var firstNameTextField: TextField!
  @IBOutlet weak var lastNameTextField: TextField!
  @IBOutlet weak var emailTextField: TextField!
  @IBOutlet weak var crmTextField: TextField!
  @IBOutlet weak var crmStateTextField: TextField!
  @IBOutlet weak var schoolTextField: TextField!
  @IBOutlet weak var femaleButton: UIButton!
  @IBOutlet weak var maleButton: UIButton!
  @IBOutlet weak var physicianButton: UIButton!
  @IBOutlet weak var studentButton: UIButton!
  @IBOutlet weak var crmTextFieldHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var crmFieldsVerticalSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var crmStateTextFieldHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var schoolTextFieldHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var registerButtonTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var registerButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  
  var selectedSchool: School?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    registerButtonTopSpaceConstraint.constant = UIScreen.mainScreen().bounds.height - 64 - 70
    registerButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textFieldDelegate.returnCallback = textFieldDidReturn
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    textFieldDelegate.returnCallback = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let destinationNavigationController = segue.destinationViewController as? UINavigationController {
      if let autocompleteViewController = destinationNavigationController.viewControllers.first as? AutocompleteViewController {
        if let textField = sender as? TextField {
          let schools = School.all() {
            switch $0 {
            case .Success(let boxed):
              autocompleteViewController.datasource = boxed.unbox.map() { $0 as NamedObject }
            case .Failure(let error):
              BannerView.present(error.description, inViewController: self)
            }
          }
          autocompleteViewController.datasource = schools.map() { $0 as NamedObject }
          autocompleteViewController.callback = {
            if let school = $0 as? School {
              self.selectedSchool = school
              textField.text = school.name
            }
            self.dismissViewControllerAnimated(true, completion: nil)
          }
        }
      }
    }
  }
  
  // MARK: Register
  @IBAction func registerTapped(sender: UIButton?) {
    if validateFields() {
      JHProgressHUD.sharedHUD.showInView(navigationController!.view)
      NetworkManager.sharedInstance.askForInvite(firstNameTextField.text, lastName: lastNameTextField.text, email: emailTextField.text, gender: femaleButton.selected ? "female" : "male", physician: physicianButton.selected, crm: crmTextField.text, crmState: State(string: crmStateTextField.text), school: School(id: 0, name: schoolTextField.text, branchId: 0, branchName: "")) { result in
        switch result {
        case .Success(let boxedBool):
          self.performSegueWithIdentifier(R.segue.segueSent, sender: nil)
        case .Failure(let error):
          self.invalidFields()
        }
        JHProgressHUD.sharedHUD.hide()
      }
    } else {
      invalidFields()
    }
  }
  
  func validateFields() -> Bool {
    return count(firstNameTextField.text) > 0 && count(emailTextField.text) > 0 && ((physicianButton.selected && count(crmTextField.text) > 0 && count(crmStateTextField.text) > 0) || (studentButton.selected && count(schoolTextField.text) > 0))
  }
  
  func invalidFields() {
    let bannerView = BannerView(text: "Por favor, preencha todos os campos abaixo e informe se você é médico ou estudante de medicina.")
    bannerView.presentInViewController(self)
  }
  
  func textFieldDidReturn(textField: UITextField) {
    if textField == crmStateTextField || textField == schoolTextField {
      registerTapped(nil)
    }
  }
  
  // MARK: Gender
  @IBAction func genderTapped(sender: UIButton) {
    femaleButton.selected = sender == femaleButton
    maleButton.selected = sender == maleButton
  }
  
  // MARK: Physician or Student
  @IBAction func physicianOrStudentTapped(sender: UIButton) {
    physicianButton.selected = sender == physicianButton
    studentButton.selected = sender == studentButton
    if physicianButton.selected {
      addCRMFields()
    } else {
      removeCRMFields()
    }
    if studentButton.selected {
      addSchoolField()
    } else {
      removeSchoolField()
    }
    UIView.animateWithDuration(0.3,
      animations: {
        self.view.layoutIfNeeded()
      },
      completion: { finished in
        self.crmTextField.setNeedsDisplay()
        self.crmStateTextField.setNeedsDisplay()
        self.schoolTextField.setNeedsDisplay()
    })
  }
  
  func addCRMFields() {
    crmTextFieldHeightConstraint.constant = 50
    crmFieldsVerticalSpaceConstraint.constant = 30
    crmStateTextFieldHeightConstraint.constant = 50
    crmTextField.hidden = false
    crmStateTextField.hidden = false
  }
  
  func removeCRMFields() {
    crmTextFieldHeightConstraint.constant = 0
    crmFieldsVerticalSpaceConstraint.constant = 0
    crmStateTextFieldHeightConstraint.constant = 0
    crmTextField.hidden = true
    crmStateTextField.hidden = true
  }
  
  func addSchoolField() {
    schoolTextFieldHeightConstraint.constant = 50
    schoolTextField.hidden = false
  }
  
  func removeSchoolField() {
    schoolTextFieldHeightConstraint.constant = 0
    schoolTextField.hidden = true
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
}

extension NoInvitationViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if textField == schoolTextField {
      performSegueWithIdentifier(R.segue.segueAutocomplete, sender: textField)
    }
    return false
  }
}
