//
//  MainNavigationController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
  @IBOutlet weak var menuAnimator: MenuAnimator!
  @IBOutlet weak var photoDetailAnimator: PhotoDetailAnimator!
  
  weak var menuViewController: MenuViewController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(menuAnimator.menuPanGesture)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let menuViewController = segue.destinationViewController as? MenuViewController {
      menuViewController.transitioningDelegate = menuAnimator
      menuViewController.mainNavigationController = self
      self.menuViewController = menuViewController
    }
  }
  
  // MARK: View Containment
  func showTimeline() {
    viewControllers = [R.storyboard.main.timelineViewController!]
  }
  
  func showSearch() {
    viewControllers = [R.storyboard.search.initialViewController!]
  }
  
  func showNotifications() {
    if let notificationsViewController = R.storyboard.main.notificationsViewController {
      pushViewController(notificationsViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func showHistory() {
    if let userProfileViewController = R.storyboard.main.userProfileViewController {
      userProfileViewController.user = User.currentUser
      pushViewController(userProfileViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func showInvite() {
    if let inviteViewController = R.storyboard.login.inviteViewController {
      pushViewController(inviteViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func showSubscription() {
    if let subscriptionsViewController = R.storyboard.main.subscriptionViewController {
      pushViewController(subscriptionsViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func showSettings() {
    if let settingsViewController = R.storyboard.settings.initialViewController {
      pushViewController(settingsViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func showAbout() {
    if let aboutViewController = R.storyboard.main.aboutViewController {
      pushViewController(aboutViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func goToPhoto(photo: Photo) {
    if let photoDetailViewController = R.storyboard.main.photoDetailViewController {
      photoDetailViewController.photo = photo
      pushViewController(photoDetailViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func goToComment(photoId: Int) {
    if let commentsViewController = R.storyboard.main.commentsViewController {
      commentsViewController.photoId = photoId
      pushViewController(commentsViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func goToCircle(id: Int) {
    if let circleViewController = R.storyboard.main.circleViewController {
      circleViewController.circleId = id
      pushViewController(circleViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  func goToCircle(circle: Circle) {
    if let circleViewController = R.storyboard.main.circleViewController {
      circleViewController.circle = circle
      pushViewController(circleViewController, animated: true)
      (viewControllers.first as! UIViewController).navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
}
