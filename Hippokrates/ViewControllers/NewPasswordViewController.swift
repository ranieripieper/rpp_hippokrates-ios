//
//  NewPasswordViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NewPasswordViewController: UIViewController {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var newPasswordTextField: TextField!
  @IBOutlet weak var newPasswordConfirmationTextField: TextField!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  
  var token: String!
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textFieldDelegate.returnCallback = {
      if $0 == self.newPasswordConfirmationTextField {
        self.saveTapped(nil)
      }
    }
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    textFieldDelegate.returnCallback = nil
  }
  
  // MARK: Save
  @IBAction func saveTapped(sender: UIButton?) {
    if validatePasswords() {
      let callback: Result<Bool> -> () = { result in
        switch result {
        case .Success:
          if let token = self.token {
            self.dismissViewControllerAnimated(true, completion: nil)
          } else {
            self.navigationController?.popViewControllerAnimated(true)
          }
        case .Failure(let error):
          BannerView.present(error.description, inViewController: self)
        }
        JHProgressHUD.sharedHUD.hide()
      }
      if let token = token {
        JHProgressHUD.sharedHUD.showInView(navigationController!.view)
        User.newPassword(newPasswordTextField.text, token: token, completion: callback)
      } else {
        JHProgressHUD.sharedHUD.showInView(navigationController!.view)
        User.currentUser.updateProfile(nil, specialtyId: nil, formationSchoolName: nil, formationSchoolYear: nil, professor: nil, avatar: nil, password: newPasswordTextField.text, completion: callback)
      }
    }
  }
  
  func validatePasswords() -> Bool {
    return count(newPasswordTextField.text) > 0 && newPasswordTextField.text == newPasswordConfirmationTextField.text
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
}
