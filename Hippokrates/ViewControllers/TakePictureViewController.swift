//
//  TakePictureViewController.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TakePictureViewController: UIViewController {
  @IBOutlet weak var captureView: CaptureView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if User.currentUser.remainingSubscriptionDays > 60 {
//      TestRemainingView.installInNavigationItem(navigationItem, days: User.currentUser.remainingSubscriptionDays)
//    }
    #if RELEASE
      Mixpanel.sharedInstance().track("Start Take Picture Flow")
    #endif
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    captureView.addVideoInput()
    captureView.addStillImageOutput()
    captureView.addVideoPreviewLayer()
    captureView.captureSession.startRunning()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    captureView.captureSession.stopRunning()
    captureView.removeVideoInput()
    captureView.removeStillImageOutput()
    captureView.removeVideoPreviewLayer()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let cropPictureViewController = segue.destinationViewController as? CropPictureViewController {
      if let image = sender as? UIImage {
        cropPictureViewController.image = image
      }
    }
  }
  
  // MARK: Flow
  func cropPicture(picture: UIImage) {
    self.performSegueWithIdentifier(R.segue.segueCrop, sender: picture)
  }
  
  @IBAction func closeTapped(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  // MARK: Camera
  @IBAction func takePicture(sender: UIButton) {
    captureView.captureStillImage { image in
      if image != nil {
        self.cropPicture(image!)
        #if RELEASE
          Mixpanel.sharedInstance().track("New Picture", properties: ["source": "camera"])
        #endif
      } else {
        let bannerView = BannerView(text: "Ocorreu um erro ao tirar a foto, por favor tente novamente.")
        bannerView.presentInViewController(self)
      }
    }
  }
  
  @IBAction func showGallery(sender: UIButton) {
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    imagePicker.sourceType = .PhotoLibrary
    self.presentViewController(imagePicker, animated: true, completion: nil)
    navigationController?.navigationBar.barStyle = UIBarStyle.Black
  }
  
  @IBAction func turnCamera(sender: UIButton) {
    captureView.changeVideoInput()
  }
}

extension TakePictureViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
      cropPicture(image)
      #if RELEASE
        Mixpanel.sharedInstance().track("New Picture", properties: ["source": "library"])
      #endif
      dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
