//
//  AppDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    loadSpecialties()
    loadSchools()
    loadApplication()
    if let launchOptions = launchOptions, let notification = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey] as? [String: AnyObject] {
      PushNotification.handleWithOptions(notification)
    }
    Fabric.with([Crashlytics()])
    #if RELEASE
      Mixpanel.sharedInstanceWithToken("86aee2c476815877ecca12f48477a0b0", launchOptions: launchOptions)
    #endif
    return true
  }
  
  // MARK: Load Specialties
  func loadSpecialties() {
    Specialty.initialLoad()
  }
  
  // MARK: Load Schools
  func loadSchools() {
    School.initialLoad()
  }
  
  // MARK: Application Flow
  func loadApplication() {
    window = UIWindow(frame: UIScreen.mainScreen().bounds)
    window?.makeKeyAndVisible()
    
    let viewController = UIViewController()
    viewController.view.frame = UIScreen.mainScreen().bounds
    if let view = R.nib.launchScreen.firstView(nil, options: nil) {
      view.frame = viewController.view.bounds
      viewController.view.addSubview(view)
    }
    window!.rootViewController = viewController
    
    if SSKeychain.passwordForService("us.hippokrates", account: "authToken") != nil {
      self.loadMainApplicationFlow()
    } else {
      self.loadLoginFlow()
    }
  }
  
  func loadLoginFlow() {
    window?.rootViewController = nil
    window?.rootViewController = R.storyboard.login.initialViewController
  }
  
  func loadMainApplicationFlow() {
    window?.rootViewController = nil
    window?.rootViewController = R.storyboard.main.initialViewController
  }
  
  // MARK: App links
  func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
    if let route = url.absoluteString?.componentsSeparatedByString("hippokrates.us").last {
      if route.rangeOfString("password/new", options: .CaseInsensitiveSearch) != nil {
        if let token = route.componentsSeparatedByString("token=").last {
          SSKeychain.deletePasswordForService("us.hippokrates", account: "authToken")
          loadLoginFlow()
          if let newPasswordNavigationController = R.storyboard.login.newPasswordNavigationController, newPasswordViewController = newPasswordNavigationController.viewControllers.first as? NewPasswordViewController {
            newPasswordViewController.token = token
            window?.rootViewController?.presentViewController(newPasswordNavigationController, animated: false, completion: nil)
          }
        }
      } else if route.rangeOfString("signup", options: .CaseInsensitiveSearch) != nil {
        if let code = route.componentsSeparatedByString("code=").last {
          SSKeychain.deletePasswordForService("us.hippokrates", account: "authToken")
          loadLoginFlow()
          if let codeViewController = R.storyboard.login.codeViewController, navigationController = window?.rootViewController as? UINavigationController {
            codeViewController.inviteCode = code
            navigationController.topViewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
            navigationController.pushViewController(codeViewController, animated: false)
          }
        }
      } else if route.rangeOfString("post", options: .CaseInsensitiveSearch) != nil {
        if let photoPath = route.componentsSeparatedByString("post/").last, photoId = photoPath.componentsSeparatedByString("?").first {
          loadMainApplicationFlow()
          if let commentsViewController = R.storyboard.main.commentsViewController, navigationController = window?.rootViewController as? UINavigationController {
            commentsViewController.photoId = photoId.toInt()
            navigationController.topViewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
            navigationController.pushViewController(commentsViewController, animated: false)
          }
        }
      } else if route.rangeOfString("friendship_circles", options: .CaseInsensitiveSearch) != nil {
        if let circlePath = route.componentsSeparatedByString("friendship_circles/").last, circleId = circlePath.componentsSeparatedByString("?").first {
          loadMainApplicationFlow()
          if let circleViewController = R.storyboard.main.circleViewController, navigationController = window?.rootViewController as? UINavigationController {
            circleViewController.circleId = circleId.toInt()
            navigationController.topViewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
            navigationController.pushViewController(circleViewController, animated: false)
          }
        }
      } else if route.rangeOfString("friedship_circles", options: .CaseInsensitiveSearch) != nil {
        if let circlePath = route.componentsSeparatedByString("friedship_circles/").last, circleId = circlePath.componentsSeparatedByString("?").first {
          loadMainApplicationFlow()
          if let circleViewController = R.storyboard.main.circleViewController, navigationController = window?.rootViewController as? UINavigationController {
            circleViewController.circleId = circleId.toInt()
            navigationController.topViewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
            navigationController.pushViewController(circleViewController, animated: false)
          }
        }
      }
    }
    return true
  }
  
  // MARK: Push
  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    let state = application.applicationState
    if let options = userInfo as? [String: AnyObject] where state == .Inactive || state == .Background {
      PushNotification.handleWithOptions(options)
    }
    #if RELEASE
      Mixpanel.sharedInstance().trackPushNotification(userInfo)
    #endif
  }
  
  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    #if RELEASE
      Mixpanel.sharedInstance().people.addPushDeviceToken(deviceToken)
    #endif
    PushNotification.registerToken(deviceToken.description)
  }
  
  func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
    
  }
  
  func goToPhoto(id: Int) {
    if let mainNavigationController = window?.rootViewController as? MainNavigationController {
      JHProgressHUD.sharedHUD.showInView(mainNavigationController.view)
      Photo.getPhotoWithId(id) {
        switch $0 {
        case .Success(let boxed):
          mainNavigationController.goToPhoto(boxed.unbox)
        case .Failure(let error):
          BannerView.present(error.description, inViewController: mainNavigationController)
        }
        JHProgressHUD.sharedHUD.hide()
      }
    }
  }
  
  func goToComment(photoId: Int) {
    if let mainNavigationController = window?.rootViewController as? MainNavigationController {
      mainNavigationController.goToComment(photoId)
    }
  }
  
  func goToCircle(id: Int) {
    if let mainNavigationController = window?.rootViewController as? MainNavigationController {
      mainNavigationController.goToCircle(id)
    }
  }
  
  func goToSubscription() {
    if let mainNavigationController = window?.rootViewController as? MainNavigationController {
      mainNavigationController.showSubscription()
    }
  }
}
