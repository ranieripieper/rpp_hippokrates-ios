//
//  AddressBookCollectionDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 6/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol AddressBookCollectionDelegateDelegate {
  func addressBookCollectionDelegateShouldSelectItem(addressBookCollectionDelegate: AddressBookCollectionDelegate) -> Bool
}

class AddressBookCollectionDelegate: NSObject {
  @IBOutlet weak var tableView: UITableView!
  
  var delegate: AddressBookCollectionDelegateDelegate?
  var datasource = [(String, String, UIImage?, Bool)]()
  var invites = [Int: String]()
  var filteredDatasource = [(String, String, UIImage?, Bool)]()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    AddressBook.getEmails() { result in
      switch result {
      case .Success(let records):
        self.datasource = records.unbox.map() {
          ($0, $1, $2, false)
        }
        self.filteredDatasource = self.datasource
        dispatch_async(dispatch_get_main_queue()) {
          if self.tableView != nil {
            self.tableView.reloadData()
          }
        }
      case .Failure(let error):
        break
      }
    }
  }
  
  func selectRecord(record: (String, String, UIImage?, Bool), atIndex index: Int) {
    invites[index] = record.1
  }
  
  func deselectRecordAtIndex(index: Int) {
    invites.removeValueForKey(index)
  }
}

extension AddressBookCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 70
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredDatasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let inviteCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.inviteCell.identifier, forIndexPath: indexPath) as! InviteCell
    let record = filteredDatasource[indexPath.row]
    inviteCell.configureWithAvatar(record.2, name: record.0, email: record.1, selected: record.3)
    return inviteCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let inviteCell = tableView.cellForRowAtIndexPath(indexPath) as! InviteCell
    inviteCell.selectorImageView.backgroundColor = UIColor.blueColor()
    filteredDatasource[indexPath.row].3 = true
    selectRecord(datasource[indexPath.row], atIndex: indexPath.row)
  }
  
  func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    let inviteCell = tableView.cellForRowAtIndexPath(indexPath) as! InviteCell
    inviteCell.selectorImageView.backgroundColor = UIColor.whiteColor()
    filteredDatasource[indexPath.row].3 = false
    deselectRecordAtIndex(indexPath.row)
  }
}
