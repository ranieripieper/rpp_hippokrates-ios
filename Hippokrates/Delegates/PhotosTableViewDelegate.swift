//
//  PhotosTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PhotosTableViewDelegate: NSObject {
  @IBOutlet weak var timelineViewController: TimelineViewController!
  
  var datasource = [Photo]()
  var dateFormatter: NSDateFormatter = {
    let df = NSDateFormatter()
    df.dateFormat = "dd.MM.yy - HH'h'mm"
    return df
  }()
  var lastOffset = CGPointZero
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: PhotoCell?
}

extension PhotosTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier) as? PhotoCell
      }
      sizingCell!.configureWithPhoto(datasource[indexPath.row], showUser: true, showHeader: true, dateFormatter: dateFormatter)
      return sizingCell!.calculateHeight()
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let photoCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier, forIndexPath: indexPath) as! PhotoCell
    photoCell.configureWithPhoto(datasource[indexPath.row], showUser: true, showHeader: true, dateFormatter: dateFormatter)
    photoCell.delegate = timelineViewController
    return photoCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    timelineViewController.goToPhoto(datasource[indexPath.row])
  }
}

extension PhotosTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    let deltaY = (scroll.contentOffset.y - lastOffset.y) / 2
    if scroll.contentOffset.y <= -64 {
      timelineViewController.menuViewBottomSpaceConstraint.constant = 0
    } else {
      timelineViewController.menuViewBottomSpaceConstraint.constant = min(max(timelineViewController.menuViewBottomSpaceConstraint.constant - deltaY, -60), 0)
    }
    lastOffset = scroll.contentOffset
    if scroll.contentOffset.y >= scroll.contentSize.height - 2 * scroll.bounds.height {
      timelineViewController.loadPhotos()
    }
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    tableViewDidStop(scrollView as! UITableView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      tableViewDidStop(scrollView as! UITableView)
    }
  }
  
  func tableViewDidStop(tableView: UITableView) {
    var rect = CGRectZero
    if timelineViewController.menuViewBottomSpaceConstraint.constant >= -30 {
      timelineViewController.menuViewBottomSpaceConstraint.constant = 0
    } else {
      timelineViewController.menuViewBottomSpaceConstraint.constant = -60
    }
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.timelineViewController.view.layoutIfNeeded()
    })
  }
}
