//
//  SearchTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchTableViewDelegate: NSObject {
  @IBOutlet weak var searchViewController: SearchViewController!
  
  var datasource: [AnyObject] = [] {
    didSet {
      filteredDatasource = datasource
    }
  }
  var filteredDatasource: [AnyObject] = []
}

extension SearchTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredDatasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    switch searchViewController.selectedType {
    case .Specialty, .Hashtag:
      return 44
    case .User:
      return 60
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch searchViewController.selectedType {
    case .Specialty:
      let autocompleteCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.autocompleteCell.identifier, forIndexPath: indexPath) as! AutocompleteCell
      if let specialty = filteredDatasource[indexPath.row] as? Specialty {
        autocompleteCell.configureWithText(specialty.name)
      }
      return autocompleteCell
    case .User:
      let userCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.userCell.identifier, forIndexPath: indexPath) as! UserCell
      if let user = filteredDatasource[indexPath.row] as? User {
        userCell.configureWithUser(user, showEditButtons: false, longPress: false)
      }
      return userCell
    case .Hashtag:
      let autocompleteCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.autocompleteCell.identifier, forIndexPath: indexPath) as! AutocompleteCell
      if let hashtag = filteredDatasource[indexPath.row] as? String {
        autocompleteCell.configureWithText(hashtag)
      }
      return autocompleteCell
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    switch searchViewController.selectedType {
    case .Specialty:
      if let specialty = filteredDatasource[indexPath.row] as? Specialty {
        searchViewController.resultsWithSpecialty(specialty)
      }
    case .User:
      if let user = filteredDatasource[indexPath.row] as? User {
        searchViewController.resultsWithUser(user)
      }
    case .Hashtag:
      if let hashtag = filteredDatasource[indexPath.row] as? String {
        searchViewController.resultsWithHashtag(hashtag)
      }
    }
  }
}
