//
//  UserProfileCollectionDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/30/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class UserProfileCollectionDelegate: NSObject {
  @IBOutlet weak var userProfileViewController: UserProfileViewController!
  
  var datasource = [(CellType, String)]()
  var user: User? {
    didSet {
      datasource.removeAll(keepCapacity: false)
      if let formation = user?.formation {
        let tuple = (CellType.School, formation.school.name)
        datasource.append(tuple)
      }
      if let formationYear = user?.formationYear {
        let tuple = (CellType.Class, String(formationYear))
        datasource.append(tuple)
      }
      if let specialty = user?.specialty {
        let tuple = (CellType.Specialty, specialty.name)
        datasource.append(tuple)
      }
      if let occupation = user?.occupation where count(occupation) > 0 {
        let tuple = (CellType.Occupation, occupation)
        datasource.append(tuple)
      }
      if let residency = user?.residency {
        let tuple = (CellType.Residency, residency.school.name)
        datasource.append(tuple)
      }
    }
  }
  var basePostIds = Set<Int>()
  var basePosts = [Photo]()
  var posts = [Photo]() {
    didSet {
      self.basePostIds.removeAll(keepCapacity: false)
      self.basePosts = self.posts.filter() {
        if self.basePostIds.contains($0.basePostId) {
          return false
        } else {
          self.basePostIds.insert($0.basePostId)
          return true
        }
      }
    }
  }
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: PhotoCell?
  var profileOnceToken: dispatch_once_t = 0
  var sizingProfileCell: UserProfileDetailsCell?
  
  var dateFormatter: NSDateFormatter = {
    let df = NSDateFormatter()
    df.dateFormat = "dd.MM.yy - HH'h'mm"
    return df
    }()
}

extension UserProfileCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if userProfileViewController.viewType == .List {
       return posts.count
    } else if userProfileViewController.viewType == .Details {
      return datasource.count
    }
    return 0
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      if userProfileViewController.viewType == .List {
        dispatch_once(&onceToken) {
          self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier) as? PhotoCell
        }
        sizingCell!.configureWithPhoto(posts[indexPath.row], showUser: true, showHeader: true, dateFormatter: dateFormatter)
        return sizingCell!.calculateHeight()
      } else if userProfileViewController.viewType == .Details {
        dispatch_once(&profileOnceToken) {
          self.sizingProfileCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.userProfileDetailsCell.identifier) as? UserProfileDetailsCell
        }
        sizingProfileCell!.configureWithType(datasource[indexPath.row].0, text: datasource[indexPath.row].1)
        return sizingProfileCell!.calculateHeight()
      } else {
        return 44
      }
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if userProfileViewController.viewType == .List {
      let photoCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier, forIndexPath: indexPath) as! PhotoCell
      photoCell.configureWithPhoto(posts[indexPath.row], showUser: true, showHeader: true, dateFormatter: dateFormatter)
      photoCell.delegate = userProfileViewController
      return photoCell
    } else if userProfileViewController.viewType == .Details {
      let userProfileDetailsCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.userProfileDetailsCell.identifier, forIndexPath: indexPath) as! UserProfileDetailsCell
      userProfileDetailsCell.configureWithType(datasource[indexPath.row].0, text: datasource[indexPath.row].1)
      return userProfileDetailsCell
    } else {
      return UITableViewCell()
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if userProfileViewController.viewType == .List {
      let photo = posts[indexPath.row]
      userProfileViewController.goToPhoto(photo)
    }
  }
}

extension UserProfileCollectionDelegate: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let width = (UIScreen.mainScreen().bounds.width - 8 - 20 - 8) / 2
    return CGSize(width: width, height: width * 2 / 3)
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return basePosts.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let photoThumbCell = collectionView.dequeueReusableCellWithReuseIdentifier(R.reuseIdentifier.photoThumbCell.identifier, forIndexPath: indexPath) as! PhotoThumbCell
    photoThumbCell.configureWithPhoto(basePosts[indexPath.item])
    return photoThumbCell
  }
}
