//
//  InviteTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class InviteTableViewDelegate: NSObject {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var inviteViewController: InviteViewController!
  
  var extraDatasource = [(String, String, UIImage?, Bool)]()
  var datasource = [(String, String, UIImage?, Bool)]()
  var filteredDatasource = [(String, String, UIImage?, Bool)]()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    AddressBook.getEmails() { result in
      switch result {
      case .Success(let records):
        self.datasource = records.unbox.map() {
          ($0, $1, $2, false)
          }.sorted() { lhs, rhs in
            lhs.0 < rhs.0
        }
        self.filteredDatasource = self.datasource
        dispatch_async(dispatch_get_main_queue()) {
          if self.tableView != nil {
            self.tableView.reloadData()
          }
        }
      case .Failure(let error):
        BannerView.present(error.description, inViewController: self.inviteViewController)
      }
    }
  }
}

extension InviteTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 70
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return extraDatasource.count
    }
    return filteredDatasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let inviteCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.inviteCell.identifier, forIndexPath: indexPath) as! InviteCell
    let record: (String, String, UIImage?, Bool)
    if indexPath.section == 0 {
      record = extraDatasource[indexPath.row]
      tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
    } else {
      record = filteredDatasource[indexPath.row]
    }
    inviteCell.configureWithAvatar(record.2, name: record.0, email: record.1, selected: record.3)
    return inviteCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if inviteViewController.shouldSelectItem() {
      let inviteCell = tableView.cellForRowAtIndexPath(indexPath) as! InviteCell
      inviteCell.select()
      filteredDatasource[indexPath.row].3 = true
      inviteViewController.selectRecord(filteredDatasource[indexPath.row], atIndex: indexPath.row)
    }
  }
  
  func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    let inviteCell = tableView.cellForRowAtIndexPath(indexPath) as! InviteCell
    inviteCell.deselect()
    if indexPath.section == 0 {
      extraDatasource[indexPath.row].3 = false
      inviteViewController.deselectExtraRecordAtIndex(indexPath.row)
    } else {
      filteredDatasource[indexPath.row].3 = false
      inviteViewController.deselectRecordAtIndex(indexPath.row)
    }
  }
}
