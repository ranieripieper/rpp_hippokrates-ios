//
//  BulkDeletePhotosCollectionDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 6/3/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BulkDeletePhotosCollectionDelegate: NSObject {
   @IBOutlet weak var bulkDeletePhotosViewController: BulkDeletePhotosViewController!
}

extension BulkDeletePhotosCollectionDelegate: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let width = (UIScreen.mainScreen().bounds.width - 4 - 12 - 12 - 4) / 3
    return CGSize(width: width, height: width * 2 / 3 + 4 + 4 + 16)
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return bulkDeletePhotosViewController.photos.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(R.reuseIdentifier.bulkDeleteCell.identifier, forIndexPath: indexPath) as! BulkDeleteCell
    cell.configureWithPhoto(bulkDeletePhotosViewController.photos[indexPath.item])
    return cell
  }
}
