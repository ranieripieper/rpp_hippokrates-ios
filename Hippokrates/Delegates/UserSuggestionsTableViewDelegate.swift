//
//  UserSuggestionsTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class UserSuggestionsTableViewDelegate: NSObject {
  @IBOutlet weak var userSuggestionsViewController: UserSuggestionsViewController!
  
  var datasource = [User]()
}

extension UserSuggestionsTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let userCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.userCell.identifier, forIndexPath: indexPath) as! UserCell
    userCell.configureWithUser(datasource[indexPath.row], showEditButtons: true, longPress: false)
    userCell.delegate = userSuggestionsViewController
    return userCell
  }
}
