//
//  SearchUserCollectionDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 6/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol SearchUserCollectionDelegateDelegate {
  func didSelectUser(user: User)
}

class SearchUserCollectionDelegate: NSObject {
  var datasource: [User] = []
  var delegate: SearchUserCollectionDelegateDelegate?
}

extension SearchUserCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 60
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let userCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.userCell.identifier, forIndexPath: indexPath) as! UserCell
    let user = datasource[indexPath.row]
    userCell.configureWithUser(user, showEditButtons: false, longPress: false)
    return userCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let user = datasource[indexPath.row]
    delegate?.didSelectUser(user)
  }
}
