//
//  SearchResultsTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultsTableViewDelegate: NSObject {
  @IBOutlet weak var searchResultsViewController: SearchResultsViewController!
  
  var datasource = [Photo]()
  var dateFormatter: NSDateFormatter = {
    let df = NSDateFormatter()
    df.dateFormat = "dd.MM.yy - HH'h'mm"
    return df
  }()
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: PhotoCell?
}

extension SearchResultsTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 50
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let searchResultsHeader = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SearchResultsHeader") as! SearchResultsHeader
    if searchResultsViewController.searchSpecialty != nil {
      searchResultsHeader.configureWithTerm(searchResultsViewController.searchSpecialty!.name)
    } else if searchResultsViewController.searchHashtag != nil {
      searchResultsHeader.configureWithTerm(searchResultsViewController.searchHashtag!)
    }
    return searchResultsHeader
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier) as? PhotoCell
      }
      sizingCell!.configureWithPhoto(datasource[indexPath.row], showUser: true, showHeader: true, dateFormatter: dateFormatter)
      return sizingCell!.calculateHeight()
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let photoCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier, forIndexPath: indexPath) as! PhotoCell
    photoCell.configureWithPhoto(datasource[indexPath.row], showUser: true, showHeader: true, dateFormatter: dateFormatter)
    photoCell.delegate = searchResultsViewController
    return photoCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    searchResultsViewController.goToPhoto(datasource[indexPath.row])
  }
}
