//
//  CircleTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum CircleShowMode {
  case Photos, Members
}

class CircleTableViewDelegate: NSObject {
  @IBOutlet weak var circleViewController: CircleViewController!
  
  var circleShowMode: CircleShowMode = .Photos
  var dateFormatter: NSDateFormatter = {
    let df = NSDateFormatter()
    df.dateFormat = "dd.MM.yy - HH'h'mm"
    return df
  }()
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: PhotoCell?
  var userOnceToken: dispatch_once_t = 0
  var sizingUserCell: UserCell?
}

extension CircleTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch circleShowMode {
    case .Photos:
      return circleViewController.photos.count ?? 0
    case .Members:
      return circleViewController.members.count
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      switch circleShowMode {
      case .Photos:
        dispatch_once(&onceToken) {
          self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier) as? PhotoCell
        }
        sizingCell!.configureWithPhoto(circleViewController.photos[indexPath.row], showUser: true, showHeader: true, dateFormatter: dateFormatter)
        return sizingCell!.calculateHeight()
      case .Members:
        dispatch_once(&userOnceToken) {
          self.sizingUserCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.userCell.identifier) as? UserCell
        }
        sizingUserCell!.configureWithUser(circleViewController.members[indexPath.row], showEditButtons: false, longPress: true)
        return sizingUserCell!.calculateHeight()
      }
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch circleShowMode {
    case .Photos:
      let photoCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier, forIndexPath: indexPath) as! PhotoCell
      let photo = circleViewController.photos[indexPath.row]
      if photo.user == nil, let userId = photo.userId {
        let user = circleViewController.linkedUsers.filter() {
          $0.id == userId
        }.first
        if let user = user {
          photo.user = user
        }
      }
      photoCell.configureWithPhoto(photo, showUser: true, showHeader: true, dateFormatter: dateFormatter)
      photoCell.delegate = circleViewController
      return photoCell
    case .Members:
      let userCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.userCell.identifier, forIndexPath: indexPath) as! UserCell
      userCell.configureWithUser(circleViewController.members[indexPath.row], showEditButtons: false, longPress: true)
      userCell.delegate = circleViewController
      return userCell
    default:
      return UITableViewCell()
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    switch circleShowMode {
    case .Photos:
      circleViewController.goToPhotoDetail(circleViewController.photos[indexPath.row])
    case .Members:
      circleViewController.goToUser(circleViewController.members[indexPath.row])
    }
  }
}

extension CircleTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y >= scrollView.contentSize.height - 2 * scrollView.bounds.height {
      switch circleShowMode {
      case .Photos:
        circleViewController.getPhotos()
      case .Members:
        circleViewController.getMembers()
      }
    }
  }
}
