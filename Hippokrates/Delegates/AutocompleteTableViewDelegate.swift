//
//  SchoolSearchTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class AutocompleteTableViewDelegate: NSObject {
  @IBOutlet weak var autocompleteViewController: AutocompleteViewController!
  
  var datasource = [NamedObject]()
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: AutocompleteCell?
}

extension AutocompleteTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.autocompleteCell.identifier) as? AutocompleteCell
      }
      sizingCell!.configureWithText(datasource[indexPath.row].name)
      return sizingCell!.calculateHeight()
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let autocompleteCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.autocompleteCell.identifier, forIndexPath: indexPath) as! AutocompleteCell
    autocompleteCell.configureWithText(datasource[indexPath.row].name)
    return autocompleteCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    autocompleteViewController.selectedIndex(indexPath.row)
  }
}
