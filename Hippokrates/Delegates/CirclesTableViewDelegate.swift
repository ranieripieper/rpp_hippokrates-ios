//
//  CirclesTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CirclesTableViewDelegate: NSObject {
  @IBOutlet weak var menuViewController: MenuViewController!
  
  var datasource = User.currentUser.circles
}

extension CirclesTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 50
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    switch User.currentUser.type {
    case .Physician, .Student:
      return 2
    case .Professor:
      return 3
    default:
      return 1
    }
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch User.currentUser.type {
    case .Physician, .Student:
      if section == 1 {
        return datasource.count
      } else {
        return 1
      }
    case .Professor:
      if section == 2 {
        return datasource.count
      } else {
        return 1
      }
    default:
      return datasource.count
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let circleCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.circleCell.identifier, forIndexPath: indexPath) as! CircleCell
    switch (User.currentUser.type, indexPath.section) {
    case (.Student, 0):
      circleCell.configureWithAddTitle("Participar de um novo círculo")
    case (_, 0):
      circleCell.configureWithAddTitle("Criar novo círculo")
    case (.Professor, 1):
      circleCell.configureWithAddTitle("Criar novo círculo de alunos")
    default:
      circleCell.configureWithTitle(datasource[indexPath.row].name, circlePicture: datasource[indexPath.row].pictureURL)
    }
    return circleCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    switch (User.currentUser.type, indexPath.section) {
    case (.Student, 0):
      menuViewController.joinStudentCircle()
    case (_, 0):
      menuViewController.createNewCircle()
    case (.Professor, 1):
      menuViewController.createNewStudentCircle()
    default:
      menuViewController.goToCircle(datasource[indexPath.row])
    }
  }
}
