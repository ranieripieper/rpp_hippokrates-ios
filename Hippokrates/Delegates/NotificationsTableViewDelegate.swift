//
//  NotificationsTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NotificationsTableViewDelegate: NSObject {
  @IBOutlet weak var notificationsViewController: NotificationsViewController!
  
  var datasource = [UserNotification]()
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: NotificationCell?
}

extension NotificationsTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.notificationCell.identifier) as? NotificationCell
      }
      sizingCell!.configureWithNotification(datasource[indexPath.row])
      return sizingCell!.calculateHeight()
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let notificationCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.notificationCell.identifier, forIndexPath: indexPath) as! NotificationCell
    notificationCell.configureWithNotification(datasource[indexPath.row])
    return notificationCell
  }
}

extension NotificationsTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y >= scrollView.contentSize.height - 2 * scrollView.bounds.height {
      notificationsViewController.getNotifications()
    }
  }
}
