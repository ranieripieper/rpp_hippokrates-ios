//
//  PhotoDetailTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PhotoDetailTableViewDelegate: NSObject {
  @IBOutlet weak var photoDetailViewController: PhotoDetailViewController!
  
  var photo: Photo?
  var dateFormatter: NSDateFormatter = {
    let df = NSDateFormatter()
    df.dateFormat = "dd.MM.yy - HH'h'mm"
    return df
    }()
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: PhotoCell?
  var commentOnceToken: dispatch_once_t = 0
  var sizingCommentCell: CommentCell?
}

extension PhotoDetailTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return photo == nil ? 0 : 1
    }
    return photo?.comments.count ?? 0
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      if indexPath.section == 0 {
        dispatch_once(&onceToken) {
          self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier) as? PhotoCell
        }
        sizingCell!.configureWithPhoto(photo!, showUser: false, showHeader: false, dateFormatter: dateFormatter)
        return sizingCell!.calculateHeight()
      } else {
        dispatch_once(&commentOnceToken) {
          self.sizingCommentCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.commentCell.identifier) as? CommentCell
        }
        sizingCommentCell!.configureWithComment(photo!.comments[indexPath.row])
        return sizingCommentCell!.calculateHeight()
      }
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      let photoCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.photoCell.identifier, forIndexPath: indexPath) as! PhotoCell
      photoCell.configureWithPhoto(photo!, showUser: false, showHeader: false, dateFormatter: dateFormatter)
      photoCell.delegate = photoDetailViewController
      return photoCell
    } else {
      let commentCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.commentCell.identifier, forIndexPath: indexPath) as! CommentCell
      commentCell.configureWithComment(photo!.comments[indexPath.row])
      commentCell.delegate = photoDetailViewController
      return commentCell
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.section == 1 {
      photoDetailViewController.performSegueWithIdentifier(R.segue.segueComments, sender: nil)
    }
  }
}

extension PhotoDetailTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    if scroll.contentOffset.y >= scroll.contentSize.height - 2 * scroll.bounds.height {
      photoDetailViewController.getComments()
    }
  }
}
