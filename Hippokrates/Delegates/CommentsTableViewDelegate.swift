//
//  CommentsTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CommentsTableViewDelegate: NSObject {
  @IBOutlet weak var commentsViewController: CommentsViewController!
  
  var onceToken: dispatch_once_t = 0
  var sizingCell: CommentCell?
}

extension CommentsTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return commentsViewController?.photo?.comments.count ?? 0
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.commentCell.identifier) as? CommentCell
      }
      sizingCell!.configureWithComment(commentsViewController!.photo!.comments[indexPath.row])
      return sizingCell!.calculateHeight()
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let commentCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.commentCell.identifier, forIndexPath: indexPath) as! CommentCell
    commentCell.configureWithComment(commentsViewController!.photo!.comments[indexPath.row])
    commentCell.delegate = commentsViewController
    return commentCell
  }
}

extension CommentsTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y >= scrollView.contentSize.height - 2 * scrollView.bounds.height {
      commentsViewController.getComments()
    }
  }
}
