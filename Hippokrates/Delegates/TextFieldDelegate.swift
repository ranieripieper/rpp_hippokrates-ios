//
//  TextFieldDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TextFieldDelegate: NSObject, UITextFieldDelegate {
  @IBOutlet weak var viewController: UIViewController!
  
  var returnCallback: (UITextField -> ())?
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if let nextTextField = viewController.view.viewWithTag(textField.tag + 1) {
      nextTextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
      returnCallback?(textField)
    }
    return true
  }
}
