//
//  EditNotificationsTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class EditNotificationsTableViewDelegate: NSObject {
  @IBOutlet weak var editNotificationsViewController: EditNotificationsViewController!
  @IBOutlet weak var tableView: UITableView!
  
  var datasource = [PushNotification]()

  var onceToken: dispatch_once_t = 0
  var sizingCell: EditNotificationCell?
}

extension EditNotificationsTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if IOS_8() {
      return UITableViewAutomaticDimension
    } else {
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.editNotificationCell.identifier) as? EditNotificationCell
      }
      sizingCell!.configureWithPushNotification(datasource[indexPath.row])
      return sizingCell!.calculateHeight()
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let editNotificationCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.editNotificationCell.identifier, forIndexPath: indexPath) as! EditNotificationCell
    editNotificationCell.configureWithPushNotification(datasource[indexPath.row])
    editNotificationCell.delegate = self
    return editNotificationCell
  }
}

extension EditNotificationsTableViewDelegate: EditNotificationCellDelegate {
  func activated(activated: Bool, atCell cell: EditNotificationCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      var preference = datasource[indexPath.row]
      preference.activated = activated
    }
  }
}
