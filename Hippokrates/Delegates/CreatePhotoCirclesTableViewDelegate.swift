//
//  CreatePhotoCirclesTableViewDelegate.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CreatePhotoCirclesTableViewDelegate: NSObject {
  @IBOutlet weak var tableView: UITableView!
  
  var datasource = User.currentUser.circles.map() { ($0, false) }
}

extension CreatePhotoCirclesTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 50
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let createPhotoCircleCell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.createPhotoCircleCell.identifier, forIndexPath: indexPath) as! CreatePhotoCircleCell
    createPhotoCircleCell.configureWithCircle(datasource[indexPath.row].0, selected: datasource[indexPath.row].1)
    createPhotoCircleCell.delegate = self
    return createPhotoCircleCell
  }
}

extension CreatePhotoCirclesTableViewDelegate: CreatePhotoCircleCellDelegate {
  func didSelectCell(cell: CreatePhotoCircleCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      datasource[indexPath.row].1 = !datasource[indexPath.row].1
      cell.updateWithSelection(datasource[indexPath.row].1)
    }
  }
}
