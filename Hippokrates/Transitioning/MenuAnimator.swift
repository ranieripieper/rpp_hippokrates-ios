//
//  MenuAnimator.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MenuAnimator: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning {
  @IBOutlet weak var mainNavigationController: MainNavigationController!
  @IBOutlet weak var menuPanGesture: UIPanGestureRecognizer!
  
  var presenting = false
  var interactive = false
  weak var fromSuperview: UIView?
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    if presenting {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! MainNavigationController
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
      let containerView = transitionContext.containerView()
      
      fromSuperview = fromViewController.view.superview
      toViewController.view.frame = CGRect(x: 0, y: 0, width: containerView.bounds.width, height: containerView.bounds.height)
      containerView.addSubview(toViewController.view)
      containerView.addSubview(fromViewController.view)
      fromViewController.view.userInteractionEnabled = false
      if IOS_8() {
        fromViewController.viewControllers.first!.view!!.frame = containerView.bounds
        fromViewController.viewControllers.first!.view!!.transform = CGAffineTransformIdentity
      } else {
        fromViewController.view.frame = containerView.bounds
        fromViewController.view.transform = CGAffineTransformIdentity
      }
      
      if let timelineViewController = mainNavigationController.viewControllers.first as? TimelineViewController {
        timelineViewController.menuViewBottomSpaceConstraint.constant = 0
      }
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        if IOS_8() {
          fromViewController.viewControllers.first!.view!!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -0.9 * containerView.bounds.width, 0)
        } else {
          fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -containerView.bounds.width, 0)
        }
        if let timelineViewController = fromViewController.viewControllers.first as? TimelineViewController {
          timelineViewController.view.layoutIfNeeded()
        }
        }) { finished in
          if transitionContext.transitionWasCancelled() {
            self.fromSuperview?.addSubview(fromViewController.view)
            fromViewController.view.userInteractionEnabled = true
          } else {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "didEnterBackground", name: UIApplicationDidEnterBackgroundNotification, object: nil)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    } else {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)! as! MainNavigationController
      let containerView = transitionContext.containerView()
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        if IOS_8() {
          toViewController.viewControllers.first!.view!!.transform = CGAffineTransformIdentity
        } else {
          toViewController.view.transform = CGAffineTransformIdentity
        }
        }) { finished in
          if !transitionContext.transitionWasCancelled() {
            self.fromSuperview?.addSubview(toViewController.view)
            self.fromSuperview = nil
            toViewController.view.userInteractionEnabled = true
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
  
  @IBAction func menuOpenPanGestureRecognizer(menuPanGestureRecognizer: UIPanGestureRecognizer) {
    let view = mainNavigationController.view
    if menuPanGestureRecognizer.state == .Began {
      let location = menuPanGestureRecognizer.locationInView(view)
      if location.x > view.bounds.width * 0.9 {
        interactive = true
        mainNavigationController.performSegueWithIdentifier(R.segue.segueMenu, sender: nil)
      }
    } else if menuPanGestureRecognizer.state == .Changed {
      let translation = menuPanGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      updateInteractiveTransition(d)
    } else if menuPanGestureRecognizer.state == .Ended {
      if menuPanGestureRecognizer.velocityInView(view).x < 0 {
        finishInteractiveTransition()
      } else {
        cancelInteractiveTransition()
      }
      interactive = false
    }
  }
  
  // Fix animation unbalance when returning from background
  func didEnterBackground() {
    mainNavigationController.dismissViewControllerAnimated(true, completion: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidEnterBackgroundNotification, object: nil)
  }
}

extension MenuAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = false
    return self
  }
  
  func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = true
    return interactive ? self : nil
  }
  
  func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = false
    return interactive ? self : nil
  }
}
