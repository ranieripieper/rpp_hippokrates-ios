//
//  PhotoDetailAnimator.swift
//  Hippokrates
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PhotoDetailAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  @IBOutlet weak var navigationController: MainNavigationController!
  
  var presenting = false
  var interactionController: UIPercentDrivenInteractiveTransition?
  var barUserView: BarUserView?
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)! as UIViewController
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)! as UIViewController
    let containerView = transitionContext.containerView()
    
    if presenting {
      toViewController.view.transform = CGAffineTransformMakeTranslation(navigationController.view.bounds.width, 0)
      containerView.addSubview(toViewController.view)
      
      addNavigationBarSubviewWithContainerView(containerView, from: fromViewController, to: toViewController)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        fromViewController.view.transform = CGAffineTransformMakeTranslation(-self.navigationController.view.bounds.width * 2 / 3, 0)
        toViewController.view.transform = CGAffineTransformIdentity
        self.animateNavigationBarSubview(fromViewController, to: toViewController)
        }) { (finished) -> Void in
          fromViewController.view.transform = CGAffineTransformIdentity
          toViewController.view.transform = CGAffineTransformIdentity
          if !transitionContext.transitionWasCancelled() {
            let popRecognizer = UIPanGestureRecognizer (target: self, action: "popGestureRecognizer:")
            self.navigationController.view.addGestureRecognizer(popRecognizer)
            self.removeNavigationBarSubview(toViewController)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    } else {
      toViewController.view.transform = CGAffineTransformMakeTranslation(-navigationController.view.bounds.width * 2 / 3, 0)
      containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)
      
      addNavigationBarSubviewWithContainerView(containerView, from: fromViewController, to: toViewController)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        fromViewController.view.transform = CGAffineTransformMakeTranslation(self.navigationController.view.bounds.width, 0)
        self.animateNavigationBarSubview(fromViewController, to: toViewController)
        toViewController.view.transform = CGAffineTransformIdentity
        }) { (finished) -> Void in
          fromViewController.view.transform = CGAffineTransformIdentity
          toViewController.view.transform = CGAffineTransformIdentity
          if !transitionContext.transitionWasCancelled() {
            self.removeNavigationBarSubview(toViewController)
          } else {
            UIView.animateWithDuration(0.3) {
              self.animateNavigationBarSubview(toViewController, to: fromViewController)
            }
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
  
  func addNavigationBarSubviewWithContainerView(containerView: UIView, from: UIViewController, to: UIViewController) {
    if to is PhotoDetailViewController || to is CommentsViewController {
      if let barUserView = barUserView {
        if let photoDetailViewController = to as? PhotoDetailViewController {
          barUserView.callback = photoDetailViewController.goToPhotoUser
        } else if let commentsViewController = to as? CommentsViewController {
          barUserView.callback = commentsViewController.goToPhotoUser
        }
      } else {
        var user: User {
          if let photoDetailViewController = to as? PhotoDetailViewController {
            return photoDetailViewController.photo!.user!
          } else if let commentsViewController = to as? CommentsViewController {
            return commentsViewController.photo!.user!
          } else {
            return User([:])
          }
        }
        var callback: () -> () {
          if let photoDetailViewController = to as? PhotoDetailViewController {
            return photoDetailViewController.goToPhotoUser
          } else if let commentsViewController = to as? CommentsViewController {
            return commentsViewController.goToPhotoUser
          } else {
            return {}
          }
        }
        barUserView = BarUserView.initWithUser(user, callback: callback)
        navigationController.view.superview?.addSubview(barUserView!)
        barUserView!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -barUserView!.bounds.height - 20)
      }
    }
  }
  
  func animateNavigationBarSubview(from: UIViewController, to: UIViewController) {
    if to is PhotoDetailViewController || to is CommentsViewController {
      barUserView?.transform = CGAffineTransformIdentity
    } else {
      if barUserView != nil {
        barUserView!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -barUserView!.bounds.height - 20)
      }
    }
  }
  
  func removeNavigationBarSubview(to: UIViewController) {
    if !(to is PhotoDetailViewController) && !(to is CommentsViewController) {
      barUserView?.removeFromSuperview()
      barUserView = nil
    }
  }
  
  func popGestureRecognizer(popGestureRecognizer: UIPanGestureRecognizer) {
    let view = navigationController.view
    if popGestureRecognizer.state == .Began {
      let location = popGestureRecognizer.locationInView(view)
      if location.x < view.bounds.width * 0.1 && navigationController.viewControllers.count > 1 {
        interactionController = UIPercentDrivenInteractiveTransition()
        navigationController.popViewControllerAnimated(true)
      }
    } else if popGestureRecognizer.state == .Changed {
      let translation = popGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      interactionController?.updateInteractiveTransition(d)
    } else if popGestureRecognizer.state == .Ended {
      if popGestureRecognizer.velocityInView(view).x > 0 {
        interactionController?.finishInteractiveTransition()
      } else {
        interactionController?.cancelInteractiveTransition()
      }
      interactionController = nil
    }
  }
}

extension PhotoDetailAnimator: UINavigationControllerDelegate {
  func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    if operation == .Pop {
      self.presenting = false
    } else {
      self.presenting = true
    }
    return self
  }
  
  func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return interactionController
  }
}
